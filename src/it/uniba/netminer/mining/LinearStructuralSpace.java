package it.uniba.netminer.mining;

import it.uniba.netminer.dataschema.IDerivedAttribute;
import it.uniba.netminer.dataschema.IDerivedEntity;
import it.uniba.netminer.dataschema.IView;
import it.uniba.netminer.mining.StructuralNode.StructuralNodeType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class LinearStructuralSpace 
implements IIteratorFactory<IView, IDerivedAttribute>, Iterable<IDerivedAttribute> {
	
	public ArrayList<IDerivedAttribute> _attributeSet;
	public HashMap<IView, LinkedHashSet<IView>> _gerarchicalSpan;

	public LinearStructuralSpace(StructuralTree tree){
		this._attributeSet = new ArrayList<IDerivedAttribute>();
		this._gerarchicalSpan = new HashMap<IView, LinkedHashSet<IView>>();
		
		LinkedList<StructuralNode> queue = new LinkedList<StructuralNode>();
		queue.push(tree.getRoot());
		
		/*
		 * vanno linearizzati, usando una visita in ampiezza sulle viste
		 * dell'albero strutturale, soltanto gli attributi appartenenti
		 * a viste create per l'esistenza di una chiave esterna. 
		 * I nodi creati per fratellanza con il padre non vanno valutati, 
		 * sar� compito valutarli nei raffinamenti per selezione.
		 */
		for(StructuralNode node : tree){
			if(node.getNodeType() == StructuralNodeType.NEW_FK_EXHIBITED){
				IView view = node.getData();
				IDerivedEntity lastEntity = view.getLastJoinedEntity();
				for(IDerivedAttribute attribute : lastEntity){
					this._attributeSet.add(attribute);
				}
			}
		}
		
		while(!queue.isEmpty()){
			StructuralNode currentNode = queue.pop();
			StructuralNode currentChild = currentNode.getFirstChild();
			IView currentView = currentNode.getData();
			
			LinkedHashSet<IView> viewsReachableBySelectRefinement = new LinkedHashSet<IView>();
			viewsReachableBySelectRefinement.add(currentView);
			
			while(currentChild!=null){
				viewsReachableBySelectRefinement.add(currentChild.getData());
				queue.push(currentChild);
				currentChild = currentChild.getNextSibling();
			}
			
			this._gerarchicalSpan.put(currentView, viewsReachableBySelectRefinement);
		}	
	}

	
	public Set getSet(IView object){
		return this._gerarchicalSpan.get(object);
	}
	
	
	@Override
	public Iterator<IDerivedAttribute> iterator() {
		return this._attributeSet.iterator();
	}

	@Override
	public boolean hasIterator(IView object) {
		return this._gerarchicalSpan.containsKey(object);
	}
	

	@Override
	public Iterator<IDerivedAttribute> getIterator(IView object) {
		LinearStructuralPartitionIterator iterator = null;
		if(this.hasIterator(object)){
			LinkedHashSet<IView> partition = this._gerarchicalSpan.get(object);
			iterator = new LinearStructuralPartitionIterator(partition);
		}
		return iterator;
	}
	
	
	
	
	private class LinearStructuralPartitionIterator implements Iterator<IDerivedAttribute>{
		private Iterator<IView> _viewIterator;
		private Iterator<IDerivedAttribute> _attributeIterator;
		
		private LinearStructuralPartitionIterator(LinkedHashSet<IView> partition){
			this._viewIterator = partition.iterator();
			this._attributeIterator = null;
		}

		
		@Override
		public boolean hasNext() {
			boolean isNextPresent = false;
			if(this._attributeIterator == null){
				if(this._viewIterator.hasNext()){
					isNextPresent = true;
				}
			}else{
				if(this._attributeIterator.hasNext()){
					isNextPresent = true;
				}else{
					if(this._viewIterator.hasNext()){
						isNextPresent = true;
					}
				}
			}
			return isNextPresent;
		}

		
		
		@Override
		public IDerivedAttribute next() {
			if(this._attributeIterator == null){
				if(this._viewIterator.hasNext()){
					this._attributeIterator = this._viewIterator.next().iterator();
				}
			}
			
			IDerivedAttribute attribute = this._attributeIterator.next();
			if(!this._attributeIterator.hasNext()){
				if(this._viewIterator.hasNext()){
					this._attributeIterator = this._viewIterator.next().iterator();
				}
			}
			
			return attribute;
		}

		
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
