package it.uniba.netminer.mining;

import it.uniba.netminer.mining.exceptions.TargetObjectAlreadyAssignedException;

import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

public class Pane implements Iterable<TargetObject> {
	
	private Vector<TargetObject> _basket;
	private int _maxSize;
	private TimeWindow _owner;

	public Pane(int size){
		this._maxSize = size;
		this._basket = new Vector<TargetObject>();
		this._owner = null;
	}
	
	
	protected void setWindowOwner(TimeWindow window){
		this._owner = window;
	}
	
	public TimeWindow getWindowOwner(){
		return this._owner;
	}

	@Override
	public Iterator<TargetObject> iterator() {
		return this._basket.iterator();
	}
	
	public int indexOf(TargetObject obj){
		return this._basket.indexOf(obj);
	}
	
	public TargetObject getAt(int index){
		TargetObject obj = null;
		try{
			obj = this._basket.get(index);
		}catch(ArrayIndexOutOfBoundsException ex){
			
		}
		return obj;
	}
	
	public int getSize(){
		return this._maxSize;
	}
	
	public int getCount(){
		return this._basket.size();
	}
	
	public boolean addTargetObject(TargetObject obj) throws TargetObjectAlreadyAssignedException{
		if(obj.getPaneOwner()!=null){
			throw new TargetObjectAlreadyAssignedException();
		}
		
		if(!this.isFull()){
			boolean result = this._basket.add(obj);
			if(result){
				obj.setPaneOwner(this);
			}
			return result;
			
		}else{
			return false;
		}
	}
	
	public boolean isEmpty(){
		return this._basket.isEmpty();
	}
	
	public boolean isFull(){
		return this._basket.size()==this._maxSize;
	}
	
	public TargetObject first(){
		if(!this.isEmpty()){
			return this._basket.firstElement(); 
		}else{
			return null;
		}
		
	}
	
	public TargetObject last(){
		if(this.isFull()){
			return this._basket.lastElement();
		}else{
			return null;
		}
	}
	
	public Date getStartDate(){
		TargetObject first = this.first();
		Date date = null;
		if(first!=null){
			date = first.getDate();
		}
		return date;
	}
	
	public Date getEndDate(){
		TargetObject last = this.last();
		Date date = null;
		if(last!=null){
			date = last.getDate();
		}
		return date;
	}
	
	@Override
	public String toString(){
		Date startDate = this.getStartDate();
		Date endDate = this.getEndDate();
		String startStringDate = "n/a";
		String endStringDate ="n/a";
		if(startDate!=null){
			startStringDate = startDate.toString();
		}
		if(endDate!=null){
			endStringDate = endDate.toString();
		}
		return "Pane ("+startStringDate+", "+endStringDate+")";
	}

}
