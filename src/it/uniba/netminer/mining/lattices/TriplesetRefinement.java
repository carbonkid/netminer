package it.uniba.netminer.mining.lattices;

import it.uniba.netminer.dataschema.ICondition;
import it.uniba.netminer.mining.exceptions.InvalidTriplesetException;
import it.uniba.netminer.mining.setree.IRefinement;
import it.uniba.netminer.mining.setree.Itemset;
import it.uniba.netminer.mining.setree.MultiKeyList;

public class TriplesetRefinement implements IRefinement {

	protected Itemset _firstLevel;
	protected Itemset _secondLevel;
	protected Itemset _thirdLevel;
	
	public TriplesetRefinement(Itemset tripleset) throws InvalidTriplesetException{
		//verifichiamo che l'itemset sia un 3-itemset
		try{
			if(tripleset.getLevel()!=3 || tripleset.getAncestor().getAncestor().getLevel()!=1){
				throw new InvalidTriplesetException();
			}else{
				this._thirdLevel = tripleset;
				this._secondLevel = tripleset.getAncestor();
				this._firstLevel = this._secondLevel.getAncestor();
			}
		}catch(NullPointerException ex){
			throw new InvalidTriplesetException();
		}
	}

	
	
	@Override
	public int compareTo(IRefinement o) {
		int result = 0;
		if(o instanceof TriplesetRefinement){
			TriplesetRefinement refinement = (TriplesetRefinement)o;

			result = this._firstLevel.getData().compareTo(refinement._firstLevel.getData());
			if(result==0){
				result = this._secondLevel.getData().compareTo(refinement._secondLevel.getData());
				if(result==0){
					result = this._thirdLevel.getData().compareTo(refinement._thirdLevel.getData());
				}
			}
		}else{
			throw new ClassCastException();
		}
		return result;
	}
	
	
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		builder.append(this._firstLevel.getData().getData().getCondition().toString());
		builder.append(" and ");
		builder.append(this._secondLevel.getData().getData().getCondition().toString());
		builder.append(" and ");
		builder.append(this._thirdLevel.getData().getData().getCondition().toString());
		builder.append(")");
		return builder.toString();
	}

	
	
	public MultiKeyList getKeyList(){
		return (MultiKeyList)this._thirdLevel.getSupport().getKeyList();
	}
	
	
	@Override
	public ICondition getCondition() {
		// TODO Auto-generated method stub
		return null;
	}

}
