package it.uniba.netminer.mining.lattices;

import it.uniba.netminer.dataschema.ICondition;
import it.uniba.netminer.dataschema.IDerivedAttribute;
import it.uniba.netminer.dataschema.IDerivedEntity;
import it.uniba.netminer.dataschema.IView;
import it.uniba.netminer.mining.setree.IRefinement;

public class Refinement implements IRefinement {

	private ICondition _condition;
	private int _ordinal;
	
	public Refinement(ICondition condition, int ordinal){
		this._condition = condition;
		this._ordinal = ordinal;
	}
	
	public ICondition getCondition(){
		return this._condition;
	}
	
	
	public IView getView(){
		IDerivedAttribute attribute = (IDerivedAttribute)this._condition.getConditionatedAttribute();
		IDerivedEntity entity = attribute.getDerivedEntityOwner();
		return entity.getViewOwner();
	}
	
	
	@Override
	public String toString(){
		return "("+this._condition.toString()+")";
	}

	@Override
	public int compareTo(IRefinement o) {
		int result = 0;
		
		if(o instanceof Refinement){
			Refinement refinement = (Refinement)o;
			String thisAttributeName = this._condition.getConditionatedAttribute().getFullyQualifiedName();
			String refinementConditionAttributeName = refinement.getCondition().getConditionatedAttribute().getFullyQualifiedName();
				
			int comparationResult = thisAttributeName.compareToIgnoreCase(refinementConditionAttributeName);
			if(comparationResult==0){
				result = ((Integer)this._ordinal).compareTo(refinement._ordinal);
			}else{
				result = comparationResult;
			}
		}
		
		
		return result;
	}
}
