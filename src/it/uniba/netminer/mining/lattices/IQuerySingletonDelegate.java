package it.uniba.netminer.mining.lattices;

import it.uniba.netminer.mining.Pane;
import it.uniba.netminer.mining.setree.ITidList;
import it.uniba.netminer.mining.setree.Item;

public interface IQuerySingletonDelegate<T extends ITidList> {
	public T evaluateTransactionList(Item item, Pane pane);
}
