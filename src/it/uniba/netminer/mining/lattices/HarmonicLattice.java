package it.uniba.netminer.mining.lattices;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import it.uniba.netminer.dataschema.IDerivedAttribute;
import it.uniba.netminer.dataschema.IView;
import it.uniba.netminer.mining.Pane;
import it.uniba.netminer.mining.TimeWindow;
import it.uniba.netminer.mining.setree.Item;
import it.uniba.netminer.mining.setree.Itemset;
import it.uniba.netminer.mining.setree.MultiKeyList;

public class HarmonicLattice extends Lattice {

	private IQuerySingletonDelegate<MultiKeyList> _tidlistGeneratorDelegate;
	private DeductiveLattice _deductiveLattice;
	private TimeWindow _owner;

	public HarmonicLattice(
			TimeWindow owner,
			List<Refinement> list, 
			int depth, 
			float minimumSupport) {
		//reticolo armonico alto 4 livelli (3 logici + root)
		//reticolo deduttivo alto k livelli
		super(4, minimumSupport);
		this._owner = owner;
		this._deductiveLattice = new DeductiveLattice(this, depth, minimumSupport);
		this._tidlistGeneratorDelegate = new VerticalLayoutDelegate();
		
		//caratterizzo gli items su cui deve essere costruito l'SETree
		for(Refinement item : list){
			this._seTree.addItem(item);
		}
	}
	
	
	public Status getStatus(){
		return this._deductiveLattice.getStatus();
	}

	
	@Override
	public void init(Pane pane) {
		//event dispatch, inizio processo di query
		this._owner.getStreamOwner().fireQueriesStarted(this._owner);
		
		//si ottiene il supporto per ogni oggetto su cui si basa il reticolo
		LinkedList<MultiKeyList> mkListBasket = new LinkedList<MultiKeyList>();
		for(Item item : super._seTree.getItemList()){
			MultiKeyList mkList = this._tidlistGeneratorDelegate.evaluateTransactionList(item, pane);
			mkListBasket.add(mkList);
		}
		
		//event dispatch, fine processo di query e inizio processo di analisi
		this._owner.getStreamOwner().fireQueriesCompleted(this._owner);
		this._owner.getStreamOwner().fireFrequentItemsetsUpdateStarted(this._owner);
		
		//analisi del supporto al primo livello
		for(Item item : super._seTree.getItemList()){
			MultiKeyList mkList = mkListBasket.pollFirst();
			item.getSupport().setKeyList(mkList);
			float absoluteSupport = mkList.getSize();
			float relativeSupport = absoluteSupport/pane.getWindowOwner().getTargetObjectsCount();
			
			//se frequente va aggiunto al primo livello
			if(relativeSupport > super.getMinimumSupport()){
				Itemset itemset = this._seTree.getRoot().addChild(item);
				itemset.getSupport().setKeyList(mkList);
			}
		}
		
		for(Itemset itemset : this._seTree.getRoot().getChildrenList()){
			super.generateSubTree(pane, itemset);
		}
		
		//event dispatching, fine analisi itemsets
		this._owner.getStreamOwner().fireFrequentItemsetsUpdateCompleted(this._owner);
		
		//System.out.println("Harmonic initialization completed reporting "+this._seTree.getRoot().getDescendantsCount()+" frequent itemsets!");
		this._deductiveLattice.init(pane);
	}

	
	
	
	@Override
	public void update(Pane pane) {
		//event dispatch, inizio processo di query
		this._owner.getStreamOwner().fireQueriesStarted(this._owner);
				
		//si ottiene il supporto per ogni oggetto su cui si basa il reticolo
		LinkedList<MultiKeyList> mkListBasket = new LinkedList<MultiKeyList>();
		for(Item item : super._seTree.getItemList()){
			MultiKeyList mkList = this._tidlistGeneratorDelegate.evaluateTransactionList(item, pane);
			mkListBasket.add(mkList);
		}
				
		//event dispatch, fine processo di query e inizio processo di analisi
		this._owner.getStreamOwner().fireQueriesCompleted(this._owner);
		this._owner.getStreamOwner().fireFrequentItemsetsUpdateStarted(this._owner);
		
		//si ottiene il supporto per ogni oggetto su cui si basa il reticolo
		for(Item item : super._seTree.getItemList()){
			MultiKeyList updatedMkList = (MultiKeyList)item.getSupport().getKeyList();
			MultiKeyList mkList = mkListBasket.pollFirst();
			updatedMkList.integrate(mkList);
			item.getSupport().setKeyList(updatedMkList);
			
			//l'oggetto poteva essere frequente o meno in passato
			Itemset child = this._seTree.getRoot().getChildrenList().getElementByItem(item);
			
			//aggiorno la tidList incrementata
			float absoluteSupport = updatedMkList.getSize();
			float relativeSupport = absoluteSupport/pane.getWindowOwner().getTargetObjectsCount();
			if(relativeSupport > super.getMinimumSupport()){
				if(child==null){
					child = this._seTree.getRoot().addChild(item);
					child.getSupport().setKeyList(updatedMkList);
				}else{
					child.getSupport().setKeyList(updatedMkList);
				}
			}else{
				//pruning immediato del nodo
				if(child!=null){
					this.removeNode(child);
				}
			}
		}
		
		super.updateSupports(pane);
		
		//event dispatching, fine analisi itemsets
		this._owner.getStreamOwner().fireFrequentItemsetsUpdateCompleted(this._owner);
		
		//System.out.println("Harmonic update completed reporting "+this._seTree.getRoot().getDescendantsCount()+" frequent itemsets!");
		this._deductiveLattice.update(pane);
	}
	
	
	
	@Override
	protected void removeNode(Itemset rootset){
		if(rootset.getLevel()!=0){
			Itemset currentSibling = rootset.getPreviousElement();
			while(currentSibling!=null){
				Itemset nodeToPrune = currentSibling.getChildrenList().getElementByItem(rootset.getData());
				if(nodeToPrune!=null){
					this.removeNode(nodeToPrune);
				}
				currentSibling = currentSibling.getPreviousElement();
			}
			
			//prima del pruning occorre capire se il nodo contiene dei figli di altezza pari almeno a 3
			//nel caso ce ne fossero bisognerebbe rimuoverli dal reticolo deduttivo (il reticolo armonico ha altezza fissa)
			if(rootset.getLevel()==1){
				for(Itemset secondLevelChild : rootset.getChildrenList()){
					for(Itemset thirdLevelChild : secondLevelChild.getChildrenList()){
						TriplesetRefinement tripleset = new TriplesetRefinement(thirdLevelChild);
						this._deductiveLattice.markRefinementForDeletion(tripleset);
					}
				}
			}else if(rootset.getLevel()==2){
				for(Itemset thirdLevelChild : rootset.getChildrenList()){
					TriplesetRefinement tripleset = new TriplesetRefinement(thirdLevelChild);
					this._deductiveLattice.markRefinementForDeletion(tripleset);
				}
			}else if(rootset.getLevel()==3){
				TriplesetRefinement tripleset = new TriplesetRefinement(rootset);
				this._deductiveLattice.markRefinementForDeletion(tripleset);
			}
			
			//pruning
			rootset.getAncestor().removeChild(rootset);
		}
	}
	
	
	

	@Override
	public Iterator<Itemset> iterator() {
		return this._deductiveLattice.iterator();
	}


	@Override
	public Itemset bind(Pane pane, Itemset father, Itemset candidate) {
		Itemset elementObject = null;
		IDerivedAttribute attributeToRefine = (IDerivedAttribute)father.getData().getData().getCondition().getConditionatedAttribute();
		IView viewToRefine = attributeToRefine.getDerivedEntityOwner().getViewOwner();
		IDerivedAttribute attribute = (IDerivedAttribute)candidate.getData().getData().getCondition().getConditionatedAttribute();
		IView viewInvolved = attribute.getDerivedEntityOwner().getViewOwner();
		Set allowedViewsForRefinement = pane.getWindowOwner().getStreamOwner().getStructuralTree().getLinearized().getSet(viewToRefine);
		int toCount = pane.getWindowOwner().getTargetObjectsCount();

		if(allowedViewsForRefinement!=null &&
		   attributeToRefine != attribute){
			
			elementObject = father.addChild(candidate.getData());
			if(elementObject!=null){
				//calcoliamo il supporto
				MultiKeyList fatherList = (MultiKeyList)father.getSupport().getKeyList();
				MultiKeyList singletonList = (MultiKeyList)candidate.getSupport().getKeyList();
				MultiKeyList currentList = (MultiKeyList)fatherList.intersect(singletonList);
				elementObject.getSupport().setKeyList(currentList);
			
				//se il nodo � frequente dobbiamo espanderlo per verificare
				//che i pattern pi� specifici siano comunque frequenti
				float absoluteSupport = currentList.getSize();
				float relativeSupport = absoluteSupport/toCount;
				float threshold = super.getMinimumSupport();

				if(relativeSupport<threshold){
					father.removeChild(elementObject);
					elementObject = null;
				}else{
					if(elementObject.getLevel()==3){
						//il pattern � frequente, verifichiamo se gi� esisteva nel reticolo armonico
						TriplesetRefinement tripleset = new TriplesetRefinement(elementObject);
						this._deductiveLattice.addNewRefinement(tripleset);
					}
				}
			}
		}
		
		return elementObject;
	}


	@Override
	public Itemset check(Pane pane, Itemset father, Itemset candidate) {
		//aggiorno il supporto di candidate come figlio di father
		//sia father che candidate sono pattern frequenti, il figlio (father,candidate)
		//poteva essere frequente come infrequente: se non presente
		//bisogna verificare se � divenuto frequente, altrimenti bisogna aggiornare
		//la sua tidlist e verificare che non sia divenuto infrequente
		
		
		Itemset elementObject = null;
		IDerivedAttribute attributeToRefine = (IDerivedAttribute)father.getData().getData().getCondition().getConditionatedAttribute();
		IView viewToRefine = attributeToRefine.getDerivedEntityOwner().getViewOwner();
		IDerivedAttribute attribute = (IDerivedAttribute)candidate.getData().getData().getCondition().getConditionatedAttribute();
		IView viewInvolved = attribute.getDerivedEntityOwner().getViewOwner();
		Set allowedViewsForRefinement = pane.getWindowOwner().getStreamOwner().getStructuralTree().getLinearized().getSet(viewToRefine);
		int toCount = pane.getWindowOwner().getTargetObjectsCount();

		if(allowedViewsForRefinement!=null &&
		   attributeToRefine != attribute){
			
			elementObject = father.getChildrenList().getElementByItem(candidate.getData());
			if(elementObject==null){
				//non era presente, provo ad aggiungerlo se possibile
				elementObject = father.addChild(candidate.getData());
				if(elementObject!=null){
					//calcoliamo il supporto
					MultiKeyList fatherList = (MultiKeyList)father.getSupport().getKeyList();
					MultiKeyList singletonList = (MultiKeyList)candidate.getSupport().getKeyList();
					MultiKeyList currentList = (MultiKeyList)fatherList.intersect(singletonList);
					elementObject.getSupport().setKeyList(currentList);
				
					//se il nodo � frequente dobbiamo espanderlo per verificare
					//che i pattern pi� specifici siano comunque frequenti
					float absoluteSupport = currentList.getSize();
					float relativeSupport = absoluteSupport/toCount;
					float threshold = super.getMinimumSupport();
					
					if(relativeSupport < threshold){
						father.removeChild(elementObject);
						elementObject = null;
					}else{
						if(elementObject.getLevel()==3){
							//il pattern � un nuovo frequente
							TriplesetRefinement tripleset = new TriplesetRefinement(elementObject);
							this._deductiveLattice.addNewRefinement(tripleset);
						}
					}
				}
				
			}else{
				//era presente, aggiorno la tidlist
				MultiKeyList fatherList = (MultiKeyList)father.getSupport().getKeyList();
				MultiKeyList singletonList = (MultiKeyList)candidate.getSupport().getKeyList();
				MultiKeyList currentList = (MultiKeyList)fatherList.intersect(singletonList);
				elementObject.getSupport().setKeyList(currentList);
			
				//se il nodo � frequente dobbiamo espanderlo per verificare
				//che i pattern pi� specifici siano comunque frequenti
				float absoluteSupport = currentList.getSize();
				float relativeSupport = absoluteSupport/toCount;
				float threshold = super.getMinimumSupport();
				
				if(relativeSupport < threshold){
					father.removeChild(elementObject);
					elementObject = null;
				}else{
					//il pattern era gi� presente, in linea teorica dovrebbe continuare ad esistere
					//anche nel reticolo deduttivo. Bisognerebbe soltanto aggiornare la tidlist. 
				}
			}
		}
		
		return elementObject;
	}
	
	
	public TimeWindow getWindowOwner(){
		return this._owner;
	}
	
	
	public DeductiveLattice getDeductiveLattice(){
		return this._deductiveLattice;
	}

}
