package it.uniba.netminer.mining.lattices;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.SortedSet;

import it.uniba.netminer.mining.Pane;
import it.uniba.netminer.mining.setree.Item;
import it.uniba.netminer.mining.setree.Itemset;
import it.uniba.netminer.mining.setree.KeyList;
import it.uniba.netminer.mining.setree.MultiKeyList;

public class DeductiveLattice extends Lattice{
	
	private HarmonicLattice _owner;
	private LinkedList<TriplesetRefinement> _lastAddedItemRefinement;
	private LinkedList<TriplesetRefinement> _lastMarkedForDeletionItemRefinement;
	private Status _latticeStatus;
	protected long _frequentCount;
	protected long _newFrequentCount;
	protected long _newInfrequentCount;
	protected long _previousFrequentCount;

	public DeductiveLattice(HarmonicLattice lattice, int depth, float minimumSupport) {
		super(depth, minimumSupport);
		this._owner = lattice;
		this._latticeStatus = new Status(this);
		this._lastAddedItemRefinement = new LinkedList<TriplesetRefinement>();
		this._lastMarkedForDeletionItemRefinement = new LinkedList<TriplesetRefinement>();
		this._frequentCount = 0;
		this._previousFrequentCount = 0;
		this._newFrequentCount = 0;
		this._newInfrequentCount = 0;
	}
	
	
	public void markRefinementForDeletion(TriplesetRefinement refinement){
		this._lastMarkedForDeletionItemRefinement.add(refinement);
	}
	
	public void addNewRefinement(TriplesetRefinement refinement){
		this._lastAddedItemRefinement.add(refinement);
	}
	
	
	
	

	
	
	@Override
	public void init(Pane pane) {
		//event dispatch, inizio processo di analisi
		this._owner.getWindowOwner().getStreamOwner().fireFrequentTriplesetsUpdateStarted(this._owner.getWindowOwner());
		
		while(!this._lastAddedItemRefinement.isEmpty()){
			TriplesetRefinement refinement = this._lastAddedItemRefinement.pop();
			MultiKeyList keyList = refinement.getKeyList();
			float absoluteSupport = keyList.getSize();
			float relativeSupport = absoluteSupport/pane.getWindowOwner().getTargetObjectsCount();
			
			if(relativeSupport < super.getMinimumSupport()){
				System.out.println("1-Tripleset added as frequent when it's not");
			}else{
				if(!this._seTree.addItem(refinement)){
					System.out.println("1-Tripleset already present");
				}
			}	
		}
		
		for(Item item : this._seTree.getItemList()){
			TriplesetRefinement refinement = (TriplesetRefinement)item.getData();
			MultiKeyList mkList = refinement.getKeyList();
			KeyList kList = mkList.toKeyList();
			Itemset itemset = this._seTree.getRoot().addChild(item);
			itemset.getSupport().setKeyList(kList);
			item.getSupport().setKeyList(kList);
			this._owner.getWindowOwner().getStreamOwner().fireFrequentPatternFound(itemset, this._owner.getWindowOwner());
			this._frequentCount++;
		}
		
		for(Itemset itemset : this._seTree.getRoot().getChildrenList()){
			super.generateSubTree(pane, itemset);
		}
		
		this.updateSupports(pane);
		
		//event dispatching, fine analisi itemsets
		this._owner.getWindowOwner().getStreamOwner().fireFrequentTriplesetsUpdateCompleted(this._owner.getWindowOwner());
		//System.out.println("Deductive initialization completed reporting "+this._seTree.getRoot().getDescendantsCount()+" frequent itemsets!");
	}
	
	
	
	

	@Override
	public void update(Pane pane) {
		//event dispatch, inizio processo di analisi
		this._owner.getWindowOwner().getStreamOwner().fireFrequentTriplesetsUpdateStarted(this._owner.getWindowOwner());
				
		this._previousFrequentCount = this._frequentCount;
		this._frequentCount = 0;
		this._newFrequentCount = 0;
		this._newInfrequentCount = 0;
		
		while(!this._lastMarkedForDeletionItemRefinement.isEmpty()){
			TriplesetRefinement refinement = this._lastMarkedForDeletionItemRefinement.pop();
			Itemset itemset = this._seTree.getRoot().getChildrenList().getElementByRefinement(refinement);
			if(itemset!=null){
				this.removeNode(itemset);
			}
			this._seTree.removeItem(refinement);
		}
		
		while(!this._lastAddedItemRefinement.isEmpty()){
			TriplesetRefinement refinement = this._lastAddedItemRefinement.pop();
			MultiKeyList keyList = refinement.getKeyList();
			float absoluteSupport = keyList.getSize();
			float relativeSupport = absoluteSupport/pane.getWindowOwner().getTargetObjectsCount();
			
			if(relativeSupport < super.getMinimumSupport()){
				System.out.println("1-Tripleset added as frequent when it's not");
			}else{
				if(!this._seTree.addItem(refinement)){
					System.out.println("1-Tripleset already present");
				}
			}	
		}
		
		for(Item item : this._seTree.getItemList()){
			TriplesetRefinement refinement = (TriplesetRefinement)item.getData();
			MultiKeyList mkList = refinement.getKeyList();
			KeyList kList = mkList.toKeyList();
			Itemset itemset = this._seTree.getRoot().getChildrenList().getElementByItem(item);
			if(itemset==null){
				itemset = this._seTree.getRoot().addChild(item);
				itemset.getSupport().setKeyList(kList);
				item.getSupport().setKeyList(kList);
				this._owner.getWindowOwner().getStreamOwner().fireFrequentPatternFound(itemset, this._owner.getWindowOwner());
				this._newFrequentCount++;
			}
			itemset.getSupport().setKeyList(kList);
			item.getSupport().setKeyList(kList);
		}
		
		super.updateSupports(pane);
		this._frequentCount = this._previousFrequentCount-this._newInfrequentCount+this._newFrequentCount;
		
		//event dispatching, fine analisi itemsets
		this._owner.getWindowOwner().getStreamOwner().fireFrequentTriplesetsUpdateCompleted(this._owner.getWindowOwner());
		//System.out.println("Deductive update completed reporting "+this._seTree.getRoot().getDescendantsCount()+" frequent itemsets!");
	}
	
	
	
	public Status getStatus(){
		return this._latticeStatus;
	}



	@Override
	public Iterator<Itemset> iterator() {
		return this._seTree.iterator();
	}

	
	
	@Override
	protected void removeNode(Itemset rootset){
		if(rootset.getLevel()!=0){
			Itemset currentSibling = rootset.getPreviousElement();
			while(currentSibling!=null){
				Itemset nodeToPrune = currentSibling.getChildrenList().getElementByItem(rootset.getData());
				if(nodeToPrune!=null){
					this.removeNode(nodeToPrune);
				}
				currentSibling = currentSibling.getPreviousElement();
			}
			this._newInfrequentCount += rootset.getDescendantsCount()+1;
			rootset.getAncestor().removeChild(rootset);
		}
	}


	@Override
	public Itemset bind(Pane pane, Itemset father, Itemset candidate) {
		Itemset childObject = null;
		
		int toCount = pane.getWindowOwner().getTargetObjectsCount();
		int comparation = father.getData().compareTo(candidate.getData());
		
		if(comparation<0){
			if(father.getLevel()<this._seTree.getDepth()){
				childObject = father.addChild(candidate.getData());
			}
			
			if(childObject!=null){
				//calcoliamo il supporto
				KeyList fatherList = (KeyList)father.getSupport().getKeyList();
				KeyList singletonList = (KeyList)candidate.getSupport().getKeyList();
				KeyList currentList = (KeyList)fatherList.intersect(singletonList);
				childObject.getSupport().setKeyList(currentList);
			
				//se il nodo � frequente dobbiamo espanderlo per verificare
				//che i pattern pi� specifici siano comunque frequenti
				float absoluteSupport = currentList.getSize();
				float relativeSupport = absoluteSupport/toCount;
				float threshold = super.getMinimumSupport();

				if(relativeSupport < threshold){
					father.removeChild(childObject);
					childObject = null;
				}else{
					this._owner.getWindowOwner().getStreamOwner().fireFrequentPatternFound(childObject, this._owner.getWindowOwner());
					this._frequentCount++;
				}
			}
		}
		
		return childObject;
	}


	@Override
	public Itemset check(Pane pane, Itemset father, Itemset candidate) {
		//aggiorno il supporto di candidate come figlio di father
		//sia father che candidate sono pattern frequenti, il figlio (father,candidate)
		//poteva essere frequente come infrequente: se non presente
		//bisogna verificare se � divenuto frequente, altrimenti bisogna aggiornare
		//la sua tidlist e verificare che non sia divenuto infrequente
		
		Itemset elementObject = null;
		
		if(father.getLevel()<this._seTree.getDepth()){
			int toCount = pane.getWindowOwner().getTargetObjectsCount();
			int comparation = father.getData().compareTo(candidate.getData());
			
			if(comparation<0){
				elementObject = father.getChildrenList().getElementByItem(candidate.getData());
				if(elementObject==null){
					//non era presente, provo ad aggiungerlo se possibile
					elementObject = father.addChild(candidate.getData());
					if(elementObject!=null){
						//calcoliamo il supporto
						KeyList fatherList = (KeyList)father.getSupport().getKeyList();
						KeyList singletonList = (KeyList)candidate.getSupport().getKeyList();
						KeyList currentList = (KeyList)fatherList.intersect(singletonList);
						elementObject.getSupport().setKeyList(currentList);
					
						//se il nodo � frequente dobbiamo espanderlo per verificare
						//che i pattern pi� specifici siano comunque frequenti
						float absoluteSupport = currentList.getSize();
						float relativeSupport = absoluteSupport/toCount;
						float threshold = super.getMinimumSupport();
						
						if(relativeSupport < threshold){
							father.removeChild(elementObject);
							elementObject = null;
						}else{
							this._owner.getWindowOwner().getStreamOwner().fireFrequentPatternFound(elementObject, this._owner.getWindowOwner());
							this._newFrequentCount++;
						}
					}
				}else{
					//era presente, aggiorno la tidlist
					KeyList fatherList = (KeyList)father.getSupport().getKeyList();
					KeyList singletonList = (KeyList)candidate.getSupport().getKeyList();
					KeyList currentList = (KeyList)fatherList.intersect(singletonList);
					elementObject.getSupport().setKeyList(currentList);
				
					//se il nodo � frequente dobbiamo espanderlo per verificare
					//che i pattern pi� specifici siano comunque frequenti
					float absoluteSupport = currentList.getSize();
					float relativeSupport = absoluteSupport/toCount;
					float threshold = super.getMinimumSupport();
					
					if(relativeSupport < threshold){
						this._newInfrequentCount += elementObject.getDescendantsCount()+1;
						father.removeChild(elementObject);
						elementObject = null;
					}
				}
			}
		}
		
		return elementObject;
	}

	
	public HarmonicLattice getHarmonicLatticeOwner(){
		return this._owner;
	}
	
}
