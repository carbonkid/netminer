package it.uniba.netminer.mining.lattices;

public class Status {

	private DeductiveLattice _owner;
	
	public Status(DeductiveLattice lattice){
		this._owner = lattice;
	}
	
	public long getCurrentFrequentItemsetsCount(){
		return this._owner._frequentCount;
	}
	
	public long getPreviouslyFrequentItemsetsCount(){
		return this._owner._previousFrequentCount;
	}
	
	public long getNewFrequentItemsetsCount(){
		return this._owner._newFrequentCount;
	}
	
	public long getNewInfrequentItemsetsCount(){
		return this._owner._newInfrequentCount;
	}
	
	public double getConceptChangeRate(){
		double numerator = (double)this.getNewFrequentItemsetsCount()+(double)this.getNewInfrequentItemsetsCount();
		double denominator = (double)this.getNewFrequentItemsetsCount()+(double)this.getPreviouslyFrequentItemsetsCount();
		if(numerator==0){
			return 0;
		}else{
			return numerator/denominator;
		}
	}

}
