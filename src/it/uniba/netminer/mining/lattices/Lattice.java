package it.uniba.netminer.mining.lattices;

import java.util.Iterator;

import it.uniba.netminer.mining.Pane;
import it.uniba.netminer.mining.setree.Itemset;
import it.uniba.netminer.mining.setree.SetEnumerationTree;

public abstract class Lattice implements Iterable<Itemset> {

	protected SetEnumerationTree _seTree;
	private float _minimumSupport;
	
	public Lattice(int depth, float minimumSupport){
		this._seTree = new SetEnumerationTree(depth);
		this._minimumSupport = minimumSupport;
	}
	
	public float getMinimumSupport(){
		return this._minimumSupport;
	}
	
	public int getMaximumDepth(){
		return this._seTree.getDepth();
	}
	
	public long getItemsetsCount(){
		return this._seTree.getRoot().getDescendantsCount();
	}
	
	public SetEnumerationTree getSetEnumerationTree(){
		return this._seTree;
	}
	
	
	public abstract void init(Pane pane);
	
	
	public abstract void update(Pane pane);
	
	
	public abstract Iterator<Itemset> iterator();
	
	
	
	public abstract Itemset check(Pane pane, Itemset father, Itemset candidate);
	
	public abstract Itemset bind(Pane pane, Itemset father, Itemset candidate);
	
	protected void removeNode(Itemset rootset){
		if(rootset.getLevel()!=0){
			Itemset currentSibling = rootset.getPreviousElement();
			while(currentSibling!=null){
				Itemset nodeToPrune = currentSibling.getChildrenList().getElementByItem(rootset.getData());
				if(nodeToPrune!=null){
					this.removeNode(nodeToPrune);
				}
				currentSibling = currentSibling.getPreviousElement();
			}
			rootset.getAncestor().removeChild(rootset);
		}
	}
	
	
	protected void generateSubTree(Pane pane, Itemset rootset){
		//preprocessiamo gli itemsets frequenti
		Itemset rootSet = rootset;
		Itemset nextBrother = rootset.getNextElement();
		while(nextBrother!=null){
			this.bind(pane, rootSet, nextBrother);
			nextBrother=nextBrother.getNextElement();
		}
		
		if(rootSet.hasChildren()){
			Itemset currentNode = rootSet.getChildrenList().getLastElement();
			Itemset currentSibling = currentNode;
			Itemset currentLastSibling = currentNode;
			while(currentNode!=rootSet){
				if(currentNode==currentSibling){
					if(currentNode.isFirstElement()){
						currentNode=currentNode.getAncestor();
						currentSibling=currentNode;
					}else{
						currentNode = currentNode.getPreviousElement();
						currentLastSibling = currentNode.getAncestor().getChildrenList().getLastElement();
						currentSibling = currentLastSibling;
					}
				}else{
					while(currentSibling!=currentNode){
						this.bind(pane, currentNode, currentSibling);
						currentSibling = currentSibling.getPreviousElement();
					}
	
					if(currentNode.hasChildren()){
						currentNode = currentNode.getChildrenList().getLastElement();
						currentSibling = currentNode;
						currentLastSibling = currentNode;
					}
				}
			}
		}
	}
	
	
	
	protected void updateSupports(Pane pane){
		Itemset rootSet = this._seTree.getRoot();
		if(rootSet.hasChildren()){
			Itemset currentNode = rootSet.getChildrenList().getLastElement();
			Itemset currentSibling = currentNode;
			Itemset currentLastSibling = currentNode;
			while(currentNode!=rootSet){
				if(currentNode==currentSibling){
					if(currentNode.isFirstElement()){
						currentNode=currentNode.getAncestor();
						currentSibling=currentNode;
					}else{
						currentNode = currentNode.getPreviousElement();
						currentLastSibling = currentNode.getAncestor().getChildrenList().getLastElement();
						currentSibling = currentLastSibling;
					}
				}else{
					while(currentSibling!=currentNode){
						this.check(pane, currentNode, currentSibling);
						currentSibling = currentSibling.getPreviousElement();
					}
	
					if(currentNode.hasChildren()){
						currentNode = currentNode.getChildrenList().getLastElement();
						currentSibling = currentNode;
						currentLastSibling = currentNode;
					}
				}
			}
		}
	}
}
