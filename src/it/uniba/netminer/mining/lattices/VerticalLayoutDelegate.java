package it.uniba.netminer.mining.lattices;

import java.util.HashSet;
import java.util.Set;

import it.uniba.netminer.dataaccess.DatabaseSession;
import it.uniba.netminer.dataaccess.QueryDelegate;
import it.uniba.netminer.dataschema.ConjunctiveCondition;
import it.uniba.netminer.dataschema.IDerivedAttribute;
import it.uniba.netminer.dataschema.IDerivedPrimaryKey;
import it.uniba.netminer.dataschema.IQueryable;
import it.uniba.netminer.dataschema.IView;
import it.uniba.netminer.dataschema.Restriction;
import it.uniba.netminer.mining.Pane;
import it.uniba.netminer.mining.StructuralNode;
import it.uniba.netminer.mining.setree.Item;
import it.uniba.netminer.mining.setree.MultiKey;
import it.uniba.netminer.mining.setree.MultiKeyList;

public class VerticalLayoutDelegate implements IQuerySingletonDelegate<MultiKeyList> {

	@Override
	public MultiKeyList evaluateTransactionList(Item item, Pane pane) {
		//costruzione della condizione di temporale
		IDerivedAttribute timingAttribute = pane.getWindowOwner().getStreamOwner().getTimingAttribute();
		ConjunctiveCondition timeCondition = new ConjunctiveCondition(timingAttribute);
		timeCondition.addRestriction(Restriction.greaterThanOrEqual(pane.getStartDate()));
		timeCondition.addRestriction(Restriction.lowerThanOrEqual(pane.getEndDate()));

		StructuralNode root = pane.getWindowOwner().getStreamOwner().getStructuralTree().getRoot();
		IView view = root.getData();
		IView viewLink = root.getFirstChild().getFirstChild().getData();
		IDerivedPrimaryKey linkPk = viewLink.getLastJoinedEntity().getDerivedPrimaryKey();

		//per ogni vista dell'albero strutturale valutiamo le keyList degli attributi (che non siano
		//uguali all'attributo target, una chiave primaria, riferiti/parte da/di una chiave esterna)
		//vanno valutati i patterns frequenti

		//Passaggio di processing:
		//valutiamo il primo livello, costruisco il query object sulla base dell'item
		IDerivedAttribute attribute = (IDerivedAttribute)item.getData().getCondition().getConditionatedAttribute();
		IQueryable patternQuery = attribute.getDerivedEntityOwner().getViewOwner().query();
		patternQuery.addCondition(item.getData().getCondition());
		IQueryable supportQuery = patternQuery.clone();

		//aggiungo la clausola per la chiave delle unit� di analisi
		supportQuery.addCondition(timeCondition);
		for(IDerivedAttribute keyAtt : view.getPrimaryKey()){
			supportQuery.addProjection(keyAtt, false);
		}
		//aggiungo la clausola per il distinct delle unit� dei links
		IDerivedAttribute conditionated = (IDerivedAttribute) item.getData().getCondition().getConditionatedAttribute();
		for(IDerivedAttribute pkAtt : linkPk){
			for(IDerivedAttribute keyAtt : conditionated.getDerivedEntityOwner().getViewOwner()){
				if(keyAtt.getOriginalAttribute() == pkAtt.getOriginalAttribute()){
					if(keyAtt.getName().equals(pkAtt.getName())){
						supportQuery.addProjection(keyAtt, false);
					}
				}
			}
		}

		//calcolo il supporto relativo
		QueryDelegate query = new QueryDelegate(supportQuery);
		DatabaseSession session = new DatabaseSession(query);
		session.dispatch();
		HashSet<Object> keyList = new HashSet<Object>(query.getResultSet());
		Set<MultiKey> keySet = new HashSet<MultiKey>();
		for(Object record : keyList){
			if(record instanceof Object[]){
				Object[] map = (Object[])record;
				Object netKey = map[0];
				Object linkKey = map[1];
				if(netKey!=null && linkKey!=null){
					keySet.add(new MultiKey(netKey,linkKey));
				}
			}
		}
		
		MultiKeyList multiKeyList = new MultiKeyList(keySet,0,1);	
		
		return multiKeyList;
	}

}
