package it.uniba.netminer.mining;

import it.uniba.netminer.mining.exceptions.PaneAlreadyAssignedException;
import it.uniba.netminer.mining.exceptions.PaneNotFilledException;

import java.util.Date;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Vector;

import javax.swing.tree.TreePath;

public class LandmarkWindow extends TimeWindow {
	private int _tosCount;
	private Vector<Pane> _basket;
	
	public LandmarkWindow(DataStream stream, int depth, float minimumSupport) {
		super(stream, depth, minimumSupport);
		this._tosCount = 0;
		this._basket = new Vector<Pane>(); 
	}

	@Override
	public Iterator<Pane> iterator() {
		return this._basket.iterator();
	}

	@Override
	public void addPane(Pane pane) throws PaneNotFilledException, PaneAlreadyAssignedException{
		if(pane.getWindowOwner()!=null){
			throw new PaneAlreadyAssignedException();
		}
		if(pane.isFull()){
			this._basket.add(pane);
			pane.setWindowOwner(this);
			this._tosCount += pane.getSize();
			
			//siccome possiamo solo aggiungere dei panes, � solo in questo punto che richiederemo
			//un aggiornamento del modello per JTree associato al data stream;
			Object[] pathNodeList = new Object[]{this.getStreamOwner(), this};
			TreePath path = new TreePath(pathNodeList);
			int index = this.indexOf(pane);
			
			//event dispatching
			super.getStreamOwner().fireNodeInserted(path, index, pane);
			super._owner.fireWindowChanged(this);
			super._owner.fireLatticeUpdateStarted(this);
			
			//lattice updating
			if(this.getPaneCount()==1){
				super._lattice.init(pane);
			}else if(this.getPaneCount()>1){
				super._lattice.update(pane);
			}
			
			//event dispatching
			super._owner.fireLatticeUpdateCompleted(this);
		}else{
			throw new PaneNotFilledException();
		}
	}

	@Override
	public int getPaneCount() {
		return this._basket.size();
	}

	@Override
	public int getTargetObjectsCount() {
		return this._tosCount;
	}

	@Override
	public Date getStartDate() {
		Date date = null;
		try{
			Pane firstPane = this._basket.firstElement();
			date = firstPane.getStartDate();
		}catch(NoSuchElementException ex){}
		return date;
	}

	@Override
	public Date getEndDate() {
		Date date = null;
		try{
			Pane lastPane = this._basket.lastElement();
			date = lastPane.getEndDate();
		}catch(NoSuchElementException ex){}
		return date;
	}

	
	
	@Override
	public int indexOf(Pane pane) {
		return this._basket.indexOf(pane);
	}
	
	@Override
	public String toString(){
		Date startDate = this.getStartDate();
		Date endDate = this.getEndDate();
		String startStringDate = "n/a";
		String endStringDate ="n/a";
		if(startDate!=null){
			startStringDate = startDate.toString();
		}
		if(endDate!=null){
			endStringDate = endDate.toString();
		}
		return "Landmark Time Window ("+startStringDate+", "+endStringDate+")";
	}

	@Override
	public Pane getPaneAt(int index) {
		return this._basket.get(index);
	}

	@Override
	public Pane getFirstPane() {
		return this._basket.firstElement();
	}

	@Override
	public Pane getLastPane() {
		return this._basket.lastElement();
	}
	
}
