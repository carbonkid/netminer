package it.uniba.netminer.mining;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

import it.uniba.netminer.dataaccess.DatabaseSession;
import it.uniba.netminer.dataaccess.QueryDelegate;
import it.uniba.netminer.dataschema.IAttribute;
import it.uniba.netminer.dataschema.IDerivedAttribute;
import it.uniba.netminer.dataschema.IDerivedEntity;
import it.uniba.netminer.dataschema.IQueryable;
import it.uniba.netminer.dataschema.IView;

public class TargetObjectFactory implements IGenerable<TargetObject> {
	
	private IDerivedEntity _derivedTargetEntity;
	private IDerivedAttribute _derivedTargetAttribute;
	private SelectionValuesFactory _discretizer;
	private StructuralTree _structuralTree;
	
	
	public TargetObjectFactory(StructuralTree structuralTree, IAttribute timingAttribute){
		this._structuralTree = structuralTree;
		this._discretizer = new SelectionValuesFactory();
		
		//trovo l'attributoTemporale
		IView rootView = this._structuralTree.getRoot().getData();
						
		//in generale l'attributo passato � una attributo semplice
		//dobbiamo localizzare il derived attribute associato nella radice dell'albero strutturale
		for(IDerivedAttribute attribute : rootView){
			if(attribute.getOriginalAttribute() == timingAttribute){
				this._derivedTargetAttribute = attribute;
				this._derivedTargetEntity = attribute.getDerivedEntityOwner();
				break;
			}
		}
	}
	
	
	public StructuralTree getStructuralTree(){
		return this._structuralTree;
	}
	
	public SelectionValuesFactory getDiscretizer(){
		return this._discretizer;
	}
	
	
	
	public IDerivedEntity getDerivedTimingTargetEntity(){
		return this._derivedTargetEntity;
	}
	
	public IDerivedAttribute getDerivedTimingAttribute(){
		return this._derivedTargetAttribute;
	}

	@Override
	public IGenerator<TargetObject> generator() {
		return new TargetObjectGenerator();
	}
	
	
	private class TargetObjectGenerator implements IGenerator<TargetObject>{
		
		private IQueryable _firstView;
		private int _lastIndex;
		
		public TargetObjectGenerator(){
			this._lastIndex = 0;
			this._firstView = _structuralTree.getRoot().getData().query().clone();
		}

		@Override
		public TargetObject next() {
			this._firstView.setLimitIndex(this._lastIndex);
			this._firstView.setLimitCount(1);
			QueryDelegate delegate = new QueryDelegate(this._firstView);
			DatabaseSession session = new DatabaseSession(delegate);
			session.dispatch();
			
			List<?> results = delegate.getResultSet();
			TargetObject object = null;
			if(results!=null){
				HashMap row = (HashMap) results.get(0);
				Date date = (Date)row.get(_derivedTargetAttribute.getOriginalAttribute().getName());
				object = new TargetObject(date);
				this._lastIndex++;
			}

			return object;
		}

		@Override
		public boolean canGenerateNext() {
			//se ci sono altre unit� di analisi nel database, allora posso generare l'id univoco.
			boolean retval = true;
			return retval;
		}
		
	}
	
}
