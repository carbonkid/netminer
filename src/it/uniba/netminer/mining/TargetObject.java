package it.uniba.netminer.mining;

import java.sql.Date;

public class TargetObject {
	
	private Pane _paneOwner;
	private Date _date;
	
	public TargetObject(Date date){
		this._date = date;
		this._paneOwner = null;
	}
	
	
	public Date getDate(){
		return this._date;
	}

	protected void setPaneOwner(Pane pane){
		this._paneOwner = pane;
	}
	
	public Pane getPaneOwner(){
		return this._paneOwner;
	}
	
	@Override
	public String toString(){
		return this.getDate().toString();
	}
	
}
