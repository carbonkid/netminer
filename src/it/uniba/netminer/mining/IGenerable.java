package it.uniba.netminer.mining;

public interface IGenerable<T> {
	public IGenerator<T> generator();
}
