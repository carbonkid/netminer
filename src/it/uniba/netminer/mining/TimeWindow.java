package it.uniba.netminer.mining;

import it.uniba.netminer.mining.exceptions.PaneAlreadyAssignedException;
import it.uniba.netminer.mining.exceptions.PaneNotFilledException;
import it.uniba.netminer.mining.lattices.HarmonicLattice;
import it.uniba.netminer.mining.lattices.IQuerySingletonDelegate;
import it.uniba.netminer.mining.lattices.Refinement;
import it.uniba.netminer.mining.setree.MultiKeyList;

import java.util.Date;
import java.util.Iterator;
import java.util.List;


public abstract class TimeWindow implements Iterable<Pane> {

	protected DataStream _owner;
	protected HarmonicLattice _lattice;
	
	
	public TimeWindow(DataStream stream, int depth, float minimumSupport){
		if(stream==null){
			throw new NullPointerException();
		}
		
		this._owner = stream;
		List<Refinement> singletonList = this._owner.getSingletonList();  
		this._lattice = new HarmonicLattice(this, singletonList, depth, minimumSupport);
	}
	
	public DataStream getStreamOwner(){
		return this._owner;
	}
	
	public HarmonicLattice getHarmonicLattice(){
		return this._lattice;
	}
	
	@Override
	public abstract Iterator<Pane> iterator();
	public abstract int indexOf(Pane pane);
	public abstract void addPane(Pane pane) throws PaneNotFilledException, PaneAlreadyAssignedException;
	public abstract Pane getPaneAt(int index);
	public abstract Pane getFirstPane();
	public abstract Pane getLastPane();
	public abstract int getPaneCount();
	public abstract int getTargetObjectsCount();
	public abstract Date getStartDate();
	public abstract Date getEndDate();
}
