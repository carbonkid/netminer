package it.uniba.netminer.mining;

public class LatticeStatus {

	private long _newlyInfrequent;
	private long _newlyFrequent;
	private long _oldFrequent;
	private long _nowFrequent;
	
	public LatticeStatus(long newlyFreq, long newlyInf, long oldFreq, long nowFreq){
		this._newlyFrequent = newlyFreq;
		this._newlyInfrequent = newlyInf;
		this._oldFrequent = oldFreq;
		this._nowFrequent = nowFreq;
	}
	
	
	public long getNewFrequentCount(){
		return this._newlyFrequent;
	}
	
	public long getNewInfrequentCount(){
		return this._newlyInfrequent;
	}
	
	public long getTotalOldFrequentCount(){
		return this._oldFrequent;
	}
	
	public long getTotalFrequentCount(){
		return this._nowFrequent;
	}
	
	public double getFrequentChange(){
		double changed = this._newlyFrequent + this._newlyInfrequent;
		double denominator = this._oldFrequent + this._newlyFrequent;
		return changed/denominator;
	}

}
