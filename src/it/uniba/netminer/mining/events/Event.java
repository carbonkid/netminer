package it.uniba.netminer.mining.events;

public class Event {
	
	private Object _source;
	
	public Event(Object source){
		this._source = source;
	}
	
	public Object getSource(){
		return this._source;
	}
}
