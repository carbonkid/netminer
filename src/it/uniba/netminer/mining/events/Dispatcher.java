package it.uniba.netminer.mining.events;

import java.util.Iterator;
import java.util.Stack;

public abstract class Dispatcher<G extends Listener<? extends Event>>{

	protected Stack<G> _listeners;
	
	public Dispatcher(){
		this._listeners = new Stack<G>();
	}
	
	public void addListener(G listener) {
		if(!this.hasListener(listener)){
			this._listeners.push(listener);
		}
	}

	
	
	
	public boolean hasListener(G listener) {
		return this._listeners.contains(listener);
	}

	
	
	
	
	public void removeListener(G listener) {
		this._listeners.remove(listener);
	}
	
	
	
	public Iterator<G> getListenerIterator(){
		return this._listeners.iterator();
	}
	
}
