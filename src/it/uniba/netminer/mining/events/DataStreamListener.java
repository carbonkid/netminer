package it.uniba.netminer.mining.events;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import it.uniba.netminer.mining.DataStream;
import it.uniba.netminer.mining.Pane;
import it.uniba.netminer.mining.TimeWindow;
import it.uniba.netminer.mining.lattices.Status;
import it.uniba.netminer.mining.setree.Itemset;

public class DataStreamListener extends Listener<DataStreamEvent>{

	private long _timeQueryTask;
	private long _timeUpdateItemsetsTask;
	private long _timeUpdateTriplesetsTask;
	private long _timeSearchChangesTask;
	private int _changePatternsCount;
	private int _changePatternsCandidateCount;
	private TimeWindow _currentTimeWindow;
	private boolean _isAnalyzingWindow;
	
	private OutputStream _outputStream;
	private XMLStreamWriter _writer;
	
	public DataStreamListener(){
		this._timeQueryTask = 0;
		this._timeUpdateItemsetsTask = 0;
		this._timeUpdateTriplesetsTask = 0;
		this._timeSearchChangesTask = 0;
		this._isAnalyzingWindow = false;
		this._changePatternsCandidateCount = 0;
		this._changePatternsCount = 0;
	}

	
	/**
	 * funzione di callback chiamata quando lo stream riceve la prima 
	 * unit� di analisi.
	 * 
	 * @param event
	 */
	public void streamStarted(DataStreamEvent event) {
		DataStream stream = event.getDataStream();
		
		//ottengo i parametri di input del processo di mining
		float minimumSupport = stream.getRecentPastTimeWindow().getHarmonicLattice().getMinimumSupport();
		double minimumFChange = stream.getMinimumFrequencyChange();
		double minimumGrowthRate = stream.getMinimumGrowthRate();
		int maximumLatticeDepth = stream.getRecentPastTimeWindow().getHarmonicLattice().getDeductiveLattice().getMaximumDepth();
		int paneSize = stream.getPaneSize();
		int initialRecentPastSize = stream.getRecentPastSize();
		int initialRemotePastSize = stream.getRemotePastSize();
		
		//apro il file xml
		System.out.println("stream started");
		try {
			this._outputStream = new FileOutputStream(new File("results/result.xml"));
			this._writer = XMLOutputFactory.newInstance().createXMLStreamWriter(
				new OutputStreamWriter(this._outputStream, "utf-8")
			);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//li scrivo nel primo elemento xml
		try {
			this._writer.writeStartDocument();
			this._writer.writeStartElement("miningProcess");
			this._writer.writeStartElement("miningParams");
				this._writer.writeStartElement("minimumSupport");
				this._writer.writeCharacters(Float.toString(minimumSupport));
				this._writer.writeEndElement();
				this._writer.writeStartElement("minimumFChange");
				this._writer.writeCharacters(Double.toString(minimumFChange));
				this._writer.writeEndElement();
				this._writer.writeStartElement("minimumGrowthRate");
				this._writer.writeCharacters(Double.toString(minimumGrowthRate));
				this._writer.writeEndElement();
				this._writer.writeStartElement("maximumLatticeDepth");
				this._writer.writeCharacters(Integer.toString(maximumLatticeDepth));
				this._writer.writeEndElement();
				this._writer.writeStartElement("numberOfTargetObjectsPerPane");
				this._writer.writeCharacters(Integer.toString(paneSize));
				this._writer.writeEndElement();
				this._writer.writeStartElement("numberOfInitialPanesInRecentPast");
				this._writer.writeCharacters(Integer.toString(initialRecentPastSize));
				this._writer.writeEndElement();
				this._writer.writeStartElement("numberOfInitialPanesInRemotePast");
				this._writer.writeCharacters(Integer.toString(initialRemotePastSize));
				this._writer.writeEndElement();
			this._writer.writeEndElement();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	

	/**
	 * funzione di callback chiamata periodicamente quando lo stream 
	 * resta in attesa di nuove unit� di analisi.
	 * 
	 * @param event
	 */
	public void streamIdle(DataStreamEvent event){
		/*
		 * alla prima attesa chiudo lo stream (ipotesi non realistica in un
		 * contesto reale - � di utilit� semplicemente per chiudere il file dei
		 * risultati).
		 */
		DataStream stream = event.getDataStream();
		stream.close();
		
		//chiudo il tag root e il documento xml
		try {
			this._writer.writeEndElement();
			this._writer.writeEndDocument();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		
		//chiudo il file
		try {
			this._writer.flush();
			this._writer.close();
			this._outputStream.flush();
			this._outputStream.close();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	
	
	/**
	 * funzione di callback chiamata non appena lo stream riceve un nuovo
	 * pane di unit� di analisi in una delle sue due finestre temporali.
	 * 
	 * @param event
	 */
	public void windowChanged(DataStreamEvent event){
		DataStream streamChanged = event.getDataStream();
		TimeWindow windowChanged = event.getWindowChanged();
		Pane lastPane = windowChanged.getLastPane();
		System.out.println("adding pane to: "+windowChanged);
		
		
		try {
			if(this._isAnalyzingWindow){
				this._writer.writeStartElement("stats");
					this._writer.writeEmptyElement("queryPhase");
					this._writer.writeAttribute("completedIn", Long.toString(this._timeQueryTask));
					this._writer.writeEmptyElement("itemsetsUpdatePhase");
					this._writer.writeAttribute("completedIn", Long.toString(this._timeUpdateItemsetsTask));
					this._writer.writeEmptyElement("triplesetsUpdatePhase");
					this._writer.writeAttribute("completedIn", Long.toString(this._timeUpdateTriplesetsTask));
					this._writer.writeEmptyElement("changePatternSearchPhase");
					this._writer.writeAttribute("completedIn", Long.toString(this._timeSearchChangesTask));
				this._writer.writeEndElement();
				this._writer.writeEndElement();
				this._isAnalyzingWindow = false;
			}
			
			String windowName = "";
			if(windowChanged == streamChanged.getRecentPastTimeWindow()){
				windowName = "RecentPast";
			}else if(windowChanged == streamChanged.getRemotePastTimeWindow()){
				windowName = "RemotePast";
			}
			
			String operationPerformed = "";
			if(windowChanged.getPaneCount()==1){
				operationPerformed = "initialization";
			}else{
				operationPerformed = "update";
			}
			
			this._writer.writeStartElement(operationPerformed+windowName);
			this._writer.writeAttribute("pane", lastPane.getStartDate()+" "+lastPane.getEndDate());
			this._writer.writeAttribute("window", windowChanged.getStartDate()+" "+windowChanged.getEndDate());
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this._isAnalyzingWindow = true;
		
		this._currentTimeWindow = windowChanged;
	}
	
	
	
	/**
	 * funzione di callback chiamata a seguito dell'aggiunta di un nuovo
	 * pane in una finestra temporale dello stream; anticipa immediatamente
	 * l'inizio del processo completo di aggiornamento del reticolo (itemsets
	 * + triplesets) dei pattern frequenti per una finestra temporale.
	 * 
	 * @param event
	 */
	public void latticeUpdateStarted(DataStreamEvent event){
		
	}
	
	
	
	
	/**
	 * Funzione di callback chiamata all'inizio delle operazioni di interrogazione
	 * del layout verticale dello stream, operazione necessaria ad ottenere le
	 * tidlists per gli 1-itemsets.
	 * 
	 * @param event
	 */
	public void queriesStarted(DataStreamEvent event){
		this._timeQueryTask = System.currentTimeMillis();
	}
	
	
	
	
	/**
	 * Funzione di callback chiamata al termine delle operazioni di interrogazione 
	 * del layout verticale dello stream, operazione necessaria ad ottenere le 
	 * tidlists per gli 1-itemsets.
	 * 
	 * @param event
	 */
	public void queriesCompleted(DataStreamEvent event){
		this._timeQueryTask = System.currentTimeMillis() - this._timeQueryTask;
	}
	
	
	
	
	/**
	 * Funzione di callback chiamata all'inizio del processo di aggiornamento 
	 * del reticolo armonico (soli itemsets) dei pattern frequenti per una finestra 
	 * temporale.
	 * 
	 * @param event
	 */
	public void frequentItemsetsUpdateStarted(DataStreamEvent event){
		this._timeUpdateItemsetsTask = System.currentTimeMillis();
	}
	
	
	
	
	/**
	 * Funzione di callback chiamata al termine del processo di aggiornamento
	 * del reticolo armonico (soli itemsets) dei pattern frequenti per una finestra
	 * temporale.
	 * 
	 * @param event
	 */
	public void frequentItemsetsUpdateCompleted(DataStreamEvent event){
		this._timeUpdateItemsetsTask = System.currentTimeMillis() - this._timeUpdateItemsetsTask;
	}

	
	
	
	/**
	 * Funzione di callback chiamata all'inizio del processo di aggiornamento 
	 * del reticolo deduttivo (soli triplesets) dei pattern frequenti per una finestra 
	 * temporale.
	 * 
	 * @param event
	 */
	public void frequentTriplesetsUpdateStarted(DataStreamEvent event){
		this._timeUpdateTriplesetsTask = System.currentTimeMillis();
	}
	
	
	
	/**
	 * Funzione di callback chiamata al termine del processo di aggiornamento
	 * del reticolo deduttivo (soli triplesets) dei pattern frequenti per una finestra
	 * temporale.
	 * 
	 * @param event
	 */
	public void frequentTriplesetsUpdateCompleted(DataStreamEvent event){
		this._timeUpdateTriplesetsTask = System.currentTimeMillis() - this._timeUpdateTriplesetsTask;
	}
	
	
	
	
	/**
	 * funzione di callback chiamata al termine del processo completo 
	 * di aggiornamento del reticolo (itemsets + triplesets) dei pattern
	 * frequenti per una finestra temporale.
	 * 
	 * @param event
	 */
	public void latticeUpdateCompleted(DataStreamEvent event){
		//verifichiamo l'fchange
		DataStream streamChanged = event.getDataStream();
		TimeWindow windowChanged = event.getWindowChanged();
		Pane lastPane = windowChanged.getLastPane();
		Status status = windowChanged.getHarmonicLattice().getStatus();
		
		
		if(windowChanged == streamChanged.getRecentPastTimeWindow()){
			double fchange = status.getConceptChangeRate()*100;
			System.out.println("registered concept change rate of "+fchange+"%");
			
			if(fchange > streamChanged.getMinimumFrequencyChange()){
				//ricerca dei pattern emergenti
				streamChanged.searchChangePattern();
				
				//swap delle finestre temporali
				streamChanged.swap();
			}
		}
	}


	/**
	 * Funzione di callback chiamata al presentarsi di un nuovo pattern frequente
	 * (itemset o tripleset).
	 * @param event
	 */
	public void frequentPatternFound(DataStreamEvent event){
		Itemset itemset = event.getFrequentItemsetFound();
		
		/*
		float support = itemset.getSupport().getKeyList().getSize()/this._currentTimeWindow.getTargetObjectsCount();
		
		try {
			this._writer.writeStartElement("frequentPattern");
				this._writer.writeAttribute("support", Float.toString(support*100));
				this._writer.writeCharacters(itemset.getStringRappresentation());
			this._writer.writeEndElement();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	
	/**
	 * Funzione di callback chiamata all'inizio del processo di estrazione
	 * dei pattern di cambiamento dal passato recente rispetto al passato remoto.
	 * 
	 * @param event
	 */
	public void changesInspectionStarted(DataStreamEvent event) {
		this._timeSearchChangesTask = System.currentTimeMillis();
	}
	
	
	
	
	/**
	 * Funzione di callback chiamata al termine del processo di estrazione
	 * dei pattern di cambiamento dal passato recente rispetto al passato remoto.
	 * 
	 * @param event
	 */
	public void changesInspectionCompleted(DataStreamEvent event) {
		this._timeSearchChangesTask = System.currentTimeMillis() - this._timeSearchChangesTask;
	}

	
	

	/**
	 * Funzione di callback chiamata al fallimento del test di eccedenza del growth-rate
	 * per i pattern di cambiamento candidati che hanno gi� superato il test strutturale.
	 * 
	 * @param event
	 */
	public void invalidChangePatternFound(DataStreamEvent event){
		ChangePatternProxy changePattern = event.getChangePatternFound();
		this._changePatternsCandidateCount++;
		/*
		try {
			this._writer.writeStartElement("changePattern");
				this._writer.writeAttribute("growthRate", Double.toString(changePattern.getGrowthRate()*100));
				this._writer.writeStartElement("pattern");
					this._writer.writeCharacters(changePattern.getEmergentItemset().getStringRappresentation());
				this._writer.writeEndElement();
				this._writer.writeStartElement("respectTo");
					this._writer.writeCharacters(changePattern.getComparedItemset().getStringRappresentation());
				this._writer.writeEndElement();
			this._writer.writeEndElement();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}

	
	
	
	/**
	 * Funzione di callback chiamata al superamento del test di eccedenza del growth-rate
	 * per i pattern di cambiamento candidati che hanno gi� superato il test strutturale.
	 * Il pattern candidato gestito dall'evento � a tutti gli effetti un pattern di cambiamento
	 * dal passato recente rispetto al passato remoto.
	 * 
	 * @param event
	 */
	public void validChangePatternFound(DataStreamEvent event){
		ChangePatternProxy changePattern = event.getChangePatternFound();
		this._changePatternsCount++;
		/*
		try {
			this._writer.writeStartElement("invalidCandidate");
				this._writer.writeAttribute("growthRate", Double.toString(changePattern.getGrowthRate()*100));
				this._writer.writeStartElement("pattern");
					this._writer.writeCharacters(changePattern.getEmergentItemset().getStringRappresentation());
				this._writer.writeEndElement();
				this._writer.writeStartElement("respectTo");
					this._writer.writeCharacters(changePattern.getComparedItemset().getStringRappresentation());
				this._writer.writeEndElement();
			this._writer.writeEndElement();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}	
}