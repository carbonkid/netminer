package it.uniba.netminer.mining.events;

import it.uniba.netminer.mining.DataStream;
import it.uniba.netminer.mining.TimeWindow;
import it.uniba.netminer.mining.setree.Itemset;


public class DataStreamEvent extends Event {
	
	private TimeWindow _windowChanged;
	private Itemset _itemset;
	private ChangePatternProxy _changePattern;

	public DataStreamEvent(DataStream source) {
		super(source);
	}
	
	
	public DataStreamEvent(DataStream source, TimeWindow window){
		this(source);
		this._windowChanged = window;
	}
	
	public DataStreamEvent(DataStream source, TimeWindow window, ChangePatternProxy changePattern){
		this(source, window);
		this._changePattern = changePattern;
	}
	
	public DataStreamEvent(DataStream source, TimeWindow window, Itemset frequentItemset){
		this(source, window);
		this._itemset = frequentItemset;
	}
	
	public DataStream getDataStream(){
		return (DataStream)this.getSource();
	}
	
	public TimeWindow getWindowChanged(){
		return this._windowChanged;
	}
	
	public Itemset getFrequentItemsetFound(){
		return this._itemset;
	}
	
	public ChangePatternProxy getChangePatternFound(){
		return this._changePattern;
	}
	
	
}
