package it.uniba.netminer.mining.events;

import it.uniba.netminer.mining.setree.Itemset;

public class ChangePatternProxy {
	private Itemset _emergentItemset;
	private Itemset _comparedItemset;
	private double _growthRate;
	
	public ChangePatternProxy(Itemset emergentPattern, Itemset respectTo, double growthRate){
		this._emergentItemset = emergentPattern;
		this._comparedItemset = respectTo;
		this._growthRate = growthRate;
	}
	
	public double getGrowthRate(){
		return this._growthRate;
	}
	
	public Itemset getEmergentItemset(){
		return this._emergentItemset;
	}
	
	public Itemset getComparedItemset(){
		return this._comparedItemset;
	}
}
