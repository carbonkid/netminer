package it.uniba.netminer.mining;


import java.util.LinkedList;
import java.util.List;

import it.uniba.netminer.dataschema.IAttribute;
import it.uniba.netminer.dataschema.IDerivedAttribute;
import it.uniba.netminer.dataschema.IEntity;
import it.uniba.netminer.dataschema.IPrimaryKey;
import it.uniba.netminer.dataschema.IRelationship;
import it.uniba.netminer.exceptions.InvalidAttributeException;
import it.uniba.netminer.exceptions.InvalidEntityException;
import it.uniba.netminer.exceptions.InvalidRelationshipException;
import it.uniba.netminer.mining.StructuralNode.StructuralNodeType;
import it.uniba.netminer.mining.events.DataStreamListener;
import it.uniba.netminer.mining.lattices.Refinement;

public class MiningProcess extends Thread{

	private TargetObjectFactory _factory;
	private DataStream _dataStream;

	public MiningProcess(
			IEntity targetEntity, 
			IAttribute timingAttribute,
			IAttribute edgeTypeAttribute,
			IRelationship edgePath,
			List<IRelationship> firstNodePath,
			List<IRelationship> secondNodePath,
			float minimumSupport,
			float minimumFChange,
			float minimumGrowthRate,
			int maximumDepth,
			int paneSize,
			int remotePastWindowSize,
			int recentPastWindowSize){
		
		if(targetEntity == null){
			throw new InvalidEntityException();
		}
		if(edgePath==null || firstNodePath==null || secondNodePath==null){
			throw new InvalidRelationshipException();
		}
		if(timingAttribute == null){
			throw new InvalidAttributeException();
		}
		
		
		//costruiamo lo scheletro dell'albero strutturale 
		StructuralTree structuralTree = new StructuralTree();
		try{
			StructuralNode root = structuralTree.addChild(targetEntity);
			StructuralNode edgeStep = structuralTree.addChild(root, edgePath, StructuralNodeType.NEW_FK_EXHIBITED);
			
			StructuralNode firstNodeStep = edgeStep;
			for(IRelationship stepToFirstNode : firstNodePath){
				firstNodeStep = structuralTree.addChild(firstNodeStep, stepToFirstNode, StructuralNodeType.NEW_FK_EXHIBITED);
			}
			
			StructuralNode secondNodeStep = edgeStep;
			for(IRelationship stepToSecondNode : secondNodePath){
				secondNodeStep = structuralTree.addChild(secondNodeStep, stepToSecondNode, StructuralNodeType.NEW_FK_EXHIBITED);
			}
		}catch(InvalidRelationshipException ex){
			ex.printStackTrace();
		}
		
		
		
		//possiamo inizializzare la fabbrica di unit� di analisi
		this._factory = new TargetObjectFactory(structuralTree, timingAttribute);
		SelectionValuesFactory discretizer = this._factory.getDiscretizer();
		
		//dobbiamo discretizzare soltanto: l'identificatore del primo nodo, l'identificatore
		//del secondo nodo, le caratteristiche dell'arco
		IEntity firstNodeEntity = firstNodePath.get(firstNodePath.size()-1).getRelatedEntity();
		IEntity secondNodeEntity = secondNodePath.get(firstNodePath.size()-1).getRelatedEntity();
		IPrimaryKey firstNodePK = firstNodeEntity.getPrimaryKey();
		IPrimaryKey secondNodePK = secondNodeEntity.getPrimaryKey();
		for(IAttribute attribute : firstNodePK){
			discretizer.addAttribute(attribute);
		}
		for(IAttribute attribute : secondNodePK){
			discretizer.addAttribute(attribute);
		}
		discretizer.addAttribute(edgeTypeAttribute);
		discretizer.discretize();
		
		
		//creo gli 1-itemsets;
		LinkedList<IDerivedAttribute> attributeToConsider = new LinkedList<IDerivedAttribute>();
		List<Refinement> singletonList = new LinkedList<Refinement>();
		
		//Passaggio di preprocessing:
		//raffiniamo ogni attributo dello spazio se non implicato in chiavi primarie o chiavi esterne
		boolean alreadyFound = false;
		for(IDerivedAttribute attribute : structuralTree.getLinearized()){
			if(attribute.isInvolvedInPrimaryKey()){
				if(attribute.getEntityOwner() == firstNodeEntity){
					attributeToConsider.add(attribute);
				}else if(attribute.getEntityOwner() == secondNodeEntity){
					attributeToConsider.add(attribute);
				}
			}else{
				if(attribute.getOriginalAttribute() == edgeTypeAttribute && !alreadyFound){
					attributeToConsider.add(attribute);
					alreadyFound = true;
				}
			}
		}
				
		//Passaggio di processing:
		//costruiamo i raffinamenti di selzione di primo livello
		for(IDerivedAttribute attribute : attributeToConsider){
			if(discretizer.hasGenerator(attribute)){
				IGenerator<Refinement> generator = discretizer.getGenerator(attribute).generator();
							
				//aggiungiamo un 1-itemset alla lista degli 1-itemsets da considerare
				while(generator.canGenerateNext()){
					Refinement refinement = generator.next();
					singletonList.add(refinement);
				}
			}
		}
		
		
		//inizializziamo il data stream e i reticoli associati sulla base degli 1-itemsets trovati
		this._dataStream = new DataStream(
				this._factory, 
				singletonList, 
				paneSize,
				remotePastWindowSize,
				recentPastWindowSize,
				minimumSupport, 
				minimumFChange, 
				minimumGrowthRate, 
				maximumDepth
			);
		this._dataStream.addListener(new DataStreamListener());
	}
	
	
	

	@Override
	public void run() {
		while(true){
			if(this._dataStream.isActive()){
				this._dataStream.consume();
			}else{
				break;
			}
		}
	}
	
	
	public DataStream getDataStream(){
		return this._dataStream;
	}
	

}
