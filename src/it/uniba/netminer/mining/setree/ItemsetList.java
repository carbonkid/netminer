package it.uniba.netminer.mining.setree;



import java.util.Iterator;
import java.util.SortedSet;

public class ItemsetList extends ElementSet<Itemset, Item> {
	
	protected ItemsetList(){
		super();
	}
	
	
	public SortedSet<Itemset> getSubset(Item lowerBound, Item upperBound){
		Itemset upperLimit = new Itemset(upperBound);
		Itemset lowerLimit = new Itemset(lowerBound);
		if(lowerBound==null && upperBound!=null){
			return this._set.headSet(upperLimit, true);
		}else if(lowerBound!=null && upperBound==null){
			return this._set.tailSet(lowerLimit, true);
		}else if(lowerBound!=null && upperBound!=null){
			return this._set.subSet(lowerLimit, false, upperLimit, false);
		}else{
			throw new NullPointerException();
		}
		
	}
	
	
	public boolean hasElementByRefinement(IRefinement refinement){
		Item item = new Item(null, refinement);
		return this.hasElementByItem(item);
	}
	
	public Itemset getElementByRefinement(IRefinement refinement){
		Item item = new Item(null, refinement);
		return this.getElementByItem(item);
	}
	
	
	public boolean hasElementByItem(Item item){
		Itemset itemset = new Itemset(item);
		return this.hasElement(itemset);
	}
	
	public Itemset getElementByItem(Item item){
		Itemset itemset = new Itemset(item);
		return this.getElement(itemset);
	}
	
	@Override
	protected boolean add(Itemset node){
		return super.add(node);
	}
	
	
	@Override
	public boolean remove(Itemset singleton){
		return super.remove(singleton);
	}
	
	public Itemset getFirstElement(){
		return this._set.first();
	}
	
	@Override
	public Itemset getLastElement(){
		return this._set.last();
	}

	@Override
	public Iterator<Itemset> iterator() {
		return new ElementSetIterator<Itemset, Item>(this, true);
	}
	
	public Iterator<Itemset> descendingIterator(){
		return new ElementSetIterator<Itemset, Item>(this, false);
	}

}
