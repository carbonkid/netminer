package it.uniba.netminer.mining.setree;


public class Support {
	
	private ITidList _tidList;

	protected Support(){
		this._tidList = null;
	}
	
	public ITidList getKeyList(){
		return this._tidList;
	}
	
	public void setKeyList(ITidList tidlist){
		this._tidList = tidlist;
	}

}
