package it.uniba.netminer.mining.setree;

public interface ITidList {
	public float getSize();
	public ITidList intersect(ITidList tidlist);
	public void integrate(ITidList recentData);
}
