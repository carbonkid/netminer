package it.uniba.netminer.mining.setree;

public class MultiKey {

	protected Object _toKey;
	protected Object _ntoKey;
	
	public MultiKey(Object toKey, Object ntoKey){
		this._toKey = toKey;
		this._ntoKey = ntoKey;
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj instanceof MultiKey){
			MultiKey mk = (MultiKey)obj;
			return this._toKey.equals(mk._toKey) && this._ntoKey.equals(mk._ntoKey);
		}else{
			throw new ClassCastException();
		}
	}
	
	@Override
	public int hashCode(){
		int hash = 1;
        hash = hash * 17 + this._toKey.hashCode();
        hash = hash * 31 + this._ntoKey.hashCode();
        return hash;
	}

}
