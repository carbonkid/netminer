package it.uniba.netminer.mining.setree;

public class Element<T extends Comparable<T>>{

	protected Element<T> _nextElement;
	protected Element<T> _previousElement;
	protected T _data;
	
	protected Element(T data){
		this._data = data;
	}
	
	public T getData(){
		return this._data;
	}
	
	public Element<T> getNextElement(){
		return this._nextElement;
	}
	
	public Element<T> getPreviousElement(){
		return this._previousElement;
	}
	
	public boolean isLastElement(){
		return this._nextElement==null;
	}
	
	public boolean isFirstElement(){
		return this._previousElement==null;
	}
	

	protected void setNextElement(Element<T> element) {
		this._nextElement = element;
	}
	
	protected void setPreviousElement(Element<T> element) {
		this._previousElement = element;
	}
	
}
