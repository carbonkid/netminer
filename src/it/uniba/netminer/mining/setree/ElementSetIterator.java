package it.uniba.netminer.mining.setree;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ElementSetIterator<T extends Element<C>, C extends Comparable<C>> implements Iterator<T> {

	private ElementSet<T,C> _container;
	private Element<C> _currentEdge;
	private boolean _isAscending;
	
	protected ElementSetIterator(ElementSet<T,C> container, boolean isAscending){
		this._isAscending = isAscending;
		this._container = container;
		
		if(this._isAscending){
			//iteratore ascendente
			try{
				this._currentEdge = this._container._set.first();
			}catch(NoSuchElementException ex){
				this._currentEdge = null;
			}
		}else{
			//iteratore discendente
			try{
				this._currentEdge = this._container._set.last();
			}catch(NoSuchElementException ex){
				this._currentEdge = null;
			}
		}
		
	}
	
	@Override
	public boolean hasNext() {
		return this._currentEdge!=null;
	}

	@Override
	public T next() {
		Element<C> retval = this._currentEdge;
		if(this._isAscending){
			//iteratore ascendente
			this._currentEdge = this._currentEdge.getNextElement();
		}else{
			//iteratore discendente
			this._currentEdge = this._currentEdge.getPreviousElement();
		}
		
		return (T)retval;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	

}
