package it.uniba.netminer.mining.setree;


public class Item extends Element<IRefinement> implements Comparable<Item> {

	protected SetEnumerationTree _owner;
	protected Support _support;
	
	protected Item(SetEnumerationTree owner, IRefinement data) {
		super(data);
		this._owner = owner;
		this._support = new Support();
	}
	
	public Support getSupport(){
		return this._support;
	}

	public SetEnumerationTree getOwner(){
		return this._owner;
	}
	
	@Override
	public int compareTo(Item o) {
		return super._data.compareTo(o._data);
	}
	
	@Override
	public Item getNextElement(){
		return (Item) this._nextElement;
	}
	
	@Override
	public Item getPreviousElement(){
		return (Item) this._previousElement;
	}

}
