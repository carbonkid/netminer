package it.uniba.netminer.mining.setree;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.TreeSet;

public abstract class ElementSet<T extends Element<C>, C extends Comparable<C>> implements Iterable<T> {

	protected TreeSet<T> _set;
	
	protected ElementSet(){
		this._set = new TreeSet<T>();
	}
	
	
	public boolean hasElement(T node){
		boolean retval = false;
		try{
			retval = this._set.contains(node);
		}catch(Exception ex){}
		return retval;
	}
	
	public T getElement(T node){
		T retval = null;
		try{
			retval = this._set.ceiling(node);
			if(retval._data.compareTo(node._data)!=0){
				return null;
			}
		}catch(Exception ex){}
		return retval;
	}

	protected boolean add(T node){
		boolean result = this._set.add(node);
		if(result){
			Element<C> prevNode = null;
			Element<C> nextNode = null;
			
			try{
				prevNode = this._set.headSet(node, false).last();
			}catch(NoSuchElementException ex){}
			
			try{
				nextNode = this._set.tailSet(node, false).first();
			}catch(NoSuchElementException ex){}
			
			if(prevNode==null && nextNode==null){
				//unico nodo
				node.setNextElement(null);
				node.setPreviousElement(null);
			}else if(prevNode!=null && nextNode==null){
				//ha un precedente ma non un successivo
				node.setNextElement(null);
				node.setPreviousElement(prevNode);
				prevNode.setNextElement(node);
			}else if(prevNode==null && nextNode!=null){
				//ha un successivo ma non un precedente
				node.setPreviousElement(null);
				node.setNextElement(nextNode);
				nextNode.setPreviousElement(node);
			}else{
				//ha sia un successivo che un precedente
				node.setPreviousElement(prevNode);
				node.setNextElement(nextNode);
				prevNode.setNextElement(node);
				nextNode.setPreviousElement(node);
			}
		}
		return result;
	}
	
	
	public boolean remove(T singleton){
		boolean result = this._set.remove(singleton);
		if(result){
			Element<C> nextEdge = singleton.getNextElement();
			Element<C> prevEdge = singleton.getPreviousElement();
			
			if(nextEdge!=null){
				nextEdge.setPreviousElement(prevEdge);
			}
			
			if(prevEdge!=null){
				prevEdge.setNextElement(nextEdge);
			}
		}
		return result;
	}
	
	public T getFirstElement(){
		return this._set.first();
	}
	
	public T getLastElement(){
		return this._set.last();
	}
	
	public int getSize(){
		return this._set.size();
	}
	
	public boolean isEmpty(){
		return this._set.isEmpty();
	}
	
	@Override
	public abstract Iterator<T> iterator();
	

}
