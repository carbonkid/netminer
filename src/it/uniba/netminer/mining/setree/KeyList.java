package it.uniba.netminer.mining.setree;

import java.util.HashSet;
import java.util.Set;

public class KeyList implements ITidList {
	
	protected HashSet<Object> _keys;
	
	public KeyList(Set<Object> list){
		this._keys = new HashSet<Object>(list);
	}
	
	public KeyList(){
		this._keys = new HashSet<Object>();
	}
	
	@Override
	public float getSize(){
		return (float)this._keys.size();
	}

	@Override
	public ITidList intersect(ITidList tidlist) {
		if(tidlist instanceof KeyList){
			KeyList list = (KeyList)tidlist;
			Set<Object> intersection = new HashSet<Object>(list._keys);
			
			
			if(!intersection.equals(this._keys)){
				intersection.retainAll(this._keys);
			}
			
		    return new KeyList(intersection); 
		}else{
			throw new ClassCastException();
		} 
	}

	@Override
	public void integrate(ITidList recentData) {
		if(recentData instanceof KeyList){
			KeyList list = (KeyList)recentData;
			this._keys.addAll(list._keys);
		}
		
	}
	
	@Override
	public String toString(){
		return this._keys.toString();
	}
}
