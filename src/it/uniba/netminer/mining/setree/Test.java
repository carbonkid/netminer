package it.uniba.netminer.mining.setree;

import it.uniba.netminer.dataschema.ICondition;
import it.uniba.netminer.dataschema.IQueryable;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Test();
	}
	
	public Test(){
		SetEnumerationTree tree = new SetEnumerationTree(2);
		Refinement ref1 = new Refinement(1, "Angelo");
		Refinement ref2 = new Refinement(0, "Malerba");
		Refinement ref3 = new Refinement(0, "Ceci");
		tree.addItem(ref1);
		tree.addItem(ref2);
		tree.addItem(ref3);
		
		Itemset root = tree.getRoot();
		for(Item item : tree.getItemList()){
			System.out.println(item.getData());
			Itemset child = root.addChild(item);
			int i = 0;
		}
		
		for(Itemset itemset : root.getChildrenList()){
			System.out.println(itemset.getData());
		}
		
		Itemset currentSet = root.getChildrenList().getLastElement();
		while(currentSet!=null){
			Itemset currentSibling = currentSet.getAncestor().getChildrenList().getLastElement();
			while(currentSet!=currentSibling){
				currentSet.addChild(currentSibling.getData());
				currentSibling = currentSibling.getPreviousElement();
			}
			currentSet = currentSet.getPreviousElement();
		}
		
		System.out.println(root.getDescendantsCount());
	}
	
	
	private class Refinement implements IRefinement{
		
		protected Integer _index;
		protected String _object;
		
		public Refinement(int index, String obj){
			this._index = index;
			this._object = obj;
		}

		@Override
		public int compareTo(IRefinement o) {
			if(o instanceof Refinement){
				Refinement otherRef = (Refinement)o;
				
				int comparation = this._index.compareTo(otherRef._index);
				if(comparation==0){
					comparation = this._object.compareTo(otherRef._object);
				}
				return comparation;
			}else{
				throw new ClassCastException();
			}
		}

		@Override
		public ICondition getCondition() {
			return null;
		}
		
		@Override
		public String toString(){
			return this._index+" - "+this._object.toString();
		}
		
	}

}
