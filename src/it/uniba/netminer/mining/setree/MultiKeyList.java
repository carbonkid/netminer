package it.uniba.netminer.mining.setree;

import java.util.HashSet;
import java.util.Set;

public class MultiKeyList implements ITidList {
	
	private int _attributeTOIndex;
	private int _attributeNTOIndex;
	private HashSet<MultiKey> _keys;
	
	public MultiKeyList(Set<MultiKey> list, int i, int y){
		this._keys = new HashSet<MultiKey>(list);
		this._attributeTOIndex = i;
		this._attributeNTOIndex = y;
	}

	

	public KeyList toKeyList(){
		HashSet<Object> basket = new HashSet<Object>();
		
		for(MultiKey record : this._keys){
			Object value = record._toKey;
			if(value!=null){
				basket.add(value);
			}
		}
		return new KeyList(basket);
	}

	
	
	@Override
	public float getSize() {
		KeyList list = this.toKeyList();
		return (float)list.getSize();
	}
	

	@Override
	public ITidList intersect(ITidList otherlist) {
		if(otherlist instanceof MultiKeyList){
			MultiKeyList list = (MultiKeyList)otherlist;
			
			Set<MultiKey> intersection = new HashSet<MultiKey>(list._keys);
			
			if(!intersection.equals(this._keys)){
				intersection = this.doIntersection(intersection, this._keys);
			}
			
		    return new MultiKeyList(intersection, this._attributeTOIndex, this._attributeNTOIndex);
		}
		
		throw new ClassCastException();
		
	}
	
	
	
	private Set<MultiKey> doIntersection(Set<MultiKey> set1, Set<MultiKey> set2){
		Set<MultiKey> intersection = new HashSet<MultiKey>(set1);
		intersection.retainAll(set2);
		return intersection;
	}



	@Override
	public void integrate(ITidList tidlist) {
		if(tidlist instanceof MultiKeyList){
			MultiKeyList list = (MultiKeyList)tidlist;
			this._keys.addAll(list._keys);
		}
	}

	
	@Override
	public String toString(){
		return this._keys.toString();
	}
}
