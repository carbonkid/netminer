package it.uniba.netminer.mining.setree;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import it.uniba.netminer.mining.exceptions.LatticeDepthExceededException;

public class Itemset extends Element<Item> implements Comparable<Itemset> {

	protected int _level;
	protected Itemset _ancestor;
	protected ItemsetList _childrenList;
	protected Support _support;
	
	protected Itemset(Item data){
		this(data,0);
	}
	
	protected Itemset(Item data, int level) {
		super(data);
		this._level = level;
		this._ancestor = null;
		this._childrenList = new ItemsetList();
		this._support = new Support();
	}
	
	
	public Itemset addChild(Item node) throws LatticeDepthExceededException{
		if(this._level >= node._owner.getDepth()){
			throw new LatticeDepthExceededException();
		}else{
			Itemset temp = new Itemset(node, this._level+1);
			boolean result = this._childrenList.add(temp);
			if(result){
				temp.setAncestor(this);
				return temp;
			}
			return null;
		}
	}
	
	
	
	public boolean removeChild(Itemset edge){
		boolean result = this._childrenList.remove(edge);
		if(result){
			edge.setAncestor(null);
		}
		return result;
	}
	
	
	
	public long getDescendantsCount(){
		long count = 0;
		Stack<Itemset> toParseEdge = new Stack<Itemset>();
		toParseEdge.push(this);
		while(!toParseEdge.isEmpty()){
			Itemset currentEdge = toParseEdge.pop();
			count += currentEdge.getChildNumber();
			
			for (Itemset edge : currentEdge.getChildrenList()) {
				toParseEdge.push(edge);
			}
		}
		return count;
	}
	
	
	public Itemset getNodeByItemPath(List<Item> path){
		Itemset rootSet = this;
		for(int i=0; i<path.size(); i++){
			rootSet = rootSet.getChildrenList().getElementByItem(path.get(i));
			if(rootSet==null){
				rootSet = null;
				return null;
			}
		}
		if(rootSet==this){
			rootSet=null;
		}
		return rootSet;
	}
	
	public Itemset getNodeByItemsetPath(List<Itemset> path){
		List<Item> itemListPath = new LinkedList<Item>();
		for(Itemset itemset : path){
			itemListPath.add(itemset._data);
		}
		return this.getNodeByItemPath(itemListPath);
	}
	
	public ItemsetList getChildrenList(){
		return this._childrenList;
	}
	
	public Itemset getAncestor(){
		return this._ancestor;
	}
	
	public int getLevel(){
		return this._level;
	}
	
	public Support getSupport(){
		return this._support;
	}
	
	public int getChildNumber(){
		return this._childrenList.getSize();
	}
	
	public boolean hasChildren(){
		return this._childrenList.isEmpty()==false;
	}
	
	@Override
	public Itemset getNextElement(){
		return (Itemset) this._nextElement;
	}
	
	@Override
	public Itemset getPreviousElement(){
		return (Itemset) this._previousElement;
	}

	@Override
	public int compareTo(Itemset o) {
		return this._data.compareTo(o._data);
	}
	
	@Override
	protected void setNextElement(Element<Item> element){
		this._nextElement = (Itemset)element;
	}
	
	@Override
	protected void setPreviousElement(Element<Item> element){
		this._previousElement = (Itemset)element;
	}
	
	protected void setNextElement(Itemset element){
		this._nextElement = element;
	}
	
	protected void setPreviousElement(Itemset element){
		this._previousElement = element;
	}
	
	protected void setAncestor(Itemset element){
		this._ancestor = element;
	}
	
	public String getStringRappresentation(){
		Itemset currentItemset = this;
		StringBuilder builder = new StringBuilder();
		while(currentItemset!=null){
			if(currentItemset.getLevel()==0){
				break;
			}
			builder.append(currentItemset.getData().getData().toString());
			currentItemset = currentItemset.getAncestor();
		}
		return builder.toString();
	}
}
