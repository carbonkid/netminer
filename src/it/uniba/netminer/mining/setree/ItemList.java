package it.uniba.netminer.mining.setree;


import java.util.Iterator;

public class ItemList extends ElementSet<Item, IRefinement> {

	protected ItemList(){
		super();
	}
	
	
	public boolean hasElementByRefinement(IRefinement refinement){
		Item item = new Item(null, refinement);
		return this.hasElement(item);
	}
	
	public Item getElementByRefinement(IRefinement refinement){
		Item item = new Item(null, refinement);
		return this.getElement(item);
	}
	
	
	@Override
	public Iterator<Item> iterator() {
		return new ElementSetIterator<Item, IRefinement>(this, true);
	}
	
	
	public Iterator<Item> descendingIterator(){
		return new ElementSetIterator<Item, IRefinement>(this, false);
	}



}
