package it.uniba.netminer.mining.setree;

import it.uniba.netminer.dataschema.ICondition;

public interface IRefinement extends Comparable<IRefinement> {
	public ICondition getCondition();
}
