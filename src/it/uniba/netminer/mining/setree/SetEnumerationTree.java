package it.uniba.netminer.mining.setree;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class SetEnumerationTree implements Iterable<Itemset> {

	private ItemList _itemSet;
	private Itemset _root;
	private int _depth;
	
	public SetEnumerationTree(int depth){
		this._itemSet = new ItemList();
		this._root = new Itemset(null,0);
		this._depth = depth;
	}
	
	public boolean addItem(IRefinement refinement){
		Item item = new Item(this, refinement);
		return this._itemSet.add(item);
	}
	
	public boolean removeItem(IRefinement refinement){
		Item item = new Item(this, refinement);
		return this._itemSet.remove(item);
	}
	
	public ItemList getItemList(){
		return this._itemSet;
	}
	
	public Itemset getRoot(){
		return this._root;
	}
	
	public int getDepth(){
		return this._depth;
	}
	
	
	public Itemset getNodeByItemPath(List<Item> path){
		Itemset root = this._root;
		return root.getNodeByItemPath(path);
	}
	
	public Itemset getNodeByItemsetPath(List<Itemset> path){
		Itemset root = this._root;
		return root.getNodeByItemsetPath(path);
	}
	


	@Override
	public Iterator<Itemset> iterator() {
		return new SETreeIterator(true);
	}
	
	
	
	
	private class SETreeIterator implements Iterator<Itemset>{	
		private LinkedList<Itemset> _nodeList;	
		private boolean _isAscending;
		
		private SETreeIterator(boolean isAscending){
			this._nodeList = new LinkedList<Itemset>();
			this._isAscending = isAscending;
			Iterator<Itemset> iterator = null;
			
			if(this._isAscending){
				//iteratore normale
				iterator = _root.getChildrenList().iterator();
			}else{
				//iteratore inverso
				iterator = _root.getChildrenList().descendingIterator();
			}
			
			while(iterator.hasNext()){
				Itemset itemset = iterator.next();
				this._nodeList.add(itemset);
			}
		}
		
		@Override
		public boolean hasNext() {
			return this._nodeList.isEmpty()==false;
		}

		@Override
		public Itemset next() {
			Itemset edge = this._nodeList.pop();
			Iterator<Itemset> iterator = null;
			if(this._isAscending){
				//iteratore normale (usando l'iteratore ascendente sui rami)
				iterator = edge.getChildrenList().iterator();
			}else{
				//iteratore inverso (usando l'iteratore discendente sui rami)
				iterator = edge.getChildrenList().descendingIterator();
			}
			
			while(iterator.hasNext()){
				Itemset child = iterator.next();
				this._nodeList.add(child);
			}
			return edge;
		}
	
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

}
