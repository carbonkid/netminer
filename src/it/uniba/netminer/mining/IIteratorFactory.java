package it.uniba.netminer.mining;

import java.util.Iterator;

public interface IIteratorFactory<T, B> {
	public boolean hasIterator(T object);
	public Iterator<B> getIterator(T object);
}
