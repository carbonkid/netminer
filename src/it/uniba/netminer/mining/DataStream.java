package it.uniba.netminer.mining;

import it.uniba.netminer.dataschema.IDerivedAttribute;
import it.uniba.netminer.mining.events.ChangePatternProxy;
import it.uniba.netminer.mining.events.DataStreamEvent;
import it.uniba.netminer.mining.events.Dispatcher;
import it.uniba.netminer.mining.events.DataStreamListener;
import it.uniba.netminer.mining.lattices.DeductiveLattice;
import it.uniba.netminer.mining.lattices.Refinement;
import it.uniba.netminer.mining.setree.Item;
import it.uniba.netminer.mining.setree.Itemset;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class DataStream extends Dispatcher<DataStreamListener>{
	
	private boolean _isOpened;
	
	private int _paneSize;
	private int _remotePastSize;
	private int _recentPastSize;
	private double _minimumChange;
	private float _minimumGR;
	private DataStreamWindowingTreeModel _model;
	private IGenerator<TargetObject> _toGenerator;
	private TargetObjectFactory _factory;
	private List<Refinement> _singletonList;
	private LandmarkWindow _recentPast;
	private LandmarkWindow _remotePast;
	private Pane _lastRemotePane;
	private Pane _lastRecentPane;
	
	public DataStream(
			TargetObjectFactory factory, 
			List<Refinement> singletonList,
			int paneSize,
			int remotePastPaneSize, 
			int recentPastPaneSize,
			float minSupp, 
			float minfChange, 
			float minGRate, 
			int treeDepth){
		
		super();
		this._isOpened = true;
		
		/*
		 * 	parameters
		 */
		this._paneSize = paneSize;
		this._remotePastSize = remotePastPaneSize;
		this._recentPastSize = recentPastPaneSize;
		this._minimumChange = minfChange;
		this._minimumGR = minGRate;
		this._singletonList = singletonList;
		
		/*
		 *	target objects generator 
		 */
		this._factory = factory;
		this._toGenerator = this._factory.generator();
		
		/*
		 *	time windows
		 */
		this._recentPast = new LandmarkWindow(this, treeDepth, minSupp);
		this._remotePast = new LandmarkWindow(this, treeDepth, minSupp);
		this._lastRecentPane = new Pane(this._paneSize);
		this._lastRemotePane = new Pane(this._paneSize*this._remotePastSize);
		this._model = new DataStreamWindowingTreeModel();
	}
	
	
	public void swap(){
		this._remotePast = this._recentPast;
		this._lastRemotePane = this._lastRecentPane;
		
		int maxDepth = this._remotePast.getHarmonicLattice().getMaximumDepth();
		float minSupport = this._remotePast.getHarmonicLattice().getMinimumSupport();
		this._recentPast = new LandmarkWindow(this, maxDepth, minSupport);
		this._lastRecentPane = new Pane(this._recentPastSize);
		
		//this.setChanged();
		this.fireStructureChanged(new TreePath(this));
	}
	
	public int getRecentPastSize(){
		return this._recentPastSize;
	}
	
	public int getRemotePastSize(){
		return this._remotePastSize;
	}
	
	public int getPaneSize(){
		return this._paneSize;
	}
	
	public void searchChangePattern(){
		this.fireChangesInspectionStarted();
		this.inspectChangePatterns();
		this.fireChangesInspectionCompleted();
	}


	public void consume(){
		if(this._isOpened && this._toGenerator.canGenerateNext()){
			try{
				TargetObject targetObject = this._toGenerator.next();
			
				if(!this._lastRemotePane.isFull()){
					//consumiamo lo stream travasando unit� di analisi nel passato remoto
					this._lastRemotePane.addTargetObject(targetObject);
					if(this._lastRemotePane.getCount()==1 && this._remotePast.getTargetObjectsCount()==0){
						this.fireStreamStarted();
					}
					if(this._lastRemotePane.isFull()){
						this._remotePast.addPane(this._lastRemotePane);
					}
				}else{
					//consumiamo lo stream travasando unit� di analisi nel passato recente
					this._lastRecentPane.addTargetObject(targetObject);
					if(this._lastRecentPane.isFull()){
						this._recentPast.addPane(this._lastRecentPane);
						this._lastRecentPane = new Pane(this._paneSize);
					}
					
				}
			}catch(IndexOutOfBoundsException ex){
				this.fireStreamIdle();
			}
		}
	}
	
	public void close(){
		this._isOpened = false;
	}
	
	public boolean isActive(){
		return this._isOpened;
	}
	
	public double getMinimumGrowthRate(){
		return this._minimumGR;
	}
	
	public double getMinimumFrequencyChange(){
		return this._minimumChange;
	}
	
	public List<Refinement> getSingletonList(){
		return this._singletonList;
	}
	
	public TimeWindow getRemotePastTimeWindow(){
		return this._remotePast;
	}
	
	public TimeWindow getRecentPastTimeWindow(){
		return this._recentPast;
	}
	
	public TreeModel model() {
		return this._model;
	}
	
	public IDerivedAttribute getTimingAttribute(){
		return this._factory.getDerivedTimingAttribute();
	}
	
	public StructuralTree getStructuralTree(){
		return this._factory.getStructuralTree();
	}
	
	
	@Override
	public String toString() {
		return "Data Stream";
	}
	
	
	
	
	private void inspectChangePatterns(){
		LandmarkWindow recentPast = this._recentPast;
		LandmarkWindow remotePast = this._remotePast;
		DeductiveLattice recentLattice = recentPast.getHarmonicLattice().getDeductiveLattice();
		DeductiveLattice remoteLattice = remotePast.getHarmonicLattice().getDeductiveLattice();
		
		
		//ciascun pattern nel reticolo attuale va matchato nell'altro reticolo
		for(Itemset itemset : recentLattice){
			int length = itemset.getLevel()-1; //-1 per non considerare la radice

			if(length==0){
				for(Itemset endNode : remoteLattice.getSetEnumerationTree().getRoot().getChildrenList()){
					//valutiamo il pattern emergente
					float patternFrequency = itemset.getSupport().getKeyList().getSize()/recentPast.getTargetObjectsCount();
					float otherPatternFrequency = endNode.getSupport().getKeyList().getSize()/remotePast.getTargetObjectsCount();
					float patternGrowthRate = patternFrequency/otherPatternFrequency;
					ChangePatternProxy proxyEP = new ChangePatternProxy(itemset, endNode, patternGrowthRate);
					
					if(patternGrowthRate!=1.0 && !itemset.equals(endNode)){
						if(patternGrowthRate > this._minimumGR){
							this.fireValidChangePatternCandidateFound(proxyEP);
							break;
						}else{
							this.fireInvalidChangePatternCandidateFound(proxyEP);
						}
					}
				}
			}

			//valuto tutti i k-1 
			for(int i=0; i<length; i++){
				int j=itemset.getLevel()-1;
				LinkedList<Item> path = new LinkedList<Item>();
				LinkedList<Item> prefixPath = new LinkedList<Item>();
				LinkedList<Item> suffixPath = new LinkedList<Item>();
				Itemset currentItemset = itemset;
				while(currentItemset!=recentLattice.getSetEnumerationTree().getRoot()){
					if(i==j){
						path.addFirst(null);
					}else if(j<i){
						path.addFirst(currentItemset.getData());
						prefixPath.addFirst(currentItemset.getData());
					}else if(j>i){
						path.addFirst(currentItemset.getData());
						suffixPath.addFirst(currentItemset.getData());
					}
					j--;
					currentItemset = currentItemset.getAncestor();
				}


				//j � la posizione di variabilit�
				Item lowerBound = null;
				Item upperBound = null;
				Itemset startNode = null;
				if(prefixPath.size()==0){
					lowerBound = null;
					upperBound = suffixPath.get(0);
					startNode = remoteLattice.getSetEnumerationTree().getRoot();
				}else if(suffixPath.size()==0){
					lowerBound = prefixPath.get(prefixPath.size()-1);
					upperBound = null;
					startNode = remoteLattice.getSetEnumerationTree().getRoot().getNodeByItemPath(prefixPath);
				}else{
					lowerBound = prefixPath.get(prefixPath.size()-1);
					upperBound = suffixPath.get(0);
					startNode = remoteLattice.getSetEnumerationTree().getRoot().getNodeByItemPath(prefixPath);
				}

				//a partire dal nodo corrispondente al percorso di prefisso, occorre valutare
				//i nodi lessicograficamente compresi: lowerBound<X<upperBound + suffixPath
				if(startNode!=null){
					SortedSet<Itemset> candidateComprised = startNode.getChildrenList().getSubset(lowerBound, upperBound);
					
					for(Itemset variableCandidate : candidateComprised){
						Itemset endNode = variableCandidate.getNodeByItemPath(suffixPath);
						if(endNode!=null){
							//valutiamo il pattern emergente
							float patternFrequency = itemset.getSupport().getKeyList().getSize()/recentPast.getTargetObjectsCount();
							float otherPatternFrequency = endNode.getSupport().getKeyList().getSize()/remotePast.getTargetObjectsCount();
							float patternGrowthRate = patternFrequency/otherPatternFrequency;
							ChangePatternProxy proxyEP = new ChangePatternProxy(itemset, endNode, patternGrowthRate);
							
							
							if(patternGrowthRate!=1.0 && !itemset.equals(endNode)){
								if(patternGrowthRate > this._minimumGR){
									this.fireValidChangePatternCandidateFound(proxyEP);
									break;
								}else{
									this.fireInvalidChangePatternCandidateFound(proxyEP);
								}
							}
						}
					}
				}
			}

		}
	}
	
	
	
	
	
	
	
	private void fireChangesInspectionCompleted() {
		DataStreamEvent event = new DataStreamEvent(this);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.changesInspectionCompleted(event);
		}
	}


	private void fireChangesInspectionStarted() {
		DataStreamEvent event = new DataStreamEvent(this);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.changesInspectionStarted(event);
		}
	}

	
	
	private void fireValidChangePatternCandidateFound(ChangePatternProxy proxyEP) {
		DataStreamEvent event = new DataStreamEvent(this, this._recentPast, proxyEP);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.validChangePatternFound(event);
		}
	}


	private void fireInvalidChangePatternCandidateFound(ChangePatternProxy proxyEP) {
		DataStreamEvent event = new DataStreamEvent(this, this._recentPast, proxyEP);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.invalidChangePatternFound(event);
		}	
	}


	protected void fireStreamStarted(){
		DataStreamEvent event = new DataStreamEvent(this);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.streamStarted(event);
		}
	}
	
	
	protected void fireStreamIdle(){
		DataStreamEvent event = new DataStreamEvent(this);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.streamIdle(event);
		}
	}
	
	
	protected void fireWindowChanged(TimeWindow window){
		DataStreamEvent event = new DataStreamEvent(this, window);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.windowChanged(event);
		}
	}
	
	public void fireQueriesStarted(TimeWindow window) {
		DataStreamEvent event = new DataStreamEvent(this, window);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.queriesStarted(event);
		}
	}


	public void fireQueriesCompleted(TimeWindow window) {
		DataStreamEvent event = new DataStreamEvent(this, window);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.queriesCompleted(event);
		}
	}
	
	
	public void fireFrequentPatternFound(Itemset itemset, TimeWindow window){
		DataStreamEvent event = new DataStreamEvent(this, window, itemset);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.frequentPatternFound(event);
		}
	}
	
	public void fireFrequentItemsetsUpdateStarted(TimeWindow window) {
		DataStreamEvent event = new DataStreamEvent(this, window);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.frequentItemsetsUpdateStarted(event);
		}
	}


	public void fireFrequentItemsetsUpdateCompleted(TimeWindow window) {
		DataStreamEvent event = new DataStreamEvent(this, window);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.frequentItemsetsUpdateCompleted(event);
		}
	}
	
	public void fireFrequentTriplesetsUpdateStarted(TimeWindow window) {
		DataStreamEvent event = new DataStreamEvent(this, window);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.frequentTriplesetsUpdateStarted(event);
		}
	}


	public void fireFrequentTriplesetsUpdateCompleted(TimeWindow window) {
		DataStreamEvent event = new DataStreamEvent(this, window);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.frequentTriplesetsUpdateCompleted(event);
		}
	}
	
	
	public void fireLatticeUpdateStarted(TimeWindow window){
		DataStreamEvent event = new DataStreamEvent(this, window);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.latticeUpdateStarted(event);
		}
	}
	
	
	public void fireLatticeUpdateCompleted(TimeWindow window){
		DataStreamEvent event = new DataStreamEvent(this, window);
		Iterator<DataStreamListener> listenerIterator = this.getListenerIterator();
		while(listenerIterator.hasNext()){
			DataStreamListener listener = listenerIterator.next();
			listener.latticeUpdateCompleted(event);
		}
	}
	
	
	
	
	protected void fireNodesInserted(TreePath parentPath, int[] indices, Object[] subNodes) {
		TreeModelEvent event = new TreeModelEvent(this, parentPath, indices, subNodes);
		for(TreeModelListener lis : this._model._listenersSet) {
			lis.treeNodesInserted(event);
		}
	}
	
	protected void fireNodeInserted(TreePath parentPath, int index, Object node) {
		this.fireNodesInserted(parentPath, new int[]{index}, new Object[]{node});
	}
	
	
	protected void fireStructureChanged(TreePath path) {
	    TreeModelEvent event = new TreeModelEvent(this, path);
	    for(TreeModelListener lis : this._model._listenersSet) {
	        lis.treeStructureChanged(event);
	    }
	}
	
	
	
	
	
	protected class DataStreamWindowingTreeModel implements TreeModel{
		
		private LinkedHashSet<TreeModelListener> _listenersSet;
		
		private DataStreamWindowingTreeModel(){
			this._listenersSet = new LinkedHashSet<TreeModelListener>();
		}

		@Override
		public void addTreeModelListener(TreeModelListener l) {
			this._listenersSet.add(l);
		}

		@Override
		public Object getChild(Object parent, int index) {
			Object obj = null;
			if(parent instanceof DataStream){
				if(index==0){
					return DataStream.this._remotePast;
				}else if(index==1){
					return DataStream.this._recentPast;
				}
			}else if(parent instanceof LandmarkWindow){
				obj = ((LandmarkWindow) parent).getPaneAt(index);
			}else if(parent instanceof Pane){
				obj = ((Pane) parent).getAt(index);
			}else if(parent instanceof TargetObject){
				obj = null;
			}
			return obj;
		}

		@Override
		public int getChildCount(Object parent) {
			int count = 0;
			if(parent instanceof DataStream){
				count = 2;
			}else if(parent instanceof LandmarkWindow){
				count = ((LandmarkWindow) parent).getPaneCount();
			}else if(parent instanceof Pane){
				count = ((Pane) parent).getSize();
			}else if(parent instanceof TargetObject){
				count = 0;
			}
			return count;
		}

		@Override
		public int getIndexOfChild(Object parent, Object child) {
			int index = -1;
			if(parent==null || child==null){
				index = -1;
			}else{
				if(parent instanceof DataStream && child instanceof LandmarkWindow){
					LandmarkWindow currentChild = (LandmarkWindow)child;
					if(currentChild == DataStream.this._recentPast){
						index = 0;
					}else if(currentChild == DataStream.this._remotePast){
						index = 1;
					}
					
				}else if(parent instanceof LandmarkWindow && child instanceof Pane){
					LandmarkWindow currentWindow = (LandmarkWindow)parent;
					Pane currentChild = (Pane)child;
					index = currentWindow.indexOf(currentChild);
				}else if(parent instanceof Pane && child instanceof TargetObject){
					Pane currentPane = (Pane)parent;
					TargetObject currentChild = (TargetObject)child;
					index = currentPane.indexOf(currentChild);
				}else if(parent instanceof TargetObject){
					index = -1;
				}
			}
			return index;
		}

		@Override
		public Object getRoot() {
			return DataStream.this;
		}

		@Override
		public boolean isLeaf(Object node) {
			boolean isLeaf = false;
			if(node instanceof DataStream){
				isLeaf = false;
			}else if(node instanceof LandmarkWindow){
				LandmarkWindow window = ((LandmarkWindow) node);
				if(window.getPaneCount()==0){
					isLeaf = true;
				}
			}else if(node instanceof Pane){
				isLeaf = false;
			}else if(node instanceof TargetObject){
				isLeaf = true;
			}
			return isLeaf;
		}

		@Override
		public void removeTreeModelListener(TreeModelListener l) {
			this._listenersSet.remove(l);
		}

		@Override
		public void valueForPathChanged(TreePath path, Object newValue) {
			// TODO Auto-generated method stub
		}
	}





	

}
