package it.uniba.netminer.mining.exceptions;

public class PaneNotFilledException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4970837065341117068L;

	public PaneNotFilledException(){
		super();
	}

}
