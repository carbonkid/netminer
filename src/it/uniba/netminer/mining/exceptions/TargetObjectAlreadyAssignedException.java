package it.uniba.netminer.mining.exceptions;

public class TargetObjectAlreadyAssignedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7721456394452497517L;

	public TargetObjectAlreadyAssignedException(){
		super();
	}

}
