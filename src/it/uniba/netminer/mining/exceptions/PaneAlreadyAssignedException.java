package it.uniba.netminer.mining.exceptions;

public class PaneAlreadyAssignedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9089072378463486599L;

	public PaneAlreadyAssignedException(){
		super();
	}

}
