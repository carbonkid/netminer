package it.uniba.netminer.mining.exceptions;

public class LatticeDepthExceededException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8213166745757169725L;

	public LatticeDepthExceededException(){
		super();
	}

}
