package it.uniba.netminer.mining.visualization;

import java.awt.Dimension;
import java.awt.Point;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import processing.core.PApplet;
import it.uniba.netminer.datavisualization.IRenderable;
import it.uniba.netminer.datavisualization.ItemRenderer;
import it.uniba.netminer.datavisualization.Sketch;

public class GraphVisualizer extends Sketch {
	
	private int _radius = 200;
	private int _center = 250;
	private LinkedHashMap<Object, Node> _nodes;
	private LinkedHashSet<Edge> _edges;
	private List<Object> _listData;
	private Class<? extends ItemRenderer> _nodeRendererTypeClass;
	
	
	public GraphVisualizer(){
		this._listData = null;
		this._nodeRendererTypeClass = null;
		this._nodes = new LinkedHashMap<Object, Node>();
		this._edges = new LinkedHashSet<Edge>();
	}

	
	public void setNodeItemRenderer(Class<? extends ItemRenderer> itemRendererClass){
		this._nodeRendererTypeClass = itemRendererClass;
	}

	@Override
	public void update(PApplet drawingInterface) {
		drawingInterface.background(255, 255, 255);
		
		drawingInterface.pushStyle();
		for(Edge edge : this._edges){
			Map linkData = (Map)edge.getData();
			Point nodeApos = edge._nodeA._renderer.getPosition();
			Point nodeBpos = edge._nodeB._renderer.getPosition();
			drawingInterface.fill(0,0,0);
			drawingInterface.line(nodeApos.x, nodeApos.y, nodeBpos.x, nodeBpos.y);
			
			float deltaY = nodeBpos.y - nodeApos.y;
			float deltaX = nodeBpos.x - nodeApos.x;
			float angleInDegrees = drawingInterface.atan2(deltaY, deltaX);
			drawingInterface.pushMatrix();
			drawingInterface.translate((nodeBpos.x+nodeApos.x)/2,(nodeBpos.y+nodeApos.y)/2);
			drawingInterface.rotate(angleInDegrees);
			

			drawingInterface.textSize(10);
			drawingInterface.textAlign(drawingInterface.CENTER);
			drawingInterface.text(linkData.get("relationship").toString(),0,-7);
			drawingInterface.popMatrix();
		}
		drawingInterface.popStyle();
		
		
		drawingInterface.pushStyle();
		for(Node node : this._nodes.values()){
			drawingInterface.pushMatrix();
			drawingInterface.translate(node._renderer.getPosition().x, node._renderer.getPosition().y);
			node._renderer.update(drawingInterface);
			drawingInterface.popMatrix();
		}
		drawingInterface.popStyle();
	}

	@Override
	public void setData(List<Object> data) {
		this._listData = data;
		this._edges = new LinkedHashSet<Edge>();
		this._nodes = new LinkedHashMap<Object, Node>();
		
		for(Object record : data){
			if(record instanceof Map){
				Map link = (Map)record;
				Object nodeAid = link.get("node1");
				Object nodeBid = link.get("node2");
				
				ItemRenderer edgeRenderer;
				if(link!=null && nodeAid!=null && nodeBid!=null){
					try {
						
						ItemRenderer nodeRenderer = this._nodeRendererTypeClass.newInstance();
						Node nodeA = new Node(nodeAid, nodeRenderer);
						if(!this._nodes.containsKey(nodeAid)){
							this._nodes.put(nodeAid, nodeA);
						}else{
							nodeA = this._nodes.get(nodeAid);
						}
						nodeRenderer = this._nodeRendererTypeClass.newInstance();
						Node nodeB = new Node(nodeBid, nodeRenderer);
						if(!this._nodes.containsKey(nodeBid)){
							this._nodes.put(nodeBid, nodeB);
						}else{
							nodeB = this._nodes.get(nodeBid);
						}
						
						Edge edge = new Edge(nodeA, nodeB, link);
						this._edges.add(edge);
						/*
						if(!this._edges.contains(edge)){
							this._edges.add(edge);
						}*/
						
						
					} catch (InstantiationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		}
		
		
		//set position of nodes
		float currentAngle = -PApplet.HALF_PI;
	    float angleAmplitude = (2*PApplet.PI)/this._nodes.size();
	    float xCenter = 250;
	    float yCenter = 250;
		for(Node node : this._nodes.values()){
			Point position = new Point();
			position.x = (int) (xCenter + _radius * PApplet.cos(currentAngle));
			position.y = (int) (yCenter + _radius * PApplet.sin(currentAngle));
			node._renderer.setPosition(position);
		   	currentAngle += angleAmplitude;
		}
	}

	@Override
	public List<Object> getData() {
		return this._listData;
	}
	
	
	@Override
	public Point getPosition() {
		return super.getPosition();
	}

	@Override
	public Dimension getSize() {
		return super.getSize();
	}

	@Override
	public IRenderable getOwner() {
		return super.getOwner();
	}
	
	
	private class Node{
		private Object _nodeName;
		private ItemRenderer _renderer;
		
		private Node(Object name, ItemRenderer renderer){
			this._nodeName = name;
			this._renderer = renderer;
			this._renderer.setData(this._nodeName);
		}
		
		
		@Override
		public boolean equals(Object obj){
			boolean retval = false;
			if(obj instanceof Node){
				Node node = (Node)obj;
				retval = node._nodeName.equals(this._nodeName);
			}
			return retval;
		}
		
		@Override
		public int hashCode(){
			int hash = 1;
	        hash = hash * 17 + this._nodeName.hashCode();
	        return hash;
		}
	}

	private class Edge{
		private Node _nodeA;
		private Node _nodeB;
		private Object _objectData;
		
		private Edge(Node startNode, Node endNode, Object data){
			if(startNode==null || endNode==null){
				throw new NullPointerException();
			}
			
			this._objectData = data;
			this._nodeA = startNode;
			this._nodeB = endNode;
		}
		
		public Object getData(){
			return this._objectData;
		}
		
		
		@Override
		public boolean equals(Object obj){
			boolean retval = false;
			if(obj instanceof Edge){
				Edge edge = (Edge)obj;
				retval = (edge._nodeA.equals(this._nodeA) && edge._nodeB.equals(this._nodeB));
			}
			return retval;
		}
		
		@Override
		public int hashCode(){
			int hash = 1;
	        hash = hash * 17 + this._nodeA.hashCode();
	        hash = hash * 31 + this._nodeB.hashCode();
	        return hash;
		}
		
	}

	@Override
	public void setPosition(Point pos) {
		
	}


	@Override
	public void setSelectedItem(Object obj) {
	}


	@Override
	public Object getSelectedItem() {
		return null;
	}

}
