package it.uniba.netminer.mining.visualization;

import processing.core.PApplet;
import it.uniba.netminer.datavisualization.ItemRenderer;

public class NodeItemRenderer extends ItemRenderer {

	@Override
	public void update(PApplet drawingInterface) {
		drawingInterface.fill(0,0,255);
		drawingInterface.textSize(14);
		drawingInterface.textAlign(drawingInterface.CENTER);
		drawingInterface.text(super.getData().toString(), 0, -15);
		drawingInterface.ellipseMode(drawingInterface.CENTER);
		drawingInterface.noStroke();
		drawingInterface.fill(255,0,0);
		drawingInterface.ellipse(0, 0, 20, 20);
		drawingInterface.fill(255,255,255);
		drawingInterface.ellipse(0, 0, 17, 17);
		drawingInterface.fill(255,0,0);
		drawingInterface.ellipse(0, 0, 14, 14);
	}
	
}
