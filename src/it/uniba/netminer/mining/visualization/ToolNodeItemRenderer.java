package it.uniba.netminer.mining.visualization;

import java.util.LinkedList;

import processing.core.PApplet;
import it.uniba.netminer.datavisualization.IDataBindable;
import it.uniba.netminer.datavisualization.IRenderable;
import it.uniba.netminer.datavisualization.ItemRenderer;

public class ToolNodeItemRenderer extends ItemRenderer {

	
	public ToolNodeItemRenderer(IRenderable renderable){
		super(renderable);
	}
	
	@Override
	public void update(PApplet drawingInterface) {
		boolean rollover = true;
		if(this.overCircle(drawingInterface, this.getPosition().x,this.getPosition().y,20)){
			rollover = true;
			//active circle drawing
			if(super.getOwner()!=null){
				if(super.getOwner() instanceof IDataBindable){
					IDataBindable owner = (IDataBindable)super.getOwner();
					Object lastSelectedItem = owner.getSelectedItem();
					if(lastSelectedItem==null || 
					   !lastSelectedItem.equals(super.getData())){
						owner.setSelectedItem(super.getData());
					}
				}
			}
		}else{
			rollover = false;
		}
		
		if(!rollover){
			//disactive circle drawing
			drawingInterface.noStroke();
			drawingInterface.fill(190,190,190);
			drawingInterface.ellipse(0, 0, 20, 20);
		}else{
		    //active circle drawing
		    drawingInterface.noStroke();
			drawingInterface.fill(255,0,0);
			drawingInterface.ellipse(0, 0, 20, 20);
			drawingInterface.fill(255,255,255);
			drawingInterface.ellipse(0, 0, 17, 17);
			drawingInterface.fill(255,0,0);
			drawingInterface.ellipse(0, 0, 14, 14);
		}
		
		//label drawing
		float textWidth = drawingInterface.textWidth(super.getData().toString());
		float textHeight = drawingInterface.textAscent()+2;
		drawingInterface.stroke(1);
		drawingInterface.fill(255, 255, 255);
		drawingInterface.rect(0, -15, textWidth+2, textHeight);
		drawingInterface.fill(0,0,0);
		drawingInterface.text(super.getData().toString(),2,-15,textWidth+1,textHeight+4);
	}
	
	private boolean overCircle(PApplet drawingInterface, int x, int y, int diameter){
		float disX = x - drawingInterface.mouseX;
		float disY = y - drawingInterface.mouseY;
		if(drawingInterface.sqrt(drawingInterface.sq(disX) + drawingInterface.sq(disY)) < diameter/2 ) {
			return true;
		} else {
		    return false;
		}
	}
	
}
