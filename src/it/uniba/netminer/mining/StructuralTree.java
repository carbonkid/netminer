package it.uniba.netminer.mining;

import it.uniba.netminer.dataschema.Alias;
import it.uniba.netminer.dataschema.IEntity;
import it.uniba.netminer.dataschema.IRelationship;
import it.uniba.netminer.dataschema.IView;
import it.uniba.netminer.dataschema.View;
import it.uniba.netminer.exceptions.InvalidRelationshipException;
import it.uniba.netminer.exceptions.NegativeDepthException;
import it.uniba.netminer.mining.StructuralNode.StructuralNodeType;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class StructuralTree implements Iterable<StructuralNode> {
	
	private static String aliasPrefix = "T";

	protected int _nodeCount;
	protected StructuralNode _root;
	
	public StructuralTree() throws NegativeDepthException {
		this._root = null;
	}

	
	
	private void setRoot(IEntity entity, Alias alias) {
		IView view = new View(entity, alias); 
		this._root = new StructuralNode(view, null, this, StructuralNodeType.NEW_FK_EXHIBITED, 0);
		this._nodeCount++;
	}

	
	
	
	public StructuralNode getRoot() {
		return this._root;
	}

	
	
	public boolean isEmpty() {
		return this._root==null;
	}

	
	
	public StructuralNode getFirstChild(StructuralNode node) {
		return node.getFirstChild();
	}
	
	
	public StructuralNode addChild(IEntity data){
		Alias alias = new Alias(StructuralTree.aliasPrefix+this._nodeCount);
		this.setRoot(data, alias);
		
		return this.getRoot();
	}
	
	
	public StructuralNode addChild(StructuralNode father, IRelationship data, StructuralNodeType type)
	throws InvalidRelationshipException{
		//derivo il nodo attuale estendendo la vista del nodo padre
		Alias alias = new Alias(StructuralTree.aliasPrefix+this._nodeCount);
		IView newView = father.getData().clone();
		StructuralNode dataNode = null;
		
		
		//verifico che esista la chiave esterna per il nodo padre
		for(IRelationship fk : father.getData().getForeignKeyList()){
			if(fk.equals(data)){
				newView.joinByRelationship(fk, alias);
				dataNode = new StructuralNode(newView, fk, this, type, father.getDepthLevel()+1);
				
				this._nodeCount++;
				if(father._firstChild == null){
					father._firstChild = dataNode;
					father._lastChild = dataNode;
				}else{
					father._lastChild._nextSibling = dataNode;
					dataNode._prevSibling = father._lastChild;
					father._lastChild = dataNode;
				}
			}
		}
		
		if(dataNode!=null){
			return dataNode;
		}else{
			throw new InvalidRelationshipException();
		}
	}





	@Override
	public Iterator<StructuralNode> iterator() {
		return new StructuralTreeIterator();
	}
	
	
	
	private class StructuralTreeIterator implements Iterator<StructuralNode>{

		private Queue<StructuralNode> _nodesYetToParse;
		
		private StructuralTreeIterator(){
			this._nodesYetToParse = new LinkedList<StructuralNode>();
			if(_root!=null){
				this._nodesYetToParse.add(_root);
			}
		}
		
		@Override
		public boolean hasNext() {
			return !this._nodesYetToParse.isEmpty();
		}

		@Override
		public StructuralNode next() {
			//visita in ampiezza
			StructuralNode node = this._nodesYetToParse.poll();
			StructuralNode child = node.getFirstChild();
			//IView view = node.getData();
			
			while(child!=null){
				this._nodesYetToParse.add(child);
				child = child.getNextSibling();
			}
			
			return node;
		}

		@Override
		public void remove() throws UnsupportedOperationException{
			throw new UnsupportedOperationException();
		}
		
	}



	public LinearStructuralSpace getLinearized() {
		return new LinearStructuralSpace(this);
	}

}
	