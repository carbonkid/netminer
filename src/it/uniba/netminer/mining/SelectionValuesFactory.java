package it.uniba.netminer.mining;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;

import it.uniba.netminer.dataaccess.DatabaseSession;
import it.uniba.netminer.dataaccess.ISessionDelegate;
import it.uniba.netminer.dataschema.ConjunctiveCondition;
import it.uniba.netminer.dataschema.IAttribute;
import it.uniba.netminer.dataschema.IType;
import it.uniba.netminer.dataschema.Restriction;
import it.uniba.netminer.dataschema.Type.Morphology;
import it.uniba.netminer.mining.lattices.Refinement;


public class SelectionValuesFactory implements IGeneratorFactory<IAttribute, Refinement> {

	private HashMap<String, List> _basket;
	private HashSet<IAttribute> _attributesDiscretized;
	private HashSet<IAttribute> _attributesToDiscretize;
	private int _continuousAttributeNumberOfInterval;
	
	
	public SelectionValuesFactory(){
		this._basket = new HashMap<String, List>();
		this._attributesToDiscretize = new HashSet<IAttribute>();
		this._attributesDiscretized = new HashSet<IAttribute>();
		this._continuousAttributeNumberOfInterval = 4;
	}
	
	
	public void addAttribute(IAttribute attribute){
		if(attribute!=null){
			if(!this._attributesDiscretized.contains(attribute)){
				this._attributesToDiscretize.add(attribute);
			}
		}
	}

	
	public void discretize(){
		new DatabaseSession(
			new ISessionDelegate(){

				@Override
				public void onSessionOpened(Session session) throws SQLException {
					
					for(IAttribute currentAttribute : _attributesToDiscretize){
						_attributesDiscretized.add(currentAttribute);
						
						
						/*
						 * vanno discretizzati gli attributi di tipo
						 * continuo e discreto che non facciano parte di
						 * una chiave primaria, una chiave esterna e che
						 * non siano referenziati da chiave esterna.
						 * solo questi attributi verranno usati nelle 
						 * condizioni dell'albero di selezione.
						 */
						
						IType currentAttributeType = currentAttribute.getDataType();
						Morphology currentAttributeMorphology = currentAttributeType.getMorphology();
						
						if(currentAttributeMorphology != Morphology.UNCLASSIFIED){
									
							String attributeName = currentAttribute.getFullyQualifiedName();
							
							if(currentAttributeMorphology == Morphology.CONTINUOUS){
								Criteria crit = session.createCriteria(currentAttribute.getEntityOwner().getName());
						        ProjectionList projList = Projections.projectionList();
						        projList.add(Projections.min(attributeName));
						        projList.add(Projections.max(attributeName));
						        crit.setProjection(projList);
						        List results = crit.list();
						        Double min = (Double)((Object[])results.get(0))[0];
						        Double max = (Double)((Object[])results.get(0))[1];
						        Double amplitude = (min+max)/_continuousAttributeNumberOfInterval;
						        
						        List<Double> discreteValues = new ArrayList<Double>();
						        if(min==max){
						        	discreteValues.add(min);
						        }else{
							        Double currentValue = min;
							        while(currentValue<max-amplitude){
									   	if(currentValue+amplitude > max){
									   		break;
									   	}
									   	discreteValues.add(currentValue+amplitude);
									   	currentValue = currentValue + amplitude;
									}
						        }
								        
								_basket.put(currentAttribute.getName(), discreteValues);
								       
								        
							}else if(currentAttributeMorphology == Morphology.DISCRETE){
								Criteria crit = session.createCriteria(currentAttribute.getEntityOwner().getName());
								crit.setProjection(Projections.distinct(Projections.property(attributeName)));
							
								List results = crit.list();									
							    _basket.put(currentAttribute.getName(), results);
							}
						}
					}
					_attributesToDiscretize.clear();
				}
			

				@Override
				public void onSessionFailed(Exception exception) {
					System.out.println("discretization failed");
				}
				
			}
		).dispatch();
	}


	
	@Override
	public boolean hasGenerator(IAttribute object) {
		boolean retval = false;
		if(this._basket.containsKey(object.getName())){
			retval = true;
		}
		return retval;
	}


	@Override
	public IGenerable<Refinement> getGenerator(IAttribute object) {
		return new GenerableSelectionRefinement(object);
	}
	
	
	public class GenerableSelectionRefinement implements IGenerable<Refinement>{

		private IAttribute _attribute;
		
		private GenerableSelectionRefinement(IAttribute attr){
			this._attribute = attr;
		}
		
		@Override
		public IGenerator<Refinement> generator() {
			return new SelectionRefinementGenerator(this._attribute);
		}
		
	}
	
	
	public class SelectionRefinementGenerator implements IGenerator<Refinement>{

		private IAttribute _attribute;
		private Iterator _valuesIterator;
		private List _values;
		private Object _lastValue;
		private int _lastOrdinal;
		
		private SelectionRefinementGenerator(IAttribute attribute){
			this._lastValue = null;
			this._lastOrdinal = 0;
			if(attribute!=null){
				this._attribute = attribute;
				if(_basket.containsKey(attribute.getName())){
					this._values = _basket.get(attribute.getName());
					this._valuesIterator = this._values.iterator();
				}
			}
		}
		
		@Override
		public Refinement next() {
			ConjunctiveCondition condition = null;
			Morphology morphology = this._attribute.getDataType().getMorphology();
			if(morphology == Morphology.DISCRETE){
				Object value = this._valuesIterator.next();
				condition = new ConjunctiveCondition(this._attribute);
				condition.addRestriction(Restriction.equal(value));
				this._lastValue = value;
			}else if(morphology == Morphology.CONTINUOUS){
				Object first = this._values.get(0);
				condition = new ConjunctiveCondition(this._attribute);
				
				if(this._lastValue == null){
					//� il primo elemento, intervallo (-INFINITY, value]
					Object value = this._valuesIterator.next();
					condition.addRestriction(Restriction.lowerThanOrEqual(value));
					this._lastValue = value;
				}else{
					if(this._valuesIterator.hasNext()){
						//non � il primo o ultimo elemento, intervallo ]value, next[
						Object next = this._valuesIterator.next();
						condition.addRestriction(Restriction.greaterThan(this._lastValue));
						condition.addRestriction(Restriction.lowerThan(next));
						this._lastValue = next;
					}else{
						//� l'ultimo elemento, intervallo [value, +INFINITY)
						condition.addRestriction(Restriction.greaterThanOrEqual(this._lastValue));
						this._lastValue = null;
					}
				}
			}
			this._lastOrdinal++;
			return new Refinement(condition, this._lastOrdinal-1);
		}

		
		
		@Override
		public boolean canGenerateNext() {
			boolean canGenerate = false;
			Morphology morphology = this._attribute.getDataType().getMorphology();
			if(morphology == Morphology.DISCRETE){
				if(this._valuesIterator.hasNext()){
					canGenerate = true;
				}
			}else if(morphology == Morphology.CONTINUOUS){
				if(this._lastValue==null){
					if(this._valuesIterator.hasNext()){
						canGenerate = true;
					}
				}else{
					canGenerate = true;
				}
			}
			return canGenerate;
		}
		
	}
	
}
