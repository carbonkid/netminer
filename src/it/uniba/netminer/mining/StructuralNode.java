package it.uniba.netminer.mining;

import it.uniba.netminer.dataschema.IRelationship;
import it.uniba.netminer.dataschema.IView;

public class StructuralNode {
	
	public enum StructuralNodeType { NEW_FK_EXHIBITED, NESTED_ANCESTOR_SIBLING }

	protected IView _data;
	protected StructuralNodeType _nodeType;
	protected IRelationship _latestRelationshipJoined;
	protected StructuralNode _prevSibling;
	protected StructuralNode _nextSibling;
	protected StructuralNode _firstChild;
	protected StructuralNode _lastChild;
	protected int _depthLevel;
	protected StructuralTree _owner;
	
	public StructuralNode(IView data, IRelationship latestFK, StructuralTree owner, StructuralNodeType type, int depth){
		this._latestRelationshipJoined = latestFK;
		this._data = data;
		this._depthLevel = depth;
		this._owner = owner;
		this._prevSibling = null;
		this._nextSibling = null;
		this._firstChild = null;
		this._lastChild = null;
		this._nodeType = type;
	}
	

	public int getDepthLevel() {
		return this._depthLevel;
	}

	
	public StructuralTree getOwnerTree() {
		return this._owner;
	}

	
	public IView getData() {
		return this._data;
	}

	
	public StructuralNode getNextSibling() {
		return this._nextSibling;
	}

	
	public StructuralNode getPrevSibling() {
		return this._prevSibling;
	}

	
	public StructuralNode getFirstChild() {
		return this._firstChild; 
	}

	
	public boolean isLastChild() {
		return this._nextSibling==null;
	}

	
	public boolean isFirstChild() {
		return this._prevSibling==null;
	}

	
	public IRelationship getLatestRelationshipJoined() {
		return this._latestRelationshipJoined;
	}


	public StructuralNodeType getNodeType() {
		return this._nodeType;
	}
}
