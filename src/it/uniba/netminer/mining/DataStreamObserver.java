package it.uniba.netminer.mining;


import it.uniba.netminer.mining.lattices.HarmonicLattice;
import it.uniba.netminer.mining.lattices.IQuerySingletonDelegate;
import it.uniba.netminer.mining.lattices.Refinement;
import it.uniba.netminer.mining.lattices.Status;
import it.uniba.netminer.mining.lattices.VerticalLayoutDelegate;
import it.uniba.netminer.mining.setree.Itemset;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

public class DataStreamObserver implements Observer {
	
	private boolean _started;
	private List<Refinement> _singletons;
	private double _minimumChange;
	private float _minimumSupport;
	private float _minimumGR;
	private int _latticeDepth;
	private HarmonicLattice _remoteLattice;
	private HarmonicLattice _recentLattice;
	private BufferedWriter _logFile;
	private FileWriter _fwriter;

	public DataStreamObserver(List<Refinement> singletonList, float minSupp, float minfChange, float minGRate, int treeDepth){
//		IQuerySingletonDelegate delegate = new VerticalLayoutDelegate();
//		this._recentLattice = new HarmonicLattice(singletonList, delegate, treeDepth, minSupp);
//		this._remoteLattice = new HarmonicLattice(singletonList, delegate, treeDepth, minSupp);
//		this._minimumChange = minfChange;
//		this._minimumSupport = minSupp;
//		this._minimumGR = minGRate;
//		this._singletons = singletonList;
//		this._started = false;
//		this._latticeDepth = treeDepth;
//		
//		try {
//			this._fwriter = new FileWriter("results/stream-log.xml");
//			this._logFile = new BufferedWriter(this._fwriter);
//			this._logFile.write("<params>");
//			this._logFile.write("\t<minimumSupport>"+this._minimumSupport+"</minimumSupport>\n");
//			this._logFile.write("\t<minimumConceptChange>"+this._minimumChange+"</minimumConceptChange>\n");
//			this._logFile.write("\t<minimumGrowthRate>"+this._minimumGR+"</minimumGrowthRate>\n");
//			this._logFile.write("\t<latticeDepth>"+this._latticeDepth+"</latticeDepth>\n");
//			this._logFile.write("</params>\n");
//		}catch(IOException ex){
//			
//		}
	}

	@Override
	public void update(Observable arg, Object o){
//		try{
//			if(o == null){
//				this._logFile.close();
//				this._fwriter.close();
//				System.out.println("End of data stream reached");
//			}
//			
//			
//			if(arg instanceof DataStream &&
//			   o instanceof Pane){
//				DataStream dataStreamChanged = (DataStream)arg;
//				Pane paneAdded = (Pane)o;
//				TimeWindow timeWindowAltered = paneAdded.getWindowOwner();
//				long initTimeTask = System.currentTimeMillis();
//				
//				int numberOfPane = timeWindowAltered.getPaneCount();
//				int targetObjectsCount = timeWindowAltered.getTargetObjectsCount();
//				Date windowStartDate = timeWindowAltered.getStartDate();
//				Date windowEndDate = timeWindowAltered.getEndDate();
//				Date paneEndDate = paneAdded.getEndDate();
//				Date paneStartDate = paneAdded.getStartDate();
//				
//				System.out.println("["+paneStartDate+" - "+paneEndDate+"] log:");
//				this._logFile.write("<streamContent startDate='"+paneStartDate+"' endDate='"+paneEndDate+"' >\n");
//				
//				if(this._started){
//					if(timeWindowAltered == dataStreamChanged.getRecentPastTimeWindow()){
//						Status status;
//						Set<Itemset> emergingPatterns = null;
//						if(timeWindowAltered.getPaneCount()==1){
//							//do initialization
//							this._logFile.write("\t<resize window='recent past' startDate='"+windowStartDate+"' endDate='"+windowEndDate+"' panesCount='"+numberOfPane+"' toCount='"+targetObjectsCount+"'>\n");
//							this._recentLattice.init(paneAdded);
//							status = this._recentLattice.getStatus();
//							this._logFile.write("\t\t<status>\n");
//							this._logFile.write("\t\t\t<frequentItemsets>"+status.getCurrentFrequentItemsetsCount()+"</frequentItemsets>\n");
//							this._logFile.write("\t\t</status>\n");
//							
//							/*
//							String filenameDumpItemsets = "results/itemsets dumps/initialization itemsets "+paneStartDate+" to "+paneEndDate+".xml";
//							String filenameDumpTriplesets = "results/triplesets dumps/initialization triplesets "+paneStartDate+" to "+paneEndDate+".xml";
//							this._logFile.write("\t\t<itemsetsDump>"+filenameDumpItemsets+"</itemsetsDump>\n");
//							this._logFile.write("\t\t<itemsetsDump>"+filenameDumpTriplesets+"</itemsetsDump>\n");
//							this._recentLattice.dumpItemsetsToFile(filenameDumpItemsets);
//							this._recentLattice.dumpTriplesetsToFile(filenameDumpTriplesets);
//							*/
//							this._logFile.write("\t</resize>\n");
//						}else{
//							//do update
//							this._logFile.write("\t<update window='recent past' startDate='"+windowStartDate+"' endDate='"+windowEndDate+"' panesCount='"+numberOfPane+"' toCount='"+targetObjectsCount+"'>\n");
//							this._recentLattice.update(paneAdded);
//							status = this._recentLattice.getStatus();
//							
//							this._logFile.write("\t\t<status>\n");
//							this._logFile.write("\t\t\t<frequentItemsets>"+status.getCurrentFrequentItemsetsCount()+"</frequentItemsets>\n");
//							this._logFile.write("\t\t\t<conceptChange>"+status.getConceptChangeRate()+"%</conceptChange>\n");
//							this._logFile.write("\t\t</status>\n");
//							
//							/*
//							String filenameDumpItemsets = "results/itemsets dumps/update itemsets "+paneStartDate+" to "+paneEndDate+".xml";
//							this._logFile.write("\t\t<itemsetsDump>"+filenameDumpItemsets+"</itemsetsDump>\n");
//							this._recentLattice.dumpItemsetsToFile(filenameDumpItemsets);
//							*/
//							
//							String filenameDumpTriplesets = "results/triplesets dumps/update triplesets "+paneStartDate+" to "+paneEndDate+".xml";
//							this._logFile.write("\t\t<triplesetsDump>"+filenameDumpTriplesets+"</triplesetsDump>\n");
//							this._recentLattice.dumpTriplesetsToFile(filenameDumpTriplesets);
//
//							if(status.getConceptChangeRate() > this._minimumChange){
//								String emergingPatternDump = "results/ep dumps/emerging pattern found at "+paneEndDate+".xml";
//								emergingPatterns = this._recentLattice.getEmergingPatterns(this._remoteLattice, this._minimumGR, dataStreamChanged.getRecentPastTimeWindow().getTargetObjectsCount(), dataStreamChanged.getRemotePastTimeWindow().getTargetObjectsCount(), emergingPatternDump);
//								this._logFile.write("\t\t<emergingPatternDump count='"+emergingPatterns.size()+"'>\n");
//								this._logFile.write("\t\t\t<fromRecentPast startDate='"+dataStreamChanged.getRecentPastTimeWindow().getStartDate()+"' endDate='"+dataStreamChanged.getRecentPastTimeWindow().getEndDate()+"' />\n");
//								this._logFile.write("\t\t\t<toRemotePast startDate='"+dataStreamChanged.getRemotePastTimeWindow().getStartDate()+"' endDate='"+dataStreamChanged.getRemotePastTimeWindow().getEndDate()+"' />\n");
//								this._logFile.write("\t\t\t<filename>"+emergingPatternDump+"</filename>\n");
//								this._logFile.write("\t\t</emergingPatternDump>\n");
//								
//								//sliding phase
//								dataStreamChanged.swap();
//								IQuerySingletonDelegate delegate = new VerticalLayoutDelegate();
//								this._remoteLattice = this._recentLattice;
//								this._recentLattice = new HarmonicLattice(this._singletons, delegate, this._latticeDepth, this._minimumSupport);
//							}
//							this._logFile.write("\t</update>\n");
//						}
//						
//						System.out.println("Detected concept change rate: "+status.getConceptChangeRate()*100+"%");
//						if(emergingPatterns!=null){
//							System.out.println("Extracted emerging patterns: "+emergingPatterns.size());
//						}
//					}
//				}else{
//					if(timeWindowAltered == dataStreamChanged.getRemotePastTimeWindow()){
//						if(timeWindowAltered.getPaneCount()==1){
//							//do ECLAT
//							this._logFile.write("\t<initialization window='remote past' startDate='"+windowStartDate+"' endDate='"+windowEndDate+"' panesCount='"+numberOfPane+"' toCount='"+targetObjectsCount+"'>\n");
//							this._remoteLattice.init(paneAdded);
//							this._logFile.write("\t</initialization>\n");
//						}
//					}else if(timeWindowAltered == dataStreamChanged.getRecentPastTimeWindow()){
//						if(timeWindowAltered.getPaneCount()==dataStreamChanged.getRecentPastSize()-1){
//							//do initialization
//							this._logFile.write("\t<initialization window='recent past' startDate='"+windowStartDate+"' endDate='"+windowEndDate+"' panesCount='"+numberOfPane+"' toCount='"+targetObjectsCount+"'>\n");
//							this._recentLattice.init(paneAdded);
//							this._recentLattice.dumpItemsetsToFile("results/itemsets dumps/initialization itemsets.xml");
//							this._recentLattice.dumpTriplesetsToFile("results/triplesets dumps/initialization triplesets.xml");
//							this._started = true;
//							this._logFile.write("\t</initialization>\n");
//						}
//					}
//				}
//				
//				long duration = System.currentTimeMillis()-initTimeTask;
//				System.out.println("Task completed in "+duration+" ms");
//				this._logFile.write("\t<processingTime duration='"+duration+"' unit='ms' />\n");
//				this._logFile.write("</streamContent>\n");
//			}
//		}catch(IOException ex){
//			
//		}
	}

}
