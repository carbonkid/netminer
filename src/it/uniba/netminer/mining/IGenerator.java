package it.uniba.netminer.mining;

public interface IGenerator<T> {
	public T next();
	public boolean canGenerateNext();
}
