
package it.uniba.netminer.mining;

public interface IGeneratorFactory<T, B>{
	public boolean hasGenerator(T object);
	public IGenerable<B> getGenerator(T object);
}
