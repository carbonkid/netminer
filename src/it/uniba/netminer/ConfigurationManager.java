package it.uniba.netminer;

import java.awt.Dimension;

import it.uniba.netminer.dataschema.Database;
import it.uniba.netminer.datavisualization.RenderingContext;

public class ConfigurationManager {
	public static Database database = null;
	public static RenderingContext getRenderingContext(){
		return new RenderingContext(new Dimension(100,100));
	}
}
