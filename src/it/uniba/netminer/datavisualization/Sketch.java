package it.uniba.netminer.datavisualization;

import java.awt.Dimension;
import java.awt.Point;
import java.util.List;

import processing.core.PApplet;

public abstract class Sketch implements IDataBindable, IRenderable {
	protected Point _position;
	protected Dimension _dimension;
	protected IRenderable _owner;
	
	public Sketch(){
		super();
		this._position = new Point();
		this._dimension = new Dimension();
		this._owner = null;
	}
	
	@Override
	public Point getPosition() {
		return this._position;
	}

	@Override
	public Dimension getSize() {
		return this._dimension;
	}

	@Override
	public IRenderable getOwner() {
		return this._owner;
	}
	
	@Override
	public abstract void update(PApplet drawingInterface);

	
	@Override
	public abstract void setData(List<Object> data);
	
	@Override
	public abstract void setSelectedItem(Object obj);
	
	@Override
	public abstract Object getSelectedItem();
}
