package it.uniba.netminer.datavisualization;

import java.awt.Dimension;

import processing.core.PApplet;

public class RenderingContext implements IRenderingContext {

	private Dimension _dimension;
	private boolean _isAA;
	private boolean _isInteractive;
	
	public RenderingContext(Dimension d){
		this._dimension = d;
	}

	@Override
	public void setSize(Dimension d) {
		this._dimension = d;
	}

	@Override
	public Dimension getSize() {
		return this._dimension;
	}

	@Override
	public void setInteractive(boolean flag) {
		this._isInteractive = flag;
	}

	@Override
	public boolean isInteractive() {
		return this._isInteractive;
	}

	@Override
	public void setAntialiased(boolean flag) {
		this._isAA = flag;
	}

	@Override
	public boolean isAntialiased() {
		return this._isAA;
	}
	
	
	@Override
	public PApplet getSandbox(Sketch renderable){
		if(renderable!=null){
			RenderingContextSketch sketch = new RenderingContextSketch(renderable);
			return sketch;
		}
		return null;
	}
	
	
	
	private class RenderingContextSketch extends PApplet{
		
		private static final long serialVersionUID = -5736460115164128979L;
		private IRenderable _renderObject;
		
		private RenderingContextSketch(Sketch renderable){
			this._renderObject = renderable;
		}
		
		@Override
		public void setup(){
			super.setSize(RenderingContext.this._dimension);
			if(RenderingContext.this._isAA==true){
				super.smooth();
			}
			if(RenderingContext.this._isInteractive==false){
				super.noLoop();
			}
		}
		
		@Override
		public void draw(){
			if(this._renderObject!=null){
				this._renderObject.update(this);
			}
		}
	}

}
