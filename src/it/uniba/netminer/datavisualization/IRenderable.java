package it.uniba.netminer.datavisualization;

import java.awt.Dimension;
import java.awt.Point;

import processing.core.PApplet;

public interface IRenderable {
	public void update(PApplet drawingInterface);
	public Point getPosition();
	public void setPosition(Point pos);
	public Dimension getSize();
	public IRenderable getOwner();
}
