package it.uniba.netminer.datavisualization;

import java.awt.Dimension;
import java.awt.Point;

import processing.core.PApplet;

public abstract class ItemRenderer implements IRenderable {
	private Object _data;
	private Point _position;
	private Dimension _dimension;
	private IRenderable _owner;
	
	public ItemRenderer(){
		this._data = null;
		this._position = new Point();
		this._dimension = new Dimension();
	}
	public ItemRenderer(IRenderable owner){
		this._owner = owner;
		this._data = null;
		this._position = new Point();
		this._dimension = new Dimension();
	}
	
	public ItemRenderer(IRenderable owner, Point position){
		this._owner = owner;
		this._data = null;
		this._position = position;
		this._dimension = new Dimension();
	}
	
	public void setData(Object data){
		this._data = data;
	}
	
	public Object getData(){
		return this._data;
	}


	@Override
	public Point getPosition() {
		return this._position;
	}
	
	public void setPosition(Point pos){
		this._position = pos;
	}
	
	@Override
	public Dimension getSize() {
		return this._dimension;
	}

	@Override
	public IRenderable getOwner() {
		return this._owner;
	}
	
	@Override
	public abstract void update(PApplet drawingInterface);

}
