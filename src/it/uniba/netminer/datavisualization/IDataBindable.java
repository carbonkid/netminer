package it.uniba.netminer.datavisualization;

import java.util.List;

public interface IDataBindable {
	public void setSelectedItem(Object obj);
	public Object getSelectedItem();
	public void setData(List<Object> data);
	public List<Object> getData();
}
