package it.uniba.netminer.datavisualization;

import java.awt.Dimension;

import processing.core.PApplet;

public interface IRenderingContext {
	public void setSize(Dimension d);
	public Dimension getSize();
	public void setInteractive(boolean flag);
	public boolean isInteractive();
	public void setAntialiased(boolean flag);
	public boolean isAntialiased();
	public PApplet getSandbox(Sketch renderable);
}
