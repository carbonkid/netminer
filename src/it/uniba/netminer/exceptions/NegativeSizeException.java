package it.uniba.netminer.exceptions;

public class NegativeSizeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6236311310443287969L;

	public NegativeSizeException(){
		super();
	}
}
