package it.uniba.netminer.exceptions;

public class TreeMaximumDepthExceededException extends RuntimeException {

	private static final long serialVersionUID = -6797964927687434241L;

	public TreeMaximumDepthExceededException(){
		super();
	}
}
