package it.uniba.netminer.exceptions;

public class InvalidEntityException extends RuntimeException {
	
	private static final long serialVersionUID = 6019223665124018361L;

	public InvalidEntityException(){
		super();
	}
	
}
