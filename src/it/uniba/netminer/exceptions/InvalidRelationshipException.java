package it.uniba.netminer.exceptions;

public class InvalidRelationshipException extends RuntimeException {

	private static final long serialVersionUID = 8224779809595158923L;

	public InvalidRelationshipException(){
		super();
	}
}
