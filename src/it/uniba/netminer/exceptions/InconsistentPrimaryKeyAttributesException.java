package it.uniba.netminer.exceptions;

public class InconsistentPrimaryKeyAttributesException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InconsistentPrimaryKeyAttributesException(){
		super();
	}
}
