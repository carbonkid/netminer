package it.uniba.netminer.exceptions;

public class InvalidTargetObjectException extends RuntimeException {

	private static final long serialVersionUID = -5187585185856794364L;

	public InvalidTargetObjectException(){
		super();
	}
}
