package it.uniba.netminer.exceptions;

public class InvalidAliasException extends RuntimeException {

	private static final long serialVersionUID = 5298762163429560129L;

	public InvalidAliasException(){
		super();
	}
}
