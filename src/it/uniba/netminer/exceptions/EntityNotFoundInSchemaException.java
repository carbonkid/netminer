package it.uniba.netminer.exceptions;

public class EntityNotFoundInSchemaException extends RuntimeException {

	private static final long serialVersionUID = 2395929810266449572L;

	public EntityNotFoundInSchemaException(){
		
	}
}
