package it.uniba.netminer.exceptions;

public class UnsupportedTypeException extends RuntimeException {

	private static final long serialVersionUID = -7883436044119535619L;

	public UnsupportedTypeException(){
		super();
	}
}
