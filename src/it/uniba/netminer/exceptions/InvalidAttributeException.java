package it.uniba.netminer.exceptions;

public class InvalidAttributeException extends RuntimeException {

	private static final long serialVersionUID = -1748571460841898812L;

	public InvalidAttributeException(){
		super();
	}
}
