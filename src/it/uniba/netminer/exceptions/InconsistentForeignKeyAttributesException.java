package it.uniba.netminer.exceptions;

public class InconsistentForeignKeyAttributesException extends RuntimeException {

	private static final long serialVersionUID = -8548327174259037401L;

	public InconsistentForeignKeyAttributesException(){
		super();
	}
}
