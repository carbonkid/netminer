package it.uniba.netminer.exceptions;

public class InvalidEntityMappingException extends RuntimeException {

	private static final long serialVersionUID = 3643717864346347897L;

	public InvalidEntityMappingException(){
		super();
	}
}
