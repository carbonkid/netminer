package it.uniba.netminer.exceptions;

public class InvalidOperatorException extends RuntimeException {

	private static final long serialVersionUID = -6208993051450923319L;

	public InvalidOperatorException(){
		super();
	}
}
