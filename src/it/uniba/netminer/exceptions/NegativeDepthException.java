package it.uniba.netminer.exceptions;

public class NegativeDepthException extends RuntimeException {

	private static final long serialVersionUID = 7748351036159750453L;

	public NegativeDepthException(){
		super();
	}
}
