package it.uniba.netminer.exceptions;

public class PrototypeNotOwnedByLatticeException extends RuntimeException {

	private static final long serialVersionUID = -5696943155693557613L;

	public PrototypeNotOwnedByLatticeException(){
		super();
	}
}
