package it.uniba.netminer.assets.ui;

import it.uniba.netminer.ConfigurationManager;
import it.uniba.netminer.dataschema.IAttribute;
import it.uniba.netminer.dataschema.IDatabaseSchema;
import it.uniba.netminer.dataschema.IEntity;
import it.uniba.netminer.dataschema.IRelationship;
import it.uniba.netminer.exceptions.EntityNotFoundInSchemaException;
import it.uniba.netminer.mining.MiningProcess;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;
import javax.swing.LayoutStyle.ComponentPlacement;


@SuppressWarnings("serial")
public class NetMinerMiningDialog extends JDialog {	
	
	private final JPanel miningPanel = new JPanel();
	private JComboBox _comboBoxTablesNames;
	private JComboBox _comboBoxStartNodeTable;
	private JComboBox _comboBoxEndNodeTable;
	private JComboBox _comboBoxTablesEdgeNames;
	private JSpinner _spinnerForeignKeyDeepness;
	private JPanel buttonPane;
	private JComboBox _comboEdgeTypeFields;
	private JComboBox _comboDateFields;
	private JSpinner _spinnerRecentSize;
	private JSpinner _spinnerRemoteSize;
	private JSpinner _spinnerMinimumSupport;
	private JSpinner _spinnerMinimumFChange;
	private JSpinner _spinnerMinimumGrowthRate;
	private JSpinner _spinnerPaneSize;


	/**
	 * Create the dialog.
	 */
	public NetMinerMiningDialog(JFrame parent) {
		super(parent);
		setModal(true);
		setResizable(false);
		setTitle("Target Object Settings");
		setBounds(100, 100, 454, 440);
		miningPanel.setBorder(new TitledBorder(null, "Mining parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		miningPanel.setLayout(null);
		{
			buttonPane = new JPanel();
			buttonPane.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						 onClickConfirmButton(e);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						onClickCancelButton(e);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		JPanel streamPane = new JPanel();
		streamPane.setBorder(new TitledBorder(null, "Data stream parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(6)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(miningPanel, GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE)
							.addGap(6))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(streamPane, GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)
							.addGap(4))))
				.addComponent(buttonPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(streamPane, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(miningPanel, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(buttonPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(58, Short.MAX_VALUE))
		);
		
		_spinnerMinimumSupport = new JSpinner();
		_spinnerMinimumSupport.setBounds(199, 23, 47, 20);
		miningPanel.add(_spinnerMinimumSupport);
		_spinnerMinimumSupport.setModel(new SpinnerNumberModel(new Float(1), new Float(0), new Float(100), new Float(0)));
		
		JLabel lblMinimumSupportThreshold = new JLabel("Minimum support threshold:");
		lblMinimumSupportThreshold.setBounds(12, 23, 153, 16);
		miningPanel.add(lblMinimumSupportThreshold);
		
		JLabel lblMinimumChangeThreshold = new JLabel("Minimum change threshold:");
		lblMinimumChangeThreshold.setBounds(12, 47, 151, 16);
		miningPanel.add(lblMinimumChangeThreshold);
		
		_spinnerMinimumFChange = new JSpinner();
		_spinnerMinimumFChange.setModel(new SpinnerNumberModel(new Float(2), new Float(0), new Float(100), new Float(1)));
		_spinnerMinimumFChange.setBounds(199, 47, 47, 20);
		miningPanel.add(_spinnerMinimumFChange);
		
		JLabel lblMinimumGrowthrateThreshold = new JLabel("Minimum growth-rate threshold:");
		lblMinimumGrowthrateThreshold.setBounds(12, 71, 175, 16);
		miningPanel.add(lblMinimumGrowthrateThreshold);
		
		_spinnerMinimumGrowthRate = new JSpinner();
		_spinnerMinimumGrowthRate.setModel(new SpinnerNumberModel(new Float(10), new Float(0), null, new Float(1)));
		_spinnerMinimumGrowthRate.setBounds(199, 71, 47, 20);
		miningPanel.add(_spinnerMinimumGrowthRate);
		streamPane.setLayout(null);
		
		JLabel lblTargetTable = new JLabel("Target table:");
		lblTargetTable.setBounds(12, 29, 73, 14);
		streamPane.add(lblTargetTable);
		
		_comboBoxTablesNames = new JComboBox();
		_comboBoxTablesNames.setBounds(81, 26, 341, 20);
		streamPane.add(_comboBoxTablesNames);
		
		JLabel lblForeignKeyTree = new JLabel("Lattice maximum depth:");
		lblForeignKeyTree.setBounds(12, 188, 123, 14);
		streamPane.add(lblForeignKeyTree);
		
		_spinnerForeignKeyDeepness = new JSpinner();
		_spinnerForeignKeyDeepness.setBounds(166, 185, 47, 20);
		streamPane.add(_spinnerForeignKeyDeepness);
		_spinnerForeignKeyDeepness.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		
		JLabel lblTimestampReferenceFields = new JLabel("Date field:");
		lblTimestampReferenceFields.setBounds(12, 129, 93, 14);
		streamPane.add(lblTimestampReferenceFields);
		
		JLabel lblInitialRemoteWindow = new JLabel("Start remote past size (pane):");
		lblInitialRemoteWindow.setBounds(12, 215, 145, 16);
		streamPane.add(lblInitialRemoteWindow);
		
		_spinnerRecentSize = new JSpinner();
		_spinnerRecentSize.setModel(new SpinnerNumberModel(new Integer(31), new Integer(0), null, new Integer(1)));
		_spinnerRecentSize.setBounds(166, 214, 47, 20);
		streamPane.add(_spinnerRecentSize);
		
		JLabel lblInitialRecentWindow = new JLabel("Start recent past size (pane):");
		lblInitialRecentWindow.setBounds(223, 215, 150, 16);
		streamPane.add(lblInitialRecentWindow);
		
		_spinnerRemoteSize = new JSpinner();
		_spinnerRemoteSize.setModel(new SpinnerNumberModel(new Integer(31), new Integer(0), null, new Integer(1)));
		_spinnerRemoteSize.setBounds(376, 214, 50, 20);
		streamPane.add(_spinnerRemoteSize);
		
		JLabel lblEdgeTable = new JLabel("Edge table path:");
		lblEdgeTable.setBounds(12, 54, 93, 14);
		streamPane.add(lblEdgeTable);
		
		JLabel lblNodeTable = new JLabel("Start node path:");
		lblNodeTable.setBounds(12, 79, 93, 14);
		streamPane.add(lblNodeTable);
		
		this._comboBoxTablesEdgeNames = new JComboBox();
		_comboBoxTablesEdgeNames.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onComboBoxTablesEdgeSelectionChange(arg0);
			}
		});
		_comboBoxTablesEdgeNames.setBounds(113, 51, 309, 20);
		streamPane.add(_comboBoxTablesEdgeNames);
		
		this._comboBoxStartNodeTable = new JComboBox();
		_comboBoxStartNodeTable.setBounds(113, 76, 309, 20);
		streamPane.add(_comboBoxStartNodeTable);
		
		JLabel lblEndNodePath = new JLabel("End node path:");
		lblEndNodePath.setBounds(12, 104, 93, 14);
		streamPane.add(lblEndNodePath);
		
		this._comboBoxEndNodeTable = new JComboBox();
		_comboBoxEndNodeTable.setBounds(113, 101, 309, 20);
		streamPane.add(_comboBoxEndNodeTable);
		
		JLabel lblEdgeDescriptionAttribute = new JLabel("Edge type field:");
		lblEdgeDescriptionAttribute.setBounds(12, 154, 98, 14);
		streamPane.add(lblEdgeDescriptionAttribute);
		
		_comboDateFields = new JComboBox();
		_comboDateFields.setBounds(113, 126, 309, 20);
		streamPane.add(_comboDateFields);
		
		_comboEdgeTypeFields = new JComboBox();
		_comboEdgeTypeFields.setBounds(113, 151, 309, 20);
		streamPane.add(_comboEdgeTypeFields);
		
		JLabel lblNewLabel = new JLabel("Pane of transaction size:");
		lblNewLabel.setBounds(223, 188, 139, 14);
		streamPane.add(lblNewLabel);
		
		_spinnerPaneSize = new JSpinner();
		_spinnerPaneSize.setBounds(376, 186, 50, 20);
		streamPane.add(_spinnerPaneSize);
		_comboBoxTablesNames.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onComboBoxTablesNamesSelectionChange(e);
			}
		});
		getContentPane().setLayout(groupLayout);
		
		this.onCreationComplete();
	}
	
	
	private void onClickConfirmButton(ActionEvent evn){
		IEntity targetEntity = (IEntity)this._comboBoxTablesNames.getSelectedItem();
		IRelationship edgeEntity = (IRelationship)this._comboBoxTablesEdgeNames.getSelectedItem();
		List<IRelationship> startNodePath = (List<IRelationship>)this._comboBoxStartNodeTable.getSelectedItem();
		List<IRelationship> endNodePath = (List<IRelationship>)this._comboBoxEndNodeTable.getSelectedItem();
		IAttribute targetAttribute = (IAttribute)this._comboDateFields.getSelectedItem();
		IAttribute edgeTypeAttribute = (IAttribute)this._comboEdgeTypeFields.getSelectedItem();
		int depth = (Integer)this._spinnerForeignKeyDeepness.getValue();
		int paneSize = (Integer)this._spinnerPaneSize.getValue();
		int initialRemoteSize = (Integer)this._spinnerRemoteSize.getValue();
		int initialRecentSize = (Integer)this._spinnerRecentSize.getValue();
		
		float minimumSupport = (Float)this._spinnerMinimumSupport.getValue()/100;
		float minimumFChange = (Float)this._spinnerMinimumFChange.getValue()/100;
		float minimumGrowthRate = (Float)this._spinnerMinimumGrowthRate.getValue()/100;
		
		
		Thread process = new MiningProcess(
			targetEntity,
			targetAttribute,
			edgeTypeAttribute,
			edgeEntity,
			startNodePath,
			endNodePath,
			minimumSupport,
			minimumFChange,
			minimumGrowthRate,
			depth,
			paneSize,
			initialRemoteSize,
			initialRecentSize
		);
	
		process.start();
		JInternalFrame timeWindowFrame = new NetMinerMiningTimeWindows((MiningProcess)process); 
		timeWindowFrame.setVisible(true);
		((JFrame)this.getParent()).getContentPane().add(timeWindowFrame);
		this.dispose();
	}
	
	
	private void onClickCancelButton(ActionEvent evn){
		this.dispose();
	}
	
	
	private void onComboBoxTablesEdgeSelectionChange(ActionEvent evn){
		DefaultComboBoxModel modelNodeA = new DefaultComboBoxModel();
		DefaultComboBoxModel modelNodeB = new DefaultComboBoxModel();
		DefaultComboBoxModel modelEdgeType = new DefaultComboBoxModel();
		if(this._comboBoxTablesEdgeNames.getSelectedIndex() != -1){
			IRelationship relationship = (IRelationship)this._comboBoxTablesEdgeNames.getSelectedItem();
			IEntity table = relationship.getRelatedEntity();
			
			for(IRelationship foreignKey : table.getForeignKeyList()){
				IEntity middleTable = foreignKey.getRelatedEntity();
				
				for(IAttribute attribute : foreignKey.getRelatedEntity()){
					modelEdgeType.addElement(attribute);
				}
				
				for(IRelationship nextForeignKey : middleTable.getForeignKeyList()){
					LinkedList<IRelationship> nodePath = new LinkedList<IRelationship>();
					nodePath.push(nextForeignKey);
					nodePath.push(foreignKey);
					modelNodeA.addElement(nodePath);
					modelNodeB.addElement(nodePath);
				}
			}
		}
		
		this._comboBoxStartNodeTable.setModel(modelNodeA);
		this._comboBoxEndNodeTable.setModel(modelNodeB);
		this._comboEdgeTypeFields.setModel(modelEdgeType);
	}
	
	
	
	private void onComboBoxTablesNamesSelectionChange(ActionEvent evn){
		JComboBox tableComboBox = (JComboBox)evn.getSource();
		if(tableComboBox.getSelectedIndex() != -1){
			IEntity table = (IEntity)tableComboBox.getSelectedItem();
			DefaultComboBoxModel timeAttributesModel = new DefaultComboBoxModel();
			DefaultComboBoxModel edgeModel = new DefaultComboBoxModel();
			
			for(IRelationship foreignKey : table.getForeignKeyList()){
				edgeModel.addElement(foreignKey);
			}
			
			for(IAttribute attribute : table){
				if(attribute.getDataType().getSqlType() == Types.DATE ||
				   attribute.getDataType().getSqlType() == Types.TIMESTAMP){
					timeAttributesModel.addElement(attribute);
				}
			}
			
			this._comboBoxTablesEdgeNames.setModel(edgeModel);
			this._comboDateFields.setModel(timeAttributesModel);
		}
	}
	
	
	
	private void onCreationComplete(){
		DefaultComboBoxModel targetTableModel = new DefaultComboBoxModel();
		DefaultComboBoxModel edgeTableModel = new DefaultComboBoxModel();
		DefaultComboBoxModel startNodeTableModel = new DefaultComboBoxModel();
		DefaultComboBoxModel endNodeTableModel = new DefaultComboBoxModel();
		
		if(ConfigurationManager.database!=null){
			IDatabaseSchema schema = ConfigurationManager.database.getDatabaseSchema();
			try{				
				for(IEntity table : schema){
					targetTableModel.addElement(table);
				}
			}catch(EntityNotFoundInSchemaException ex){
				
			}
		}
		
		this._comboBoxTablesNames.setModel(targetTableModel);
		this._comboBoxStartNodeTable.setModel(startNodeTableModel);
		this._comboBoxEndNodeTable.setModel(endNodeTableModel);
		this._comboBoxTablesEdgeNames.setModel(edgeTableModel);
	}
}
