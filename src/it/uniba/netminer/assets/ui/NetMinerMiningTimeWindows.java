package it.uniba.netminer.assets.ui;

import it.uniba.netminer.ConfigurationManager;
import it.uniba.netminer.dataaccess.DatabaseSession;
import it.uniba.netminer.dataaccess.ISessionDelegate;
import it.uniba.netminer.datavisualization.RenderingContext;
import it.uniba.netminer.mining.DataStream;
import it.uniba.netminer.mining.MiningProcess;
import it.uniba.netminer.mining.TargetObject;
import it.uniba.netminer.mining.visualization.GraphVisualizer;
import it.uniba.netminer.mining.visualization.InteractiveGraphVisualizer;
import it.uniba.netminer.mining.visualization.NodeItemRenderer;
import it.uniba.netminer.mining.visualization.ToolNodeItemRenderer;

import javax.swing.JInternalFrame;
import javax.swing.JTree;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Rectangle;

import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;

import org.hibernate.Query;
import org.hibernate.Session;

import processing.core.PApplet;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class NetMinerMiningTimeWindows extends JInternalFrame {
	
	private JTree _tree;

	/**
	 * Create the frame.
	 */
	public NetMinerMiningTimeWindows(MiningProcess p) {
		setTitle("Analysis Monitor");
		setResizable(true);
		setBounds(100, 100, 278, 412);
		
		DataStream stream = p.getDataStream();
		TreeModel streamModel = stream.model();
		getContentPane().setLayout(new GridLayout(1, 1, 0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane);
		
		this._tree = new JTree();
		this._tree.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				onTreeClickAction(arg0);
			}
		});
		
		scrollPane.setViewportView(this._tree);
		this._tree.setModel(streamModel);
		this._tree.setBounds(10, 83, 262, 382);
	}
	
	
	private void onTreeClickAction(MouseEvent event){
		if(event!=null){
			if(SwingUtilities.isRightMouseButton(event)==true){
				final TreePath path = this._tree.getPathForLocation(event.getX (), event.getY());
				if(path!=null){
					Rectangle pathBounds = this._tree.getUI().getPathBounds(this._tree, path );
					if(path.getLastPathComponent() instanceof TargetObject){
		                if (pathBounds != null && pathBounds.contains(event.getX(), event.getY())){
		                	this._tree.setSelectionPath(path);
		                	
		                    JPopupMenu menu = new JPopupMenu();
		                    JMenuItem item = new JMenuItem("Render graph");
		                    
		                    item.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent arg0) {
									onPopupMenuRenderGraphClicked(arg0, (TargetObject)path.getLastPathComponent());
								}
		                    });
		                    
		                    menu.add(item);
		                    menu.show(this._tree, event.getX(), event.getY());
		                }
					}
				}
			}
		}
	}
	
	private void onPopupMenuRenderGraphClicked(ActionEvent event, TargetObject object){
		final TargetObject obj = object;
		if(object!=null){
			new DatabaseSession(
				new ISessionDelegate(){
					@Override
					public void onSessionFailed(Exception exception) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSessionOpened(Session session)
							throws SQLException {
						Date toDate = obj.getDate();
						
						String query = "select T2 from nets T0, linksowned T1, links T2 " +
								"where T0.netId = T1.netId and T1.linkId = T2.linkId " +
								"AND T0.date = :netDate";
						Query queryHQL = session.createQuery(query);
						queryHQL.setParameter("netDate", toDate);
						List<Object> result = queryHQL.list();
						
						RenderingContext context = ConfigurationManager.getRenderingContext();
						context.setSize(new Dimension(500,500));
						context.setInteractive(true);
						context.setAntialiased(true);
						
						InteractiveGraphVisualizer sketch = new InteractiveGraphVisualizer();
						sketch.setNodeItemRenderer(ToolNodeItemRenderer.class);
						sketch.setData(result);
						
						PApplet framebuffer = context.getSandbox(sketch);
						
						/*
						RoundGraphSketch graph = new RoundGraphSketch();
						graph.setGetterFirstNodeInEdge("node1");
						graph.setGetterSecondNodeInEdge("node2");
						graph.setData((List<Map<?, ?>>) result);
						*/
						
						JInternalFrame netGraphFrame = new JInternalFrame("net image at "+toDate);
						netGraphFrame.setClosable(true);  
						netGraphFrame.setIconifiable(true);  
						netGraphFrame.setLayout(new BorderLayout());
						netGraphFrame.add(framebuffer, BorderLayout.CENTER);
						netGraphFrame.setVisible(true);
						netGraphFrame.setSize(500,550);
						netGraphFrame.setResizable(false);
						
						((JFrame)NetMinerMiningTimeWindows.this.getParent().getParent().getParent().getParent()).getContentPane().add(netGraphFrame);
						framebuffer.init();
					}
				}
			).dispatch();
		}
	}
}
