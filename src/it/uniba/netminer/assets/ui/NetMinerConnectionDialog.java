package it.uniba.netminer.assets.ui;

import it.uniba.netminer.ConfigurationManager;
import it.uniba.netminer.dataaccess.DatabaseSession;
import it.uniba.netminer.dataaccess.ISessionDelegate;
import it.uniba.netminer.dataschema.Database;
import it.uniba.netminer.dataschema.IAttribute;
import it.uniba.netminer.dataschema.IEntity;
import it.uniba.netminer.dataschema.IForeignKeysList;
import it.uniba.netminer.dataschema.IRelationship;
import it.uniba.netminer.dataschema.IRelationshipField;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.TitledBorder;

import org.hibernate.Session;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NetMinerConnectionDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField _textFieldDatabaseURL;
	private JTextField _textFieldDatabaseName;
	private JTextField _textFieldDatabaseUsername;
	private JTextField _textFieldDatabasePassword;
	private JComboBox _comboBoxDatabaseType;
	private JComboBox _comboBoxDatabaseDialect;


	/**
	 * Create the dialog.
	 */
	public NetMinerConnectionDialog(JFrame parent) {
		super(parent);
		setTitle("Connection Properties");
		setResizable(false);
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("In order to perform data mining tasks, users must provide a database connection.");
			lblNewLabel.setBounds(10, 11, 414, 14);
			contentPanel.add(lblNewLabel);
		}
		
		JLabel lblDatabaseUrl = new JLabel("Database URL:");
		lblDatabaseUrl.setBounds(10, 36, 103, 14);
		contentPanel.add(lblDatabaseUrl);
		
		JLabel lblDatabaseName = new JLabel("Database Name:");
		lblDatabaseName.setBounds(10, 61, 103, 14);
		contentPanel.add(lblDatabaseName);
		
		JLabel lblDatabaseUsername = new JLabel("Database Username:");
		lblDatabaseUsername.setBounds(10, 86, 103, 14);
		contentPanel.add(lblDatabaseUsername);
		
		_textFieldDatabaseURL = new JTextField();
		_textFieldDatabaseURL.setText("jdbc:mysql://localhost");
		_textFieldDatabaseURL.setBounds(127, 33, 297, 20);
		contentPanel.add(_textFieldDatabaseURL);
		_textFieldDatabaseURL.setColumns(10);
		
		_textFieldDatabaseName = new JTextField();
		_textFieldDatabaseName.setText("kedsql");
		_textFieldDatabaseName.setBounds(127, 58, 297, 20);
		contentPanel.add(_textFieldDatabaseName);
		_textFieldDatabaseName.setColumns(10);
		
		_textFieldDatabaseUsername = new JTextField();
		_textFieldDatabaseUsername.setText("root");
		_textFieldDatabaseUsername.setBounds(127, 83, 297, 20);
		contentPanel.add(_textFieldDatabaseUsername);
		_textFieldDatabaseUsername.setColumns(10);
		
		JLabel lblDatabasePassword = new JLabel("Database Password:");
		lblDatabasePassword.setBounds(10, 111, 103, 14);
		contentPanel.add(lblDatabasePassword);
		
		_textFieldDatabasePassword = new JTextField();
		_textFieldDatabasePassword.setBounds(127, 108, 297, 20);
		contentPanel.add(_textFieldDatabasePassword);
		_textFieldDatabasePassword.setColumns(10);
		
		JLabel lblDatabaseDialect = new JLabel("Database Dialect:");
		lblDatabaseDialect.setBounds(10, 161, 103, 14);
		contentPanel.add(lblDatabaseDialect);
		
		_comboBoxDatabaseType = new JComboBox();
		_comboBoxDatabaseType.setModel(new DefaultComboBoxModel(new String[] {"com.mysql.jdbc.Driver"}));
		_comboBoxDatabaseType.setBounds(127, 133, 297, 20);
		contentPanel.add(_comboBoxDatabaseType);
		
		JLabel lblDatabaseTypeDriver = new JLabel("Database Driver:");
		lblDatabaseTypeDriver.setBounds(10, 136, 103, 14);
		contentPanel.add(lblDatabaseTypeDriver);
		
		_comboBoxDatabaseDialect = new JComboBox();
		_comboBoxDatabaseDialect.setModel(new DefaultComboBoxModel(new String[] {"org.hibernate.dialect.MySQLDialect"}));
		_comboBoxDatabaseDialect.setBounds(127, 158, 297, 20);
		contentPanel.add(_comboBoxDatabaseDialect);
		
		JButton buttonTestConnection = new JButton("Test connection");
		buttonTestConnection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onClickTestConnectionButton(arg0);
			}
		});
		buttonTestConnection.setBounds(315, 189, 109, 23);
		contentPanel.add(buttonTestConnection);
		buttonTestConnection.setActionCommand("OK");
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						onClickConfirmButton(arg0);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						onClickCancelButton(arg0);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	
	private void onClickCancelButton(ActionEvent evn){
		this.dispose();
	}
	
	
	private void onClickConfirmButton(ActionEvent evn){
		//impostiamo un oggetto temporaneo di configurazione per la connessione al database
		String dialect = this._comboBoxDatabaseDialect.getSelectedItem().toString();
		String driverClass = this._comboBoxDatabaseType.getSelectedItem().toString();
		String url = this._textFieldDatabaseURL.getText();
		String name = this._textFieldDatabaseName.getText();
		String username = this._textFieldDatabaseUsername.getText();
		String password = this._textFieldDatabasePassword.getText();
		Database db = new Database(url, name, username, password, dialect, driverClass);
		ConfigurationManager.database = db;
		ConfigurationManager.database.loadSchema();
		
		
		for(IEntity entity : db.getDatabaseSchema()){
			System.out.println(entity);
			System.out.println("   "+entity.getPrimaryKey());
			for(IAttribute attribute : entity){
				System.out.println("   "+attribute);
			}
			IForeignKeysList fkList = entity.getForeignKeyList();
			for(IRelationship foreignKey : fkList){
				System.out.println("   "+foreignKey);
				for(IRelationshipField field : foreignKey){
					System.out.println("      "+field);
				}
			}
		}
		
		
		new DatabaseSession(
			new ISessionDelegate(){
				@Override
				public void onSessionOpened(Session session) {
					/*
					List<Links> uList = session.createQuery("FROM Links WHERE day='18' AND month='05' AND year='1979'").list();
					for(Links edge : uList){
						System.out.println("Relation ("+edge.getNodeA()+", "+edge.getNodeB()+") at "+edge.getDay()+"/"+edge.getMonth()+"/"+edge.getYear());
					}
						
					
					RoundGraphSketch<Links> graph = new RoundGraphSketch<Links>();
					graph.setReferenceClass(Links.class);
					graph.setGetterFirstNodeInEdge("getNodeA");
					graph.setGetterSecondNodeInEdge("getNodeB");
					graph.setData(uList);
					
					
					JInternalFrame netGraphFrame = new JInternalFrame("net image at 15-04-1979");
					netGraphFrame.setClosable(true);  
					netGraphFrame.setIconifiable(true);  
					netGraphFrame.setLayout(new BorderLayout());
					netGraphFrame.add(graph, BorderLayout.CENTER);
					netGraphFrame.setVisible(true);
					netGraphFrame.setSize(500,550);
					netGraphFrame.setResizable(false);
					
					((JFrame)NetMinerConnectionDialog.this.getParent()).getContentPane().add(netGraphFrame);
					graph.init();
					*/
					dispose();
				}

				@Override
				public void onSessionFailed(Exception exception) {
					exception.printStackTrace();
				}
			}
		).dispatch();
		
		
	}
	
	
	/**
	 * Funzione di callback per il click sul pulsante di test della connessione.
	 * Semplicemente, in base ai dati inseriti dall'utente, crea un istanza di "Configuration"
	 * e prova un accesso notificando l'esito della connessione.
	 * 
	 * @param evn ActionEvent
	 */
	private void onClickTestConnectionButton(ActionEvent evn){
		String dialect = this._comboBoxDatabaseDialect.getSelectedItem().toString();
		String driverClass = this._comboBoxDatabaseType.getSelectedItem().toString();
		String url = this._textFieldDatabaseURL.getText();
		String name = this._textFieldDatabaseName.getText();
		String username = this._textFieldDatabaseUsername.getText();
		String password = this._textFieldDatabasePassword.getText();
		Database dataProvider = new Database(url, name, username, password, dialect, driverClass);
		
		//proviamo i dati di connessione
		final Database database = ConfigurationManager.database;
		ConfigurationManager.database = dataProvider; 
				
		new DatabaseSession(
			new ISessionDelegate(){
				@Override
				public void onSessionOpened(Session session) {
					ConfigurationManager.database = database;
					JOptionPane.showMessageDialog(
							NetMinerConnectionDialog.this, "Connection test successfully exceeded.", 
							"Success", JOptionPane.INFORMATION_MESSAGE);
				}

				@Override
				public void onSessionFailed(Exception exception) {
					ConfigurationManager.database = database;
					JOptionPane.showMessageDialog(
							NetMinerConnectionDialog.this, "Connection test not exceeded, something went wrong.", 
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		).dispatch();
	}
	
	
}
