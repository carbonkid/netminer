package it.uniba.netminer.assets.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class NetMinerFrame extends JFrame {
	
	private JMenuBar menuBar;
	private JDesktopPane _desktopPane;
	
	
	public NetMinerFrame(){
		super("NetMiner");
		
		this.drawElements();
	}
	
	private void drawElements(){
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.menuBar = new JMenuBar();
		
		this._desktopPane = new JDesktopPane();
		//this._desktopPane.putClientProperty("JDesktopPane.dragMode", "outline");
		this._desktopPane.setBackground(this.getContentPane().getBackground());
		this._desktopPane.setForeground(this.getContentPane().getForeground());
		this.setContentPane(this._desktopPane);
		
		JMenu fileMenu = new JMenu("File");
	    fileMenu.setMnemonic(KeyEvent.VK_F);
	    JMenuItem newMenuItem = new JMenuItem("Connect", KeyEvent.VK_C);
	    newMenuItem.addActionListener(
	    	new ActionListener(){
	    		public void actionPerformed(ActionEvent evn){
	    			onFileMenuConnectClick(evn);
	    		}
	    	}
	    );
	    fileMenu.add(newMenuItem);
	    this.menuBar.add(fileMenu);
	    
	    JMenu miningMenu = new JMenu("Mining");
	    miningMenu.setMnemonic(KeyEvent.VK_M);
	    JMenuItem toSettingsMenuItem = new JMenuItem("Target object settings", KeyEvent.VK_C);
	    toSettingsMenuItem.addActionListener(
	    	new ActionListener(){
	    		public void actionPerformed(ActionEvent evn){
	    			onMiningMenuTOParamsClick(evn);
	    		}
	    	}
	    );
	    miningMenu.add(toSettingsMenuItem);
	    this.menuBar.add(miningMenu);
	    
	    
	    this.setJMenuBar(this.menuBar);
	    this.setSize(600, 400);
		this.setVisible(true);
	}
	
	
	/**
	 * Apre la form di connessione ad un database.
	 * @param evn
	 */
	private void onFileMenuConnectClick(ActionEvent evn){
		NetMinerConnectionDialog dialog = new NetMinerConnectionDialog(this);
		dialog.setVisible(true);
	}
	
	
	/**
	 * Apre la form di configurazione dei parametri per i target objects.
	 * @param evn
	 */
	private void onMiningMenuTOParamsClick(ActionEvent evn){
		NetMinerMiningDialog dialog = new NetMinerMiningDialog(this);
		dialog.setVisible(true);
	}


}
