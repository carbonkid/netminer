package it.uniba.netminer.dataaccess;

public interface ICacheMethod {
	public String getDescription();
	public String getAttribute();
}
