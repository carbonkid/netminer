package it.uniba.netminer.dataaccess;

import it.uniba.netminer.dataschema.IEntity;

public interface IEntityMapping {
	public IEntity getEntityMapped();
	public ICacheMethod getCacheMethod();
}
