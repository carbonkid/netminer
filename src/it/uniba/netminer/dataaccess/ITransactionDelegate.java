package it.uniba.netminer.dataaccess;

import java.sql.Connection;
import java.sql.SQLException;

import org.hibernate.Session;

public interface ITransactionDelegate extends IDatabaseDelegate {
	public void onSessionOpened(Connection connection, Session session) throws SQLException;
}
