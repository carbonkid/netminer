package it.uniba.netminer.dataaccess;

import java.util.ArrayList;

import it.uniba.netminer.dataschema.IEntity;

public interface IMappingManager {
	public void refreshSessionFactoryWithMappings(ArrayList<IEntity> entities);
}
