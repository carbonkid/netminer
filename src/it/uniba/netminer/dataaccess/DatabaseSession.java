package it.uniba.netminer.dataaccess;

import it.uniba.netminer.ConfigurationManager;

import java.sql.Connection;
import java.sql.SQLException;

import org.hibernate.Transaction;
import org.hibernate.jdbc.Work;

public final class DatabaseSession {
	
	/**
	 * Un valido delegato anonimo per la creazione di compiti
	 * sul database. Il delegato viene eseguito in seguito ad una
	 * chiamata del metodo dispatchWork.
	 */
	private IDatabaseDelegate _delegate;
	
	public DatabaseSession(IDatabaseDelegate delegate){
		this._delegate = delegate;
		
	}
	
	public void dispatch(){
		if(ConfigurationManager.database != null){
			final MappingManager manager = (MappingManager)ConfigurationManager.database.getMappingManager();
		
			Transaction transaction = null;
			try{
				transaction = manager._sessionFactory.getCurrentSession().getTransaction();
				transaction.begin();
				if(this._delegate instanceof ITransactionDelegate){
					manager._sessionFactory.getCurrentSession().doWork(
						new Work() {
						    public void execute(Connection connection) throws SQLException {
						    	((ITransactionDelegate)_delegate).
						    	onSessionOpened(connection, manager._sessionFactory.getCurrentSession());
						    }
						}
					);
				}else if(this._delegate instanceof ISessionDelegate){
					((ISessionDelegate)_delegate).onSessionOpened(manager._sessionFactory.getCurrentSession());
				}
				manager._sessionFactory.getCurrentSession().flush();
				manager._sessionFactory.getCurrentSession().clear();
				transaction.commit();
				
			}catch(Exception ex){
				if(transaction!=null){
					manager._sessionFactory.getCurrentSession().flush();
					manager._sessionFactory.getCurrentSession().clear();
					transaction.rollback();
				}

				
				_delegate.onSessionFailed(ex);
				ex.printStackTrace();
			}finally{
				
			}
		}
	}
}
