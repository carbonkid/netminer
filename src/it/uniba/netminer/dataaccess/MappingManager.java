package it.uniba.netminer.dataaccess;

import java.util.ArrayList;

import it.uniba.netminer.dataschema.IEntity;
import it.uniba.netminer.exceptions.InvalidEntityException;

import org.hibernate.MappingException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.dom4j.DocumentException;
import org.dom4j.io.DOMWriter;


/**
 * La classe MappingManager si occupa di mediare la creazione di
 * un mapping Hibernate, e quindi di realizzare l'allineamento ORM
 * fra una relazione nel database e una classe POJO-like java,
 * inviando i documenti (DOM - Document Object Model) al gestore
 * interno della configurazione.
 * E' importante che sia i dettagli del mapping che quelli della
 * configurazione siano nascosti all'utente dell'applicazione.
 * 
 * @author Angelo Impedovo
 */
public class MappingManager implements IMappingManager {
	
	protected Configuration _configuration;
	protected SessionFactory _sessionFactory;
	protected String _url;
	protected String _name;
	protected String _username;
	protected String _password;
	protected String _dialect;
	protected String _driverClass;
	

	
	public MappingManager(String url, String name, String username, String password, String dialect, String driverClass){
		this._url = url;
		this._name = name;
		this._username = username;
		this._password = password;
		this._dialect = dialect;
		this._driverClass = driverClass;
		
		this._configuration = new Configuration();
		this._configuration.setProperty("hibernate.dialect", this._dialect)
		 .setProperty("hibernate.connection.driver_class", this._driverClass)
		 .setProperty("hibernate.connection.url", this._url+"/"+this._name)
		 .setProperty("hibernate.connection.username", this._username)
		 .setProperty("hibernate.connection.password", this._password)
		 .setProperty("hibernate.default_entity_mode", "dynamic-map")
		 .setProperty("hibernate.current_session_context_class", "thread")
		 .setProperty("hibernate.transaction.factory_class", "org.hibernate.transaction.JDBCTransactionFactory");
		
		this._sessionFactory = this._configuration.buildSessionFactory();
	}

	
	
	protected void shutdown(){
		this._sessionFactory.close();
	}




	@Override
	public void refreshSessionFactoryWithMappings(ArrayList<IEntity> entities) {
		this.shutdown();

		this._configuration = new Configuration();
		this._configuration.setProperty("hibernate.dialect", this._dialect)
		 .setProperty("hibernate.connection.driver_class", this._driverClass)
		 .setProperty("hibernate.connection.url", this._url+"/"+this._name)
		 .setProperty("hibernate.connection.username", this._username)
		 .setProperty("hibernate.connection.password", this._password)
		 .setProperty("hibernate.default_entity_mode", "dynamic-map")
		 .setProperty("hibernate.current_session_context_class", "thread")
         .setProperty("hibernate.transaction.factory_class", "org.hibernate.transaction.JDBCTransactionFactory");
		
		try {
			for(IEntity entity : entities){
				EntityMapping mapping = new EntityMapping(entity, CacheMethod.READ_ONLY_STRATEGY);
				this._configuration.addDocument(new DOMWriter().write(mapping.getMappingXML()));
			}
			this._sessionFactory = this._configuration.buildSessionFactory();
		} catch (MappingException e) {
			throw new InvalidEntityException();
		} catch (DocumentException e) {
			throw new InvalidEntityException();
		}
		
	}
}
