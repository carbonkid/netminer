package it.uniba.netminer.dataaccess;

public interface IDataAccessor {
	public IMappingManager getMappingManager();
}
