package it.uniba.netminer.dataaccess;

import java.sql.SQLException;

import org.hibernate.Session;

public interface ISessionDelegate extends IDatabaseDelegate {	
	public void onSessionOpened(Session session) throws SQLException;
}
