package it.uniba.netminer.dataaccess;

import it.uniba.netminer.dataschema.IQueryable;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

public class QueryDelegate implements ISessionDelegate {
	
	private List<?> _results;
	private IQueryable _queryObject;
	
	public QueryDelegate(IQueryable queryObject){
		this._queryObject = queryObject;
	}

	@Override
	public void onSessionFailed(Exception exception) {
		this._results = null;
		exception.printStackTrace();
	}

	@Override
	public void onSessionOpened(Session session) throws SQLException {
		String hql = this._queryObject.toHQL();
		Query query = session.createQuery(hql);
		
		if(this._queryObject.getLimitCount()>-1 &&
		   this._queryObject.getLimitIndex()>-1){
			query.setFirstResult(this._queryObject.getLimitIndex());
			query.setMaxResults(this._queryObject.getLimitCount());
		}
		
		this._results = query.list();
	}
	
	
	public List<?> getResultSet(){
		return this._results;
	}

}
