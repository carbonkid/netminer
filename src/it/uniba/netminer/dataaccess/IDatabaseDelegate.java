package it.uniba.netminer.dataaccess;

public interface IDatabaseDelegate {
	public void onSessionFailed(Exception exception);
}
