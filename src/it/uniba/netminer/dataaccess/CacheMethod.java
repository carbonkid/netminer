package it.uniba.netminer.dataaccess;

public class CacheMethod implements ICacheMethod {

	public static final CacheMethod READ_ONLY_STRATEGY = 
			new CacheMethod("readOnlyStrategy", "read-only");
	public static final CacheMethod READ_WRITE_STRATEGY = 
			new CacheMethod("readWriteStrategy", "read-write");
	public static final CacheMethod NON_STRICT_READ_WRITE_STRATEGY = 
			new CacheMethod("nonStrictReadWriteStrategy", "nonstrict-read-write");
	public static final CacheMethod TRANSACTIONAL_STRATEGY = 
			new CacheMethod("transactionalStrategy", "transactional");
	
	private String _description;
	private String _key;
	
	private CacheMethod(String method, String keyword){
		this._description = method;
		this._key = keyword;
	}
	
	@Override
	public String getDescription() {
		return this._description;
	}

	@Override
	public String getAttribute() {
		return this._key;
	}

}
