package it.uniba.netminer.dataschema;

public interface IForeignKeysList extends Iterable<IRelationship> {
	public int getSize();
	public IRelationship getAt(int index) throws IndexOutOfBoundsException;
}
