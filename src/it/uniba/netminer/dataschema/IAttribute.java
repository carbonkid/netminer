package it.uniba.netminer.dataschema;

public interface IAttribute {
	public IEntity getEntityOwner();
	public String getName();
	public String getFullyQualifiedName();
	public IType getDataType();
	public int getDataLength();
	public boolean isInvolvedInPrimaryKey();
	public boolean isInvolvedInForeignKey();
	public boolean isReferencedInForeignKey();
}
