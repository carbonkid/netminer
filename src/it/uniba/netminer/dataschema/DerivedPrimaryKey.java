package it.uniba.netminer.dataschema;

import it.uniba.netminer.exceptions.EntityNotFoundInSchemaException;
import it.uniba.netminer.exceptions.InconsistentPrimaryKeyAttributesException;

import java.util.HashMap;
import java.util.Iterator;

public class DerivedPrimaryKey implements IDerivedPrimaryKey {

	protected IDerivedEntity _owner;
	protected HashMap<String, IDerivedAttribute> _attributes;
	
	protected DerivedPrimaryKey(IDerivedEntity owner) throws EntityNotFoundInSchemaException{
		if(owner==null){
			throw new EntityNotFoundInSchemaException();
		}
		
		this._owner = owner;
		this._attributes = new HashMap<String, IDerivedAttribute>();
	}
	
	
	protected void addAttribute(IDerivedAttribute attribute) throws InconsistentPrimaryKeyAttributesException {
		if(this._owner != attribute.getDerivedEntityOwner()){
			throw new EntityNotFoundInSchemaException();
		}
		
		this._attributes.put(attribute.getFullyQualifiedName(), attribute);
	}
	
	@Override
	public Iterator<IDerivedAttribute> iterator() {
		return this._attributes.values().iterator();
	}

	@Override
	public boolean includesAttribute(IDerivedAttribute attribute) {
		return this._attributes.containsKey(attribute.getFullyQualifiedName());
	}

	@Override
	public int getNumberOfAttributes() {
		return this._attributes.values().size();
	}
	
	@Override
	public String toString(){
		return "<derived primary key of "+this.getNumberOfAttributes()+" attributes>";
	}

}
