package it.uniba.netminer.dataschema;

public interface IRelationship extends Iterable<IRelationshipField> {
	public String getName();
	public IEntity getOwnerEntity();
	public IEntity getRelatedEntity();
	public int getLinkedFieldsNumber();
}
