package it.uniba.netminer.dataschema;

import com.google.common.base.Joiner;

public class ConjunctiveCondition extends Condition {
	
	private final static String CONJUNCTION_SYMBOL = " and ";
	
	public ConjunctiveCondition(IAttribute attr) {
		super(attr);
	}

	@Override
	public String toString() {
		return Joiner.on(ConjunctiveCondition.CONJUNCTION_SYMBOL).join(super._restrictions);
	}

}
