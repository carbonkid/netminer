package it.uniba.netminer.dataschema;

import java.sql.Date;

public class Restriction {
	
	private IOperator _operator;
	private Object _value;
	protected IAttribute _attribute;
	
	private Restriction(IOperator op, Object val){
		this._operator = op;
		this._value = val;
		this._attribute = null;
	}
	
	@Override
	public String toString(){
		String attr = "";
		if(this._attribute!=null){
			attr = this._attribute.getFullyQualifiedName();
		}
		if(this._value instanceof IAttribute){
			return attr+" "+this._operator.getSymbol()+" "+((IAttribute)this._value).getFullyQualifiedName();
		}else if(this._value instanceof Date){
			return attr+" "+this._operator.getSymbol()+" '"+this._value+"'";
		}else if(this._value instanceof String){
			return attr+" "+this._operator.getSymbol()+" '"+this._value+"'";
		}else{
			return attr+" "+this._operator.getSymbol()+" "+this._value.toString();
		}
		
	}
	
	public static Restriction equal(Object val){
		return new Restriction(Operator.EQUAL, val);
	}
	
	public static Restriction notEqual(Object val){
		return new Restriction(Operator.NOT_EQUAL, val);
	}
	
	public static Restriction greaterThan(Object val){
		return new Restriction(Operator.GREATER_THEN, val);
	}
	
	public static Restriction greaterThanOrEqual(Object val){
		return new Restriction(Operator.GREATER_THEN_OR_EQUAL, val);
	}
	
	public static Restriction lowerThan(Object val){
		return new Restriction(Operator.LOWER_THEN, val);
	}
	
	public static Restriction lowerThanOrEqual(Object val){
		return new Restriction(Operator.LOWER_THEN_OR_EQUAL, val);
	}
}
