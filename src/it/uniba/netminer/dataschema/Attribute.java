package it.uniba.netminer.dataschema;

public class Attribute implements IAttribute {
	
	protected Entity _owner;
	protected String _name;
	protected Type _dataType;
	protected int _dataLength;
	protected boolean _isPKinvolved;
	protected boolean _isFKinvolved;
	protected boolean _isFKreferenced;
	
	protected Attribute(Entity owner, String name, int dataType){
		this._isPKinvolved = false;
		this._isFKinvolved = false;
		this._isFKreferenced =false;
		this._owner = owner;
		this._name = name;
		this._dataType = new Type(dataType);
		this._dataLength = 0;
	}

	@Override
	public String getName() {
		return this._name;
	}
	
	@Override
	public String getFullyQualifiedName() {
		return this._name;
	}
	

	@Override
	public IType getDataType() {
		return this._dataType;
	}
	
	@Override
	public IEntity getEntityOwner() {
		return this._owner;
	}

	@Override
	public int getDataLength() {
		return this._dataLength;
	}
	
	@Override
	public boolean isInvolvedInPrimaryKey() {
		return this._isPKinvolved;
	}

	@Override
	public boolean isInvolvedInForeignKey() {
		return this._isFKinvolved;
	}

	@Override
	public boolean isReferencedInForeignKey() {
		return this._isFKreferenced;
	}
	
	@Override
	public String toString() {
		String retval;
		retval = "<"+this._name+">";
		return retval;
	}
	

	
	@Override
	public boolean equals(Object obj){
		boolean retval = false;
		if(obj instanceof Attribute){
			Attribute objToCompare = (Attribute)obj;
			if(objToCompare._dataLength == this._dataLength &&
			   objToCompare._dataType == this._dataType &&
			   objToCompare._name.equals(this._name) &&
			   objToCompare._owner == this._owner){
				retval = true;
			}
		}
		return retval;
	}

	

}
