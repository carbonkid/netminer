package it.uniba.netminer.dataschema;

import it.uniba.netminer.dataaccess.DatabaseSession;
import it.uniba.netminer.dataaccess.QueryDelegate;
import it.uniba.netminer.exceptions.InvalidEntityException;
import it.uniba.netminer.exceptions.InvalidRelationshipException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class View implements IView{
	
	private static final String ALIAS_ENTITY_PREFIX = "T"; 
	
	protected LinkedHashMap<Alias, IDerivedEntity> _entitySet;
	protected DerivedEntity _referencedEntityForPrimaryKey;
	protected ForeignKeyList _fkList; 
	protected ForeignKeyList _lastFkAdded;
	protected DerivedEntity _lastEntityAdded;
	protected LinkedList<ConjunctiveCondition> _joinCondition;
	
	public View(){
		this._entitySet = new LinkedHashMap<Alias, IDerivedEntity>();
		this._joinCondition = new LinkedList<ConjunctiveCondition>();
	}
	
	public View(IEntity entity, Alias alias){
		if(entity==null){
			throw new InvalidEntityException();
		}
		
		this._entitySet = new LinkedHashMap<Alias, IDerivedEntity>();
		this._joinCondition = new LinkedList<ConjunctiveCondition>();
		
		//aggiungiamo l'entit� alle entit� della vista.
		//String alias = View.ALIAS_ENTITY_PREFIX+this.getNumberOfAttributes();
		DerivedEntity entityExpanded = new DerivedEntity(this, entity, alias);
		this._lastEntityAdded = entityExpanded;
		
		this._entitySet.put(alias, entityExpanded);
		
		//impostiamo la chiave primaria della vista.
		this._referencedEntityForPrimaryKey = entityExpanded;
		
		//impostiamo le chiavi esterne di queste entit�.
		this._fkList = new ForeignKeyList();
		this._lastFkAdded = new ForeignKeyList();
		
		for(IRelationship foreignKey : entityExpanded.getOriginalEntity().getForeignKeyList()){
			DerivedRelationship fk = new DerivedRelationship(this, entityExpanded, (Relationship)foreignKey);
			this._fkList.addForeignKey(fk);
			this._lastFkAdded.addForeignKey(fk);
		}	
	}

	@Override
	public Iterator<IDerivedAttribute> iterator() {
		return new InterleavedDerivedAttributeIterator();
	}

	@Override
	public int getNumberOfAttributes() {
		return this._entitySet.size();
	}


	@Override
	public IForeignKeysList getForeignKeyList() {
		return this._fkList;
	}
	
	
	@Override
	public void joinByRelationship(IRelationship relationship, Alias alias) throws InvalidRelationshipException {
		if(relationship.getClass()!=DerivedRelationship.class){
			throw new InvalidRelationshipException();
		}
		
		DerivedRelationship rel = (DerivedRelationship)relationship;
		
		if(rel.getDerivedEntityOwner()==null ||
		   rel.getRelatedEntity()==null){
			throw new InvalidEntityException();
		}
		
		
		//aggiungiamo l'entit� alle entit� della vista e alla Criteria query.
		//String alias = View.ALIAS_ENTITY_PREFIX+this.getNumberOfAttributes();
		
		DerivedEntity entityExpanded = new DerivedEntity(this, relationship.getRelatedEntity(), alias);
		this._entitySet.put(alias, entityExpanded);
		this._lastEntityAdded = entityExpanded;
	
		
		//se non � stata ancora impostata, inseriamo la chiave primaria
		if(this._referencedEntityForPrimaryKey==null){
			this._referencedEntityForPrimaryKey = entityExpanded;
		}
		
		//rendiamo inutilizzabile questa chiave, dopo il join (espandere
		//a ripetizione la tabella usando pi� volte una stessa chiave esterna
		//porterebbe ad una vista ininfluente al fine del calcolo dei patterns)
		
		this._fkList.removeForeignKey(rel);
	
		//impostiamo le chiavi esterne di queste entit�.
		if(this._fkList==null){
			this._fkList = new ForeignKeyList();
		}
		if(this._lastFkAdded==null){
			this._lastFkAdded = new ForeignKeyList();
		}
		this._lastFkAdded._fk.clear();
		for(IRelationship foreignKey : entityExpanded.getOriginalEntity().getForeignKeyList()){
			DerivedRelationship fk = new DerivedRelationship(this, entityExpanded, (Relationship)foreignKey);
			this._fkList.addForeignKey(fk);
			this._lastFkAdded.addForeignKey(fk);
		}
		
		//aggiungiamo la condizione di equijoin per questa operazione di join
		
		
		for(IRelationshipField foreignKeyField : rel){
			
			IAttribute firstFieldInCondition = foreignKeyField.getIndexAttribute();
			IAttribute secondFieldInCondition = foreignKeyField.getReferencedAttribute();
			DerivedEntity derivedEntityOwner = rel.getDerivedEntityOwner();
			DerivedAttribute firstDerivedAttribute = new DerivedAttribute(derivedEntityOwner, firstFieldInCondition);
			DerivedAttribute secondDerivedAttribute = new DerivedAttribute(entityExpanded, secondFieldInCondition);
			
			ConjunctiveCondition equiJoinCondition = new ConjunctiveCondition(firstDerivedAttribute);
			equiJoinCondition.addRestriction(Restriction.equal(secondDerivedAttribute));
			this._joinCondition.add(equiJoinCondition);
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public IView clone(){
		View view = new View();
		view._entitySet = (LinkedHashMap<Alias, IDerivedEntity>) this._entitySet.clone();
		view._lastFkAdded = new ForeignKeyList();
		view._lastFkAdded._fk = (HashMap<String, IRelationship>) this._lastFkAdded._fk.clone();
		view._fkList = new ForeignKeyList();
		view._fkList._fk = (HashMap<String, IRelationship>) this._fkList._fk.clone();
		view._referencedEntityForPrimaryKey = this._referencedEntityForPrimaryKey;
		view._joinCondition = (LinkedList<ConjunctiveCondition>) this._joinCondition.clone();
		
		return view;
	}

	
	
	
	@Override
	public IForeignKeysList getLastForeignKeyAddedList() {
		return this._lastFkAdded;
	}
	
	
	
	@Override
	public IDerivedEntity getLastJoinedEntity() {
		return this._lastEntityAdded;
	}
	
	public Iterator<IDerivedEntity> derivedEntityDescendingIterator(){
		LinkedList<IDerivedEntity> list = new LinkedList<IDerivedEntity>(this._entitySet.values());
		return list.descendingIterator();
	}
	
	
	public Iterator<IDerivedEntity> derivedEntityIterator(){
		Iterator<IDerivedEntity> iterator = this._entitySet.values().iterator();
		return iterator;
	}
	
	
	
	@Override
	public IQueryable query() {
		return new QueryView();
	}
	
	
	
	private class InterleavedDerivedAttributeIterator implements Iterator<IDerivedAttribute> {

		private Iterator<IDerivedEntity> _entityIterator;
		private Iterator<IDerivedAttribute> _attributeIterator;
		
		private InterleavedDerivedAttributeIterator(){
			this._entityIterator = _entitySet.values().iterator();
			this._attributeIterator = null;
		}
		
		@Override
		public boolean hasNext() {
			boolean isNextPresent = false;
			if(this._attributeIterator == null){
				if(this._entityIterator.hasNext()){
					isNextPresent = true;
				}
			}else{
				if(this._attributeIterator.hasNext()){
					isNextPresent = true;
				}else{
					if(this._entityIterator.hasNext()){
						isNextPresent = true;
					}
				}
			}
			return isNextPresent;
		}

		@Override
		public IDerivedAttribute next() {
			if(this._attributeIterator == null){
				if(this._entityIterator.hasNext()){
					this._attributeIterator = this._entityIterator.next().iterator();
				}
			}
			
			IDerivedAttribute attribute = this._attributeIterator.next();
			if(!this._attributeIterator.hasNext()){
				if(this._entityIterator.hasNext()){
					this._attributeIterator = this._entityIterator.next().iterator();
				}
			}
			
			return attribute;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		
	}


	
	
	
	
	private class QueryView implements IQueryable{

		private int _limitationIndex;
		private int _limitationCount;
		private LinkedHashMap<IAttribute, Boolean> _projectionList;
		private LinkedList<ICondition> _conditionList;
		
		private QueryView(){
			this._conditionList = new LinkedList<ICondition>();
			this._projectionList = new LinkedHashMap<IAttribute, Boolean>();
			this._limitationIndex = -1;
			this._limitationCount = -1;
		}
		
		@Override
		public void addCondition(ICondition condition) {
			this._conditionList.add(condition);
		}

		@Override
		public void addProjection(IAttribute attribute, boolean distinct) {
			this._projectionList.put(attribute, distinct);
		}
		
		@Override
		public ICondition getLastCondition() {
			return this._conditionList.get(this._conditionList.size()-1);
		}
		
		@Override
		public IQueryable clone() {
			QueryView query = new QueryView();
			query._conditionList = (LinkedList<ICondition>) this._conditionList.clone();
			query._projectionList = this._projectionList;
			query._limitationCount = this._limitationCount;
			query._limitationIndex = this._limitationIndex;
			return query;
		}
		
		@Override
		public String toString(){
			return this.toHQL();
		}
		
		@Override
		public String toHQL() {
			StringBuilder query = new StringBuilder();
			
			Iterator<Entry<IAttribute, Boolean>> projectionIterator = this._projectionList.entrySet().iterator();
			if(projectionIterator.hasNext()){
				query.append("select ");
				
				while(projectionIterator.hasNext()){
					Entry<IAttribute, Boolean> entry = projectionIterator.next();
					IAttribute attribute = entry.getKey();
					Boolean isDistinct = entry.getValue();
					if(isDistinct == true){
						query.append("distinct "+attribute.getFullyQualifiedName());
					}else{
						query.append(attribute.getFullyQualifiedName());
					}
					
					if(projectionIterator.hasNext()){
						query.append(", ");
					}
				}
			}
			
			
			if(query.length()>0){
				query.append(" ");
			}
			query.append("from ");
			
			Iterator<IDerivedEntity> entityIterator = _entitySet.values().iterator();
			while(entityIterator.hasNext()){
				IDerivedEntity entity = entityIterator.next();
				query.append(entity.getName());
				query.append(" ");
				query.append(entity.getAlias().getName());
				if(entityIterator.hasNext()){
					query.append(", ");
				}
			}
			
			
			Iterator<ConjunctiveCondition> joinIterator = _joinCondition.iterator();
			if(joinIterator.hasNext()){
				query.append(" where ");
			}
			while(joinIterator.hasNext()){
				ConjunctiveCondition cond = joinIterator.next();
				query.append(cond.toString());
				if(joinIterator.hasNext()){
					query.append(" and ");
				}
			}
			
			Iterator<ICondition> selectionConditionIterator = this._conditionList.iterator();
			
			if(selectionConditionIterator.hasNext()){
				if(_joinCondition.size()>0){
					query.append(" and ");
				}else{
					query.append(" where ");
				}
			}
			while(selectionConditionIterator.hasNext()){
				ICondition condition = selectionConditionIterator.next();
				query.append(condition.toString());
				if(selectionConditionIterator.hasNext()){
					query.append(" and ");
				}
			}
			
			return query.toString();
		}

		
		@Override
		public void setLimitIndex(int startIndex) {
			this._limitationIndex = startIndex;
		}

		@Override
		public void setLimitCount(int count) {
			this._limitationCount = count;
		}

		@Override
		public int getLimitIndex() {
			return this._limitationIndex;
		}

		@Override
		public int getLimitCount() {
			return this._limitationCount;
		}

		@Override
		public List<?> list() {
			QueryDelegate delegate = new QueryDelegate(this);
			new DatabaseSession(delegate).dispatch();
			return delegate.getResultSet();
		}

		
	}






	@Override
	public IDerivedPrimaryKey getPrimaryKey() {
		if(this._entitySet.values().iterator().hasNext()){
			return this._entitySet.values().iterator().next().getDerivedPrimaryKey();
		}else{
			return null;
		}
	}


}
