package it.uniba.netminer.dataschema;

import it.uniba.netminer.exceptions.UnsupportedTypeException;

import java.sql.Types;
import java.util.HashMap;



public class Type implements IType {
	
	private static HashMap<Integer, Morphology> typeMap;
	
	/*
	 * Informazioni circa il casting delegato dei tipi sql in tipi hibernate
	 * controllare la pagina <url>http://docs.jboss.org/hibernate/orm/3.6/reference/en-US/html/types.html#types-value-basic</url>.
	 */
	private static HashMap<Integer, Class> classMap;
	public static enum Morphology {DISCRETE, CONTINUOUS, UNCLASSIFIED};
	
	static {
		Type.typeMap = new HashMap<Integer, Morphology>();
		Type.typeMap.put(new Integer(Types.ARRAY), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.BIGINT), Morphology.DISCRETE);
		Type.typeMap.put(new Integer(Types.BINARY), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.BIT), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.BLOB), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.CHAR), Morphology.DISCRETE);
		Type.typeMap.put(new Integer(Types.CLOB), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.DATE), Morphology.DISCRETE);
		Type.typeMap.put(new Integer(Types.DECIMAL), Morphology.CONTINUOUS);
		Type.typeMap.put(new Integer(Types.DISTINCT), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.DOUBLE), Morphology.CONTINUOUS);
		Type.typeMap.put(new Integer(Types.FLOAT), Morphology.CONTINUOUS);
		Type.typeMap.put(new Integer(Types.INTEGER), Morphology.DISCRETE);
		Type.typeMap.put(new Integer(Types.JAVA_OBJECT), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.LONGVARBINARY), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.LONGVARCHAR), Morphology.DISCRETE);
		Type.typeMap.put(new Integer(Types.NULL), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.NUMERIC), Morphology.CONTINUOUS);
		Type.typeMap.put(new Integer(Types.OTHER), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.REAL), Morphology.CONTINUOUS);
		Type.typeMap.put(new Integer(Types.REF), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.SMALLINT), Morphology.CONTINUOUS);
		Type.typeMap.put(new Integer(Types.STRUCT), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.TIME), Morphology.DISCRETE);
		Type.typeMap.put(new Integer(Types.TIMESTAMP), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.TINYINT), Morphology.CONTINUOUS);
		Type.typeMap.put(new Integer(Types.VARBINARY), Morphology.UNCLASSIFIED);
		Type.typeMap.put(new Integer(Types.VARCHAR), Morphology.DISCRETE);
		
		Type.classMap = new HashMap<Integer, Class>();
		Type.classMap.put(new Integer(Types.ARRAY), java.lang.String.class);
		Type.classMap.put(new Integer(Types.BIGINT), java.lang.Long.class);
		Type.classMap.put(new Integer(Types.BINARY), java.lang.Long.class);
		Type.classMap.put(new Integer(Types.BIT), java.lang.Boolean.class);
		Type.classMap.put(new Integer(Types.BLOB), java.sql.Blob.class);
		Type.classMap.put(new Integer(Types.CHAR), java.lang.Character.class);
		Type.classMap.put(new Integer(Types.CLOB), java.sql.Clob.class);
		Type.classMap.put(new Integer(Types.DATE), java.sql.Date.class);
		Type.classMap.put(new Integer(Types.DECIMAL), java.math.BigDecimal.class);
		Type.classMap.put(new Integer(Types.DISTINCT), java.lang.String.class);
		Type.classMap.put(new Integer(Types.DOUBLE), java.lang.Double.class);
		Type.classMap.put(new Integer(Types.FLOAT), java.lang.Float.class);
		Type.classMap.put(new Integer(Types.INTEGER), java.lang.Integer.class);
		Type.classMap.put(new Integer(Types.JAVA_OBJECT), java.lang.Object.class);
		Type.classMap.put(new Integer(Types.LONGVARBINARY), java.lang.Byte[].class);
		Type.classMap.put(new Integer(Types.LONGVARCHAR), java.lang.String.class);
		Type.classMap.put(new Integer(Types.NULL), java.lang.String.class);
		Type.classMap.put(new Integer(Types.NUMERIC), java.math.BigDecimal.class);
		Type.classMap.put(new Integer(Types.OTHER), java.lang.String.class);
		Type.classMap.put(new Integer(Types.REAL), java.lang.Double.class);
		Type.classMap.put(new Integer(Types.REF), java.lang.String.class);
		Type.classMap.put(new Integer(Types.SMALLINT), java.lang.Short.class);
		Type.classMap.put(new Integer(Types.STRUCT), java.lang.String.class);
		Type.classMap.put(new Integer(Types.TIME), java.sql.Time.class);
		Type.classMap.put(new Integer(Types.TIMESTAMP), java.util.Calendar.class);
		Type.classMap.put(new Integer(Types.TINYINT), java.lang.Byte.class);
		Type.classMap.put(new Integer(Types.VARBINARY), java.lang.Byte[].class);
		Type.classMap.put(new Integer(Types.VARCHAR), java.lang.String.class);
		
	}
	
	protected int _sqlType;
	protected Class _javaType;
	protected Morphology _morphologyType;
	
	
	
	protected Type(int sqlType){
		Morphology morphology = Type.typeMap.get(sqlType);
		Class javaClass = Type.classMap.get(sqlType);
		if(morphology ==null || javaClass==null){
			throw new UnsupportedTypeException();
		}
		
		this._sqlType = sqlType;
		this._morphologyType = morphology;
		this._javaType = javaClass;
	}



	@Override
	public int getSqlType() {
		return this._sqlType;
	}



	@Override
	public Class getObjectType() {
		return this._javaType;
	}



	@Override
	public Morphology getMorphology() {
		return this._morphologyType;
	}
}
