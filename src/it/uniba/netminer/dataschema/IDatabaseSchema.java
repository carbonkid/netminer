package it.uniba.netminer.dataschema;

import it.uniba.netminer.exceptions.EntityNotFoundInSchemaException;

public interface IDatabaseSchema extends Iterable<IEntity> {
	public int getNumberOfEntities();
	public IEntity getEntity(String name) throws EntityNotFoundInSchemaException;
}
