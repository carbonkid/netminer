package it.uniba.netminer.dataschema;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.hibernate.Session;

import it.uniba.netminer.dataaccess.DatabaseSession;
import it.uniba.netminer.dataaccess.ITransactionDelegate;
import it.uniba.netminer.exceptions.EntityNotFoundInSchemaException;

class DatabaseSchema implements IDatabaseSchema {

	private Database _databaseOwner;
	private ArrayList<IEntity> _entities;
	private HashMap<String, Integer> _entitiesIndices;
	
	
	protected DatabaseSchema(Database owner){
		this._databaseOwner = owner;
		this._entities = new ArrayList<IEntity>();
		this._entitiesIndices = new HashMap<String, Integer>();
		this.extractSchema();
	}

	
	/**
	 * Metodo privato che estrae lo schema di un database.
	 * Lo schema contiene le definizioni e le informazioni delle tabelle,
	 * degli attributi e delle relazioni (vincoli relazionali di chiave).
	 */
	private void extractSchema() {
		new DatabaseSession(
			new ITransactionDelegate(){
				@Override
				public void onSessionOpened(Connection connection, Session session) throws SQLException {
					//leggiamo solo le tabelle (escludendo viste, tabelle temporanee e di configurazione interne)
					DatabaseMetaData metaData = connection.getMetaData();
					String[] tableTypeFilter = new String[]{"TABLE"};
					ResultSet tables = metaData.getTables(null, null, null, tableTypeFilter);
					int tableIndex = 0;
					while(tables.next()){
						String tableName = tables.getString("TABLE_NAME");
						Entity entity = new Entity(tableName, DatabaseSchema.this);
						DatabaseSchema.this._entities.add(tableIndex, entity);
						DatabaseSchema.this._entitiesIndices.put(tableName, tableIndex);
						tableIndex++;
						
						//leggiamo gli attributi, le chiavi esterne e le chiavi primarie di ogni tabella
						ResultSet attributes = metaData.getColumns(null, null, tableName, null);
						while(attributes.next()){
							String attributeName = attributes.getString("COLUMN_NAME");
							int attributeDataType = attributes.getInt("DATA_TYPE"); //from java.sql.Types
							Attribute attribute = new Attribute(entity, attributeName, attributeDataType);
							entity._attributesList.put(attributeName, attribute);
						}
						
						//leggiamo la chiave primaria della tabella
						ResultSet primaryKeys = metaData.getPrimaryKeys(null, null, tableName);
						while(primaryKeys.next()){
							String attributeName = primaryKeys.getString("COLUMN_NAME");
							Attribute attribute = (Attribute) entity.getAttribute(attributeName);
							attribute._isPKinvolved = true;
							entity._primaryKey.addAttribute(attribute);
						}
					}
					
					//ora aggiungiamo le chiavi esterne alla struttura dati
					tables.beforeFirst();
					tableIndex = 0;
					while(tables.next()){
						short firstSequenceNumber = 0;
						int foreignKeyIndex = 0;
						boolean isFirstFk = true;
						String tableName = tables.getString("TABLE_NAME");
						Entity tableEntity = (Entity) DatabaseSchema.this._entities.get(tableIndex);
						
						//leggiamo le chiavi esterne della tabella table name
						ResultSet foreignKeys = metaData.getImportedKeys(null, null, tableName);
						
						while(foreignKeys.next()){
							String fkTableName = foreignKeys.getString("FKTABLE_NAME");
							String pkTableName = foreignKeys.getString("PKTABLE_NAME");
							String fkTableAttributeName = foreignKeys.getString("FKCOLUMN_NAME");
							String pkTableAttributeName = foreignKeys.getString("PKCOLUMN_NAME");
							short keySequence = foreignKeys.getShort("KEY_SEQ");
							
							//discriminiamo le chiavi esterne in base al conteggio interno di sequenza
							//degli attributi, quando il conteggio viene resettato alla cifra iniziale
							//significa che il sistema sta analizzando una nuova chiave esterna
							if(isFirstFk){
								firstSequenceNumber = keySequence;
								isFirstFk = false;
							}
							if(keySequence==firstSequenceNumber){
								foreignKeyIndex++;
							}
							String relationshipName = fkTableName+"-"+pkTableName+"-"+foreignKeyIndex;
							
							Integer referencedEntityIndex = DatabaseSchema.this._entitiesIndices.get(pkTableName);
							Entity referencedEntity = (Entity) DatabaseSchema.this._entities.get(referencedEntityIndex);
							Relationship fk = (Relationship)tableEntity._fkList._fk.get(relationshipName);
							
							if(fk==null){
								fk = new Relationship(tableEntity, referencedEntity, relationshipName);
								tableEntity._fkList.addForeignKey(fk);
							}
							
							Attribute fkTableAttribute = (Attribute)tableEntity.getAttribute(fkTableAttributeName);	
							Attribute pkTableAttribute = (Attribute)referencedEntity.getAttribute(pkTableAttributeName);
							fkTableAttribute._isFKinvolved = true;
							pkTableAttribute._isFKreferenced = true;
							RelationshipField fieldPair = new RelationshipField(fkTableAttribute, pkTableAttribute, keySequence);
							fk.addFields(fieldPair);
						}
						
						tableIndex++;
					}
				}
	
				@Override
				public void onSessionFailed(Exception exception) {
					exception.printStackTrace();
				}
			}
		).dispatch();
		
		
		//richiediamo ad hibernate i mappings per le entit� registrate
		_databaseOwner.getMappingManager().refreshSessionFactoryWithMappings(_entities);
	}


	@Override
	public int getNumberOfEntities() {
		return this._entities.size();
	}


	@Override
	public IEntity getEntity(String name) throws EntityNotFoundInSchemaException{
		Integer entityIndex = this._entitiesIndices.get(name);
		if(entityIndex!=null){
			Entity entity = (Entity)this._entities.get(entityIndex);
			if(entity!=null){
				return entity;
			}else{
				throw new EntityNotFoundInSchemaException();
			}
		}else{
			throw new EntityNotFoundInSchemaException();
		}
		
	}


	@Override
	public Iterator<IEntity> iterator() {
		return this._entities.iterator();
	}
}
