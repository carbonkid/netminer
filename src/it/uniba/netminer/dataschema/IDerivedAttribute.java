package it.uniba.netminer.dataschema;

public interface IDerivedAttribute extends IAttribute {
	public IDerivedEntity getDerivedEntityOwner();
	public IAttribute getOriginalAttribute();
}
