package it.uniba.netminer.dataschema;

import java.io.Serializable;
import java.util.List;

public interface IQueryable extends Cloneable, Serializable {
	public String toHQL();
	public void addCondition(ICondition condition);
	public void addProjection(IAttribute attribute, boolean distinct);
	public void setLimitIndex(int startIndex);
	public void setLimitCount(int count);
	public int getLimitIndex();
	public int getLimitCount();
	public ICondition getLastCondition();
	public IQueryable clone();
	public List<?> list();
}
