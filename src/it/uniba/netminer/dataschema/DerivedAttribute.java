package it.uniba.netminer.dataschema;

public class DerivedAttribute implements IDerivedAttribute {

	protected IDerivedEntity _derivedEntityOwner;
	protected IAttribute _referencedAttribute;
	
	protected DerivedAttribute(IDerivedEntity owner, IAttribute attribute){
		this._derivedEntityOwner = owner;
		this._referencedAttribute = attribute;
	}

	@Override
	public IDerivedEntity getDerivedEntityOwner() {
		return this._derivedEntityOwner;
	}

	@Override
	public IAttribute getOriginalAttribute() {
		return this._referencedAttribute;
	}

	@Override
	public String getFullyQualifiedName() {
		return this._derivedEntityOwner.getAlias().getName()+"."+this._referencedAttribute.getName();
	}

	@Override
	public IEntity getEntityOwner() {
		return this._referencedAttribute.getEntityOwner();
	}

	@Override
	public String getName() {
		return this._referencedAttribute.getName();
	}

	@Override
	public IType getDataType() {
		return this._referencedAttribute.getDataType();
	}

	@Override
	public int getDataLength() {
		return this._referencedAttribute.getDataLength();
	}

	@Override
	public boolean isInvolvedInPrimaryKey() {
		return this._referencedAttribute.isInvolvedInPrimaryKey();
	}

	@Override
	public boolean isInvolvedInForeignKey() {
		return this._referencedAttribute.isInvolvedInForeignKey();
	}

	@Override
	public boolean isReferencedInForeignKey() {
		return this._referencedAttribute.isReferencedInForeignKey();
	}
	
	
}
