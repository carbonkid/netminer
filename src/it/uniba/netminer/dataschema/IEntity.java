package it.uniba.netminer.dataschema;


public interface IEntity extends Iterable<IAttribute>, IQuery{
	public IDatabaseSchema getSchemaOwner();
	public String getName();
	public int getNumberOfAttributes();
	public IAttribute getAttribute(String name);
	public IForeignKeysList getForeignKeyList();
	public IPrimaryKey getPrimaryKey();
	public IView deriveView(Alias alias);
}
