package it.uniba.netminer.dataschema;

public interface IDatabase {
	public String getName();
	public String getUrl();
	public IDatabaseSchema getDatabaseSchema();	
}
