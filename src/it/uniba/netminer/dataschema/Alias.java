package it.uniba.netminer.dataschema;

public class Alias {

	protected String _alias;
	
	public Alias(String name){
		this._alias = name;
	}
	
	public String getName(){
		return this._alias;
	}
	
}
