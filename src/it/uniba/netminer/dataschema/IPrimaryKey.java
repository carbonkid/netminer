package it.uniba.netminer.dataschema;

public interface IPrimaryKey extends Iterable<IAttribute>{
	public boolean includesAttribute(IAttribute attribute);
	public int getNumberOfAttributes();
}
