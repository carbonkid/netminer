package it.uniba.netminer.dataschema;

import it.uniba.netminer.exceptions.EntityNotFoundInSchemaException;
import java.util.HashMap;
import java.util.Iterator;

class Entity implements IEntity {

	protected String _name;
	protected IDatabaseSchema _schemaOwner;
	protected HashMap<String, IAttribute> _attributesList;
	protected ForeignKeyList _fkList;
	protected PrimaryKey _primaryKey;
	
	protected Entity(String name, IDatabaseSchema owner){
		this._name = name;
		this._schemaOwner = owner;
		this._attributesList = new HashMap<String, IAttribute>();
		this._fkList = new ForeignKeyList();
		this._primaryKey = new PrimaryKey(this);
	} 
	
	
	@Override
	public String getName() {
		return this._name;
	}
	
	
	
	@Override
	public int getNumberOfAttributes() {
		return this._attributesList.size();
	}


	@Override
	public IAttribute getAttribute(String name) throws EntityNotFoundInSchemaException{
		Attribute entity = (Attribute)this._attributesList.get(name);
		if(entity!=null){
			return entity;
		}else{
			throw new EntityNotFoundInSchemaException();
		}
		
	}


	@Override
	public Iterator<IAttribute> iterator() {
		return _attributesList.values().iterator();
	}
	
	
	@Override
	public String toString() {
		return "<"+this._name+">";
	}


	@Override
	public IDatabaseSchema getSchemaOwner() {
		return this._schemaOwner;
	}


	@Override
	public IForeignKeysList getForeignKeyList() {
		return this._fkList;
	}


	@Override
	public IPrimaryKey getPrimaryKey() {
		return this._primaryKey;
	}


	@Override
	public IView deriveView(Alias alias) {
		return new View(this, alias);	
	}

	
	
	
	@Override
	public boolean equals(Object obj){
		boolean retval=false;
		
		if(obj instanceof Entity){
			Entity entity = (Entity)obj;
			if(entity.getSchemaOwner() == this.getSchemaOwner() &&
			   entity.getName().equals(this._name) &&
			   entity.getNumberOfAttributes() == this._attributesList.size()){
				retval = true;
			}
		}
		
		return retval;
	}


	@Override
	public IQueryable query() {
		// TODO Auto-generated method stub
		return null;
	}
}
