package it.uniba.netminer.dataschema;

public interface ICondition extends Iterable<Restriction> {
	public void addRestriction(Restriction restriction);
	public IAttribute getConditionatedAttribute();
}
