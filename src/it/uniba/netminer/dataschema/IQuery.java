package it.uniba.netminer.dataschema;

public interface IQuery {
	public IQueryable query();
}
