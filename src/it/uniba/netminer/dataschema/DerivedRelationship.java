package it.uniba.netminer.dataschema;

import java.util.Iterator;

public class DerivedRelationship implements IRelationship {

	protected IView _ownedByView;
	protected Relationship _originalRelationship;
	protected DerivedEntity _derivedEntityOwner;
	
	protected DerivedRelationship(IView owner, 
							   DerivedEntity entityStart, 
							   Relationship relationship) {
		
		this._ownedByView = owner;
		this._derivedEntityOwner = entityStart;
		this._originalRelationship = relationship;
	}
	
	public IView getOwnerView(){
		return this._ownedByView;
	}
	
	
	public DerivedEntity getDerivedEntityOwner(){
		return this._derivedEntityOwner;
	}

	@Override
	public Iterator<IRelationshipField> iterator() {
		return this._originalRelationship.iterator();
	}

	@Override
	public String getName() {
		return this._originalRelationship.getName();
	}

	@Override
	public IEntity getOwnerEntity() {
		return this._derivedEntityOwner.getOriginalEntity();
	}

	@Override
	public IEntity getRelatedEntity() {
		return this._originalRelationship.getRelatedEntity();
	}

	@Override
	public int getLinkedFieldsNumber() {
		return this._originalRelationship.getLinkedFieldsNumber();
	}
	
	@Override
	public boolean equals(Object obj){
		boolean retval = false;
		if(obj instanceof DerivedRelationship){
			DerivedRelationship rel = (DerivedRelationship)obj;
			if(this._ownedByView == rel._ownedByView &&
			   this._derivedEntityOwner == rel._derivedEntityOwner &&
			   this._originalRelationship.equals(rel._originalRelationship)){
				retval = true;
			}
		}else if(obj instanceof Relationship){
			Relationship rel = (Relationship)obj;
			retval = this._originalRelationship.equals(rel);
		}
		return retval;
	}

}
