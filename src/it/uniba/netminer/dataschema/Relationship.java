package it.uniba.netminer.dataschema;

import it.uniba.netminer.exceptions.InconsistentForeignKeyAttributesException;

import java.util.HashSet;
import java.util.Iterator;

public class Relationship implements IRelationship {

	protected IEntity _owner;
	protected IEntity _referencedEntity; 
	protected HashSet<IRelationshipField> _fields;
	protected String _name;
	
	protected Relationship(IEntity owner, IEntity referenced, String name){
		this._name = name;
		this._owner = owner;
		this._referencedEntity = referenced;
		this._fields = new HashSet<IRelationshipField>();
	}
	
	

	@Override
	public IEntity getOwnerEntity() {
		return this._owner;
	}



	@Override
	public IEntity getRelatedEntity() {
		return this._referencedEntity;
	}



	@Override
	public Iterator<IRelationshipField> iterator() {
		return this._fields.iterator();
	}
	
	
	
	protected void addFields(RelationshipField fieldPair) throws InconsistentForeignKeyAttributesException{
		if(fieldPair==null){
			throw new InconsistentForeignKeyAttributesException();
		}else{
			if(fieldPair._attributeKey.getEntityOwner() != this._owner ||
			   fieldPair._referencedKey.getEntityOwner() != this._referencedEntity){
				throw new InconsistentForeignKeyAttributesException();
			}
		}
		
		this._fields.add(fieldPair);
	}
	


	@Override
	public int getLinkedFieldsNumber() {
		return this._fields.size();
	}


	@Override
	public String getName() {
		return this._name;
	}
	
	
	@Override
	public String toString(){
		return "<"+this._name+">";
	}
	
	
	@Override
	public boolean equals(Object obj){
		boolean retval = false;
		
		if(obj instanceof Relationship){
			Relationship relationship = (Relationship)obj;
			if(relationship._name.equals(this._name) &&
			   relationship._referencedEntity == this._referencedEntity &&
			   relationship._owner == this._owner){
				   retval = true;
			}
		}
		
		return retval;
	}
}
