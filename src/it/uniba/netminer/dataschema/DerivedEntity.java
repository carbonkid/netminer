package it.uniba.netminer.dataschema;

import java.util.Iterator;
import java.util.LinkedHashMap;


public class DerivedEntity implements IDerivedEntity {
	
	protected IView _owner;
	protected LinkedHashMap<String, IDerivedAttribute> _attributesList;
	
	protected DerivedPrimaryKey _primaryKey;
	protected IEntity _originalEntityOwner;
	protected Alias _alias;
	
	protected DerivedEntity(IView viewOwner, IEntity entity, Alias alias){
		this._owner = viewOwner;
		this._alias = alias;
		this._primaryKey = new DerivedPrimaryKey(this);
		this._originalEntityOwner = entity;
		this._attributesList = new LinkedHashMap<String, IDerivedAttribute>();
		
		for(IAttribute originalAttribute : this._originalEntityOwner){
			IDerivedAttribute derived = new DerivedAttribute(this, originalAttribute);
			this._attributesList.put(derived.getFullyQualifiedName(), derived);
			
			if(originalAttribute.isInvolvedInPrimaryKey()){
				this._primaryKey.addAttribute(derived);
			}
		}
	}

	@Override
	public Alias getAlias() {
		return this._alias;
	}

	@Override
	public String getName() {
		return this._originalEntityOwner.getName();
	}

	@Override
	public int getNumberOfDerivedAttributes() {
		return this._attributesList.size();
	}

	@Override
	public IDerivedAttribute getDerivedAttribute(String name) {
		return null;
	}

	@Override
	public IDerivedPrimaryKey getDerivedPrimaryKey() {
		return this._primaryKey;
	}

	@Override
	public IEntity getOriginalEntity() {
		return this._originalEntityOwner;
	}

	@Override
	public IView getViewOwner() {
		return this._owner;
	}

	@Override
	public Iterator<IDerivedAttribute> iterator() {
		return this._attributesList.values().iterator();
	}

	@Override
	public IQueryable query() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
