package it.uniba.netminer.dataschema;

import it.uniba.netminer.dataschema.Type.Morphology;

public interface IType {
	public int getSqlType();
	public Class getObjectType();
	public Morphology getMorphology();
}
