package it.uniba.netminer.dataschema;

import it.uniba.netminer.exceptions.InvalidOperatorException;

public final class Operator implements IOperator {

	public static Operator EQUAL = new Operator("equal", "=");
	public static Operator NOT_EQUAL = new Operator("notEqual", "!=");
	public static Operator LOWER_THEN = new Operator("lowerThan", "<");
	public static Operator GREATER_THEN = new Operator("greaterThan", ">");
	public static Operator LOWER_THEN_OR_EQUAL = new Operator("lowerThanOrEqual", "<=");
	public static Operator GREATER_THEN_OR_EQUAL = new Operator("greaterThanOrEqual", ">=");
	
	private String _name;
	private String _symbol;
	
	private Operator(String name, String symbol) throws InvalidOperatorException{
		if(name==null ||
		   symbol==null ||
		   name.length() == 0 ||
		   symbol.length() == 0){
			throw new InvalidOperatorException();
		}
		
		this._name = name;
		this._symbol = symbol;
	}

	@Override
	public String getOperatorName() {
		return this._name;
	}
	
	@Override 
	public boolean equals(Object obj){
		boolean retval = false;
		if(obj.getClass().isInstance(Operator.class)){
			Operator op  = (Operator)obj;
			if(op == this){
				retval = true;
			}
			if(op._name.equals(op._name) &&
			   op._symbol.equals(op._symbol)){
				retval = true;
			}
		}
		return retval;
	}
	
	@Override
	public String getSymbol(){
		return this._symbol;
	}
	
}