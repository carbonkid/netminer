package it.uniba.netminer.dataschema;

public interface IOperator {
	public String getOperatorName();
	public String getSymbol();
}