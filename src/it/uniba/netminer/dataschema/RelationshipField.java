package it.uniba.netminer.dataschema;

public class RelationshipField implements IRelationshipField {

	protected IAttribute _attributeKey;
	protected IAttribute _referencedKey;
	private short _keySequence;
	
	protected RelationshipField(IAttribute key, IAttribute referenced, short index){
		this._attributeKey = key;
		this._referencedKey = referenced;
		this._keySequence = index;
	}
	
	@Override
	public short getKeySequence() {
		return this._keySequence;
	}
	
	@Override
	public IAttribute getIndexAttribute() {
		return this._attributeKey;
	}

	@Override
	public IAttribute getReferencedAttribute() {
		return this._referencedKey;
	}
	
	@Override
	public boolean equals(Object obj){
		boolean retval = false;
		if(obj.getClass() == RelationshipField.class){
			RelationshipField objToCompare = (RelationshipField)obj;
			if(objToCompare.getIndexAttribute().equals(this._attributeKey) &&
			   objToCompare.getReferencedAttribute().equals(this._referencedKey)){
				retval = true;
			}
		}
		return retval;
	}
	
	@Override
	public String toString(){
		return "<"+this._attributeKey.getEntityOwner().getName()+"."+this._attributeKey.getName()+
				", "+this._referencedKey.getEntityOwner().getName()+"."+this._referencedKey.getName()+">";
	}

}
