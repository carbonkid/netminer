package it.uniba.netminer.dataschema;

import it.uniba.netminer.dataaccess.IDataAccessor;
import it.uniba.netminer.dataaccess.IMappingManager;
import it.uniba.netminer.dataaccess.MappingManager;


public class Database implements IDatabase, IDataAccessor {

	private String _name;
	private String _url;
	protected MappingManager _mapping;
	protected DatabaseSchema _schema;
	
	public Database(
			String url, 
			String name, 
			String username, 
			String password, 
			String dialect, 
			String driverClass){
		
		this._name = name;
		this._url = url;
		this._mapping = new MappingManager(url, name, username, password, dialect, driverClass);
		this._schema = null;
	}
	
	public void loadSchema() {
		this._schema = new DatabaseSchema(this);
	}
	
	@Override
	public String getName() {
		return this._name;
	}

	@Override
	public String getUrl() {
		return this._url;
	}


	@Override
	public IDatabaseSchema getDatabaseSchema() {
		return this._schema;
	}

	@Override
	public IMappingManager getMappingManager() {
		return this._mapping;
	}

}
