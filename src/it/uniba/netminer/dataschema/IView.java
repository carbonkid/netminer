package it.uniba.netminer.dataschema;

import java.util.Iterator;

public interface IView extends Iterable<IDerivedAttribute>, IQuery{
	public int getNumberOfAttributes();
	public IDerivedPrimaryKey getPrimaryKey();
	public IForeignKeysList getForeignKeyList();
	public IForeignKeysList getLastForeignKeyAddedList();
	public IDerivedEntity getLastJoinedEntity();
	public void joinByRelationship(IRelationship relationship, Alias alias);
	public IView clone();
	public Iterator<IDerivedEntity> derivedEntityDescendingIterator();
	public Iterator<IDerivedEntity> derivedEntityIterator();
}
