package it.uniba.netminer.dataschema;

import it.uniba.netminer.exceptions.EntityNotFoundInSchemaException;
import it.uniba.netminer.exceptions.InconsistentPrimaryKeyAttributesException;

import java.util.HashMap;
import java.util.Iterator;

public class PrimaryKey implements IPrimaryKey {
	protected IEntity _owner;
	protected HashMap<String, IAttribute> _attributes;
	
	protected PrimaryKey(IEntity owner) throws EntityNotFoundInSchemaException{
		if(owner==null){
			throw new EntityNotFoundInSchemaException();
		}
		
		this._owner = owner;
		this._attributes = new HashMap<String, IAttribute>();
	}
	
	protected void addAttribute(IAttribute attribute) throws InconsistentPrimaryKeyAttributesException {
		if(this._owner != attribute.getEntityOwner()){
			throw new EntityNotFoundInSchemaException();
		}
		
		this._attributes.put(attribute.getName(), attribute);
	}
		

	@Override
	public Iterator<IAttribute> iterator() {
		return this._attributes.values().iterator();
	}

	@Override
	public boolean includesAttribute(IAttribute attribute) {
		return this._attributes.containsKey(attribute.getName());
	}

	@Override
	public int getNumberOfAttributes() {
		return this._attributes.values().size();
	}
	
	@Override
	public String toString(){
		return "<primary key of "+this.getNumberOfAttributes()+" attributes>";
	}

}
