package it.uniba.netminer.dataschema;

import com.google.common.base.Joiner;

public class DisjunctiveCondition extends Condition {
	
	public final static String DISJUNCTION_SYMBOL = " or ";
	
	public DisjunctiveCondition(IAttribute attr) {
		super(attr);
	}

	@Override
	public String toString() {
		return Joiner.on(DisjunctiveCondition.DISJUNCTION_SYMBOL).join(super._restrictions);
	}
}
