package it.uniba.netminer.dataschema;

public interface IDerivedPrimaryKey extends Iterable<IDerivedAttribute>{
	public boolean includesAttribute(IDerivedAttribute attribute);
	public int getNumberOfAttributes();
}
