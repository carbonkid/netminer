package it.uniba.netminer.dataschema;

public interface IDerivedEntity extends Iterable<IDerivedAttribute>, IQuery {
	public Alias getAlias();
	public String getName();
	public int getNumberOfDerivedAttributes();
	public IDerivedAttribute getDerivedAttribute(String name);
	public IDerivedPrimaryKey getDerivedPrimaryKey();
	public IEntity getOriginalEntity();
	public IView getViewOwner();
}
