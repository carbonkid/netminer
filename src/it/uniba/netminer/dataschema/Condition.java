package it.uniba.netminer.dataschema;

import java.util.Iterator;
import java.util.LinkedHashSet;

public abstract class Condition implements ICondition {

	protected LinkedHashSet<Restriction> _restrictions;
	protected IAttribute _conditionatedAttribute;
	
        
    public Condition(IAttribute attr){
        this._conditionatedAttribute = attr;
        this._restrictions = new LinkedHashSet<Restriction>();
    }
        
    
    @Override
    public void addRestriction(Restriction restriction){
    	restriction._attribute = this._conditionatedAttribute;
    	this._restrictions.add(restriction);
    }
    
    @Override
    public IAttribute getConditionatedAttribute(){
    	return this._conditionatedAttribute;
    }
    
    @Override
    public Iterator<Restriction> iterator(){
    	return this._restrictions.iterator();
    }
    
        
    @Override
    public abstract String toString();
    /*
    }
        String firstValueString= "";
        String valueString = "";
        if(this._value instanceof DerivedAttribute){
        	DerivedAttribute entity = (DerivedAttribute)this._value;
            valueString = entity.getFullyQualifiedName();
        }else{
        	if(this._value instanceof String){
        		valueString = "'"+this._value.toString()+"'";
        	}else{
        		valueString = this._value.toString();
        	}
            
        }
                
        if(this._conditionatedAttribute instanceof DerivedAttribute){
            DerivedAttribute entity = (DerivedAttribute)this._conditionatedAttribute;
            firstValueString = entity.getFullyQualifiedName();
        }else{
            firstValueString = this._conditionatedAttribute.toString();
        }
                        
        return firstValueString+" "+this._conditionalOperator.getSymbol()+" "+valueString;
        
    }
    */

}