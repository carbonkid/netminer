package it.uniba.netminer.dataschema;

public interface IRelationshipField {
	public IAttribute getIndexAttribute();
	public IAttribute getReferencedAttribute();
	public short getKeySequence();
}
