package it.uniba.netminer.dataschema;

import java.util.HashMap;
import java.util.Iterator;

public class ForeignKeyList implements IForeignKeysList {
	
	protected HashMap<String, IRelationship> _fk;
	
	protected ForeignKeyList(){
		this._fk = new HashMap<String, IRelationship>();
	}
	
	@Override
	public Iterator<IRelationship> iterator() {
		return this._fk.values().iterator();
	}
	
	@Override
	public int getSize() {
		return this._fk.size();
	}
	
	
	public IRelationship getAt(int index) throws IndexOutOfBoundsException {
		return (IRelationship) this._fk.values().toArray()[index];
	}
	
	
	protected void addForeignKey(IRelationship fk){
		this._fk.put(fk.getName(),fk);
	}
	
	
	protected void removeForeignKey(IRelationship fk){
		this._fk.remove(fk.getName());
	}
	
}
