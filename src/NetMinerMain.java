import it.uniba.netminer.assets.ui.NetMinerFrame;

import javax.swing.UIManager;

public class NetMinerMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			
		}
		new NetMinerFrame();
	}

}
